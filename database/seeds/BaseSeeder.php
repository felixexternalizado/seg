<?php

use Illuminate\Database\Seeder;

class BaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

		DB::statement('SET FOREIGN_KEY_CHECKS = 0;'); // Desactivamos la revisión de claves foráneas
        DB::table('dias')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;'); // Reactivamos la revisión de claves foráneas

		date_default_timezone_set('Europe/Madrid');
		$start_date = new DateTime('2019-01-01');
		$end_date = new DateTime('2026-01-01');
		$period = new DatePeriod($start_date, new DateInterval('P1D'), $end_date, DatePeriod::EXCLUDE_START_DATE );
		foreach($period as $date) {
			DB::table('dias')->insert([
            'dia'=> $date->format('d'),
			'semana'=> $date->format("W"),
			'mes'=> $date->format('m'),
			'anno'=> $date->format('Y')]);

		}
//
//		DB::table('empleados')->insert([
//            'nombre' => '(empleado llood)',
//			'resumen' => 'll',
//
//
//        ]);
//		DB::table('cargos')->insert([
//            'resumen' => '(cargo llood)',
//        ]);
//
//		DB::table('lineas')->insert([
//            'resumen' => '(linea llood)',
//        ]);
//		DB::table('procesos')->insert([
//            'resumen' => '(proceso llood)',
//			 'lineas_id' => 0,
//        ]);
//		DB::table('cargoslineasempleados')->insert([
//			 'cargos_id' => 0,
//			 'lineas_id' => 0,
//			 'empleados_id' => 0,
//
//        ]);




    }
}
