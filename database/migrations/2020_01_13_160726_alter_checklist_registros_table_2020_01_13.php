<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterChecklistRegistrosTable20200113 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('checklist_confpro_registros', function (Blueprint $table) {
            $table->string('condicional')->after('opciones');
        });

        Schema::table('checklist_regcalidad_registros', function (Blueprint $table) {
            $table->string('condicional')->after('opciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
