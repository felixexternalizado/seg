<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aplicaciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre')->nullable();
            $table->timestamps();
        });
        Schema::create('checklist_confpro_registros', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('confirmacionproceso_id')->unsigned();
            $table->foreign('confirmacionproceso_id')->references('id')->on('confirmacionprocesos');
            $table->integer('indice')->default(0);
            $table->string('valor')->default("");
            $table->string('fase')->default("");
            $table->string('foto')->default("");
            $table->string('descripcion')->default("");
            $table->string('operacion')->default("");
            $table->string('comentario')->default("");
            $table->string('noconformidad')->default("");
            $table->string('limite1')->default("");
            $table->string('limite2')->default("");
            $table->string('tipo')->default("");
            $table->string('instrucciones')->default("");
            $table->string('validador')->default("");
            $table->string('opciones')->default("");
            $table->string('requerido')->default("");
            $table->string('valormanual')->default("");
            $table->tinyInteger('ncFlag')->default(0);
            $table->string('horaEntrada')->default("");
            $table->timestamps();
        });
        Schema::create('checklist_regcalidad_registros', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('registrocalidad_id')->unsigned();
            $table->foreign('registrocalidad_id')->references('id')->on('registrosdecalidad');
            $table->integer('indice')->default(0);
            $table->string('valor')->default("");
            $table->string('fase')->default("");
            $table->string('foto')->default("");
            $table->string('frecuencia')->default("");
            $table->string('descripcion')->default("");
            $table->string('operacion')->default("");
            $table->string('comentario')->default("");
            $table->string('noconformidad')->default("");
            $table->string('limite1')->default("");
            $table->string('limite2')->default("");
            $table->string('tipo')->default("");
            $table->string('instrucciones')->default("");
            $table->string('validador')->default("");
            $table->string('opciones')->default("");
            $table->string('requerido')->default("");
            $table->bigInteger('evaluador')->unsigned();
            $table->foreign('evaluador')->references('id')->on('empleados');
            $table->string('valormanual')->default("");
            $table->tinyInteger('ncFlag')->default(0);
            $table->string('horaEntrada')->default("");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aplicaciones');
        Schema::dropIfExists('checklist_confpro_registros');
        Schema::dropIfExists('checklist_regcalidad_registros');
    }
}
