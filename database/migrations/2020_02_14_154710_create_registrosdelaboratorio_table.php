<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistrosdelaboratorioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registrosdelaboratorio', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('linea_id')->unsigned();
            $table->foreign('linea_id')->references('id')->on('lineas');
            $table->bigInteger('zona_id')->unsigned();
            $table->foreign('zona_id')->references('id')->on('zonasdelinea');
            $table->string('numero_pieza');
            $table->string('catalogo');
            $table->string('nombre_pieza');
            $table->dateTime('fecha');
            $table->string('estado')->default('pendiente');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registrosdelaboratorio');
    }
}
