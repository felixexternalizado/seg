<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChecklistCambiocatalogoRegistrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checklist_cambiocatalogo_registros', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cambiocatalogo_id')->unsigned();
            $table->foreign('cambiocatalogo_id')->references('id')->on('cambiocatalogo');
            $table->integer('indice')->default(0);
            $table->string('valor')->default("");
            $table->string('fase')->default("");
            $table->string('foto')->default("");
            $table->string('descripcion')->default("");
            $table->string('operacion')->default("");
            $table->string('comentario')->default("");
            $table->string('noconformidad')->default("");
            $table->string('limite1')->default("");
            $table->string('limite2')->default("");
            $table->string('tipo')->default("");
            $table->string('instrucciones')->default("");
            $table->string('validador')->default("");
            $table->string('opciones')->default("");
            $table->string('requerido')->default("");
            $table->string('valormanual')->default("");
            $table->tinyInteger('ncFlag')->default(0);
            $table->string('horaEntrada')->default("");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checklist_cambiocatalogo_registros');
    }
}
