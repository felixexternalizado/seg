<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUserTable20191218 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_job_id_foreign');
            $table->dropIndex('users_job_id_foreign');
            $table->dropColumn('job_id');
            $table->longText('aplicaciones')->nullable()->change();
            $table->dropUnique('name');
            $table->string('name', 50)->change();
            $table->string('email', 50)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
