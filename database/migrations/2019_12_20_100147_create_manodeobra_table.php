<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManodeobraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manodeobra', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('linea_id')->unsigned();
            $table->foreign('linea_id')->references('id')->on('lineas');
            $table->bigInteger('zona_id')->unsigned();
            $table->foreign('zona_id')->references('id')->on('zonasdelinea');
            $table->bigInteger('empleado_id')->unsigned();
            $table->foreign('empleado_id')->references('id')->on('empleados');
            $table->string('fecha');
            $table->string('turno');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manodeobra');
    }
}
