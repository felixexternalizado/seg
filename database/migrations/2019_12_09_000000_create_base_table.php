<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

		Schema::create('dias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('dia');
			$table->integer('semana');
			$table->integer('mes');
			$table->integer('anno');
            //$table->timestamps();
        });
		Schema::create('empleados', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
			$table->string('resumen');
            $table->timestamps();
        });

		Schema::create('cargos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('resumen');
            $table->timestamps();
        });

		Schema::create('lineas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('resumen');
            $table->timestamps();
        });

		Schema::create('procesos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('lineas_id')->unsigned();
            $table->string('resumen');
            $table->timestamps();
        });

		Schema::table('procesos', function (Blueprint $table) {
			$table->foreign('lineas_id')->references('id')->on('lineas');
		 });

		Schema::create('cargoslineasempleados', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cargos_id')->unsigned();
            $table->bigInteger('lineas_id')->unsigned();
            $table->bigInteger('empleados_id')->unsigned();
			$table->timestamps();

        });
		Schema::table('cargoslineasempleados', function (Blueprint $table) {
			$table->foreign('cargos_id')->references('id')->on('cargos');
			$table->foreign('lineas_id')->references('id')->on('lineas');
			$table->foreign('empleados_id')->references('id')->on('empleados');
		 });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		 Schema::dropIfExists('dias');
		 Schema::dropIfExists('empleados');
		 Schema::dropIfExists('cargos');
		 Schema::dropIfExists('lineas');
		 Schema::dropIfExists('cargoslineasempleados');
		 Schema::dropIfExists('procesos');

    }
}
