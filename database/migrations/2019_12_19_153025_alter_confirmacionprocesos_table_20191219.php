<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterConfirmacionprocesosTable20191219 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('confirmacionprocesos', function (Blueprint $table) {
            $table->integer('totalMal')->after('porcentajefalloreal');
            $table->integer('totalBien')->after('totalMal');
            $table->integer('porcentajeBien')->after('totalBien');
            $table->integer('totalPreguntas')->after('porcentajeBien');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
