<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBancosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bancoresumen', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fecha');
            $table->string('banco');
            $table->string('carpeta');
            $table->string('catalogo');
            $table->string('inicio');
            $table->string('fin');
            $table->integer('nPruebas');
            $table->integer('ok');
            $table->integer('okPer');
            $table->integer('ko');
            $table->integer('nOkPer');
            $table->timestamps();
        });
        Schema::create('bancopruebas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('paso');
            $table->string('descripcion');
            $table->integer('etapa');
            $table->integer('minimo');
            $table->integer('maximo');
            $table->integer('media');
            $table->integer('total');
            $table->integer('nOk');
            $table->integer('nOkPer');
            $table->string('carpeta');
            $table->string('banco');
            $table->timestamps();
        });
        Schema::create('bancopasos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha');
            $table->time('inicio');
            $table->string('dmc')->nullable();
            $table->integer('paso');
            $table->string('descripcion')->nullable();
            $table->integer('etapa');
            $table->integer('minimo');
            $table->integer('maximo');
            $table->integer('media');
            $table->integer('total');
            $table->integer('nOk');
            $table->integer('nOkPer');
            $table->string('vV1')->nullable();
            $table->string('liV1')->nullable();
            $table->string('lsV1')->nullable();
            $table->string('vV2')->nullable();
            $table->string('sV2')->nullable();
            $table->string('liV2')->nullable();
            $table->string('lsV2')->nullable();
            $table->string('vV3')->nullable();
            $table->string('sV3')->nullable();
            $table->string('liV3')->nullable();
            $table->string('lsV3')->nullable();
            $table->string('vV4')->nullable();
            $table->string('sV4')->nullable();
            $table->string('liV4')->nullable();
            $table->string('lsV4')->nullable();
            $table->string('carpeta');
            $table->string('banco');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bancoresumen');
        Schema::dropIfExists('bancopruebas');
        Schema::dropIfExists('bancopasos');
    }
}
