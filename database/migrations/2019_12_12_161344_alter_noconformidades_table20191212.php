<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterNoconformidadesTable20191212 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('noconformidades', function (Blueprint $table) {
            $table->bigInteger('confirmacionProcesos_id')->after('id')->unsigned()->nullable();
            $table->foreign('confirmacionProcesos_id')->references('id')->on('confirmacionprocesos');
            $table->bigInteger('registrocalidad_id')->after('confirmacionProcesos_id')->unsigned()->nullable();
            $table->foreign('registrocalidad_id')->references('id')->on('registrosdecalidad');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
