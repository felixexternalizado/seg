<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistrosdecalidadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registrosdecalidad', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('linea_id')->unsigned();
            $table->foreign('linea_id')->references('id')->on('lineas');
            $table->bigInteger('maquina_id')->unsigned()->nullable();
            $table->foreign('maquina_id')->references('id')->on('maquinas');
            $table->bigInteger('procesocalidad_id')->unsigned();
            $table->foreign('procesocalidad_id')->references('id')->on('procesoscalidad');
            $table->bigInteger('empleados_id')->nullable()->unsigned();
            $table->foreign('empleados_id')->references('id')->on('empleados');
            $table->string('resumen');
            $table->string('valor');
            $table->bigInteger('evaluador')->unsigned();
            $table->foreign('evaluador')->references('id')->on('empleados');
            $table->string('fechaInicio');
            $table->string('fechaFinal');
            $table->integer('totalMal');
            $table->integer('totalBien');
            $table->integer('porcentajeBien');
            $table->integer('totalPreguntas');
            $table->string('estado')->default('borrador');
            $table->json('json')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registrosdecalidad');
    }
}
