<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOptiomizacionCosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('optimizacioncos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('numero_orden')->nullable()->comment('order_number');
            $table->string('estado')->nullable()->comment('status');
            $table->integer('numero_pieza')->nullable()->comment('part_number');
            $table->string('orden_venta')->nullable()->comment('sales_order');
            $table->string('cliente')->nullable()->comment('customer');
            $table->string('tipo_plataforma')->nullable()->comment('type_platform');
            $table->string('tipo')->nullable()->comment('type');
            $table->string('tipo_muestra')->nullable()->comment('type_sample');
            $table->string('orden_cantidad_total')->nullable()->comment('total_quantity_order');
            $table->integer('cantidad_enviada')->nullable()->comment('delivered_quantity');
            $table->integer('cantidad_pendiente_entrega')->nullable()->comment('quantity_pending_deliver');
            $table->string('kit_termopar')->nullable()->comment('kit_thermocouple');
            $table->text('observacion_orden')->nullable()->comment('order_remarks');
            $table->string('incoterm')->nullable()->comment('incoterm');
            $table->text('detalles_envio')->nullable()->comment('details_sending');
            $table->date('fecha_llegada_SGES_COS')->nullable()->comment('arrival_date_SGES_COS');
            $table->string('kwt')->nullable()->comment('kwt');
            $table->date('fecha_envio_confirmada_SGES_COS')->nullable()->comment('shipping_date_confirmed_SGES_COS');
            $table->date('fecha_envio_modificada')->nullable()->comment('shipping_date_modified');
            $table->date('fecha_entrega_real_COST_CLP')->nullable()->comment('real_delivery_date_COST_CLP');
            $table->date('fecha_envio_real_exTRETO')->nullable()->comment('real_shipping_date_exTRETO');
            $table->date('fecha_maxima_entrega')->nullable()->comment('fecha_maxima_entrega');
            $table->string('pps_ckd_montaje')->nullable()->comment('pps_ckd_montaje');
            $table->string('ok_logistico')->nullable()->comment('ok_logistico');
            $table->string('ok_tecnico')->nullable()->comment('ok_tecnico');
            $table->text('internal_remarks_COS')->nullable()->comment('internal_remarks_COS');
            $table->string('revisar')->nullable()->comment('revisar');
            $table->string('mes_tomado_medir_cumplimentacion')->nullable()->comment('mes_tomado_medir_cumplimentacion');
            $table->string('otd')->nullable()->comment('otd');
            $table->text('cumplimentacion_motivos_atraso')->nullable()->comment('cumplimentacion_motivos_atraso');
            $table->string('desviacion_tecnica')->nullable()->comment('desviacion_tecnica');
            $table->text('motivos_comentarios')->nullable()->comment('motivos_comentarios');
            $table->text('medida_correctiva')->nullable()->comment('medida_correctiva');
            $table->string('quien')->nullable()->comment('quien');
            $table->string('actions')->nullable()->comment('actions');
            $table->string('ok_logistico_rotor')->nullable()->comment('ok_logistico_rotor');
            $table->string('ok_tecnico_rotor')->nullable()->comment('ok_tecnico_rotor');
            $table->text('comentarios_internos_rotor')->nullable()->comment('comentarios_internos_rotor');
            $table->string('stock_rotor')->nullable()->comment('stock_rotor');
            $table->integer('cantidad_necesaria_rotor')->nullable()->comment('cantidad_necesaria_rotor');
            $table->date('fecha_maxima_entrega_rotor')->nullable()->comment('fecha_maxima_entrega_rotor');
            $table->text('rotor_disponible_notificado_trasladado')->nullable()->comment('rotor_disponible_notificado_trasladado');
            $table->string('ok_logistico_stator')->nullable()->comment('ok_logistico_stator');
            $table->string('ok_tecnico_stator')->nullable()->comment('ok_tecnico_stator');
            $table->text('comentarios_internos_stator')->nullable()->comment('comentarios_internos_stator');
            $table->string('stock_stator')->nullable()->comment('stock_stator');
            $table->integer('cantidad_necesaria_stator')->nullable()->comment('cantidad_necesaria_stator');
            $table->date('fecha_maxima_entrega_stator')->nullable()->comment('fecha_maxima_entrega_stator');
            $table->string('stator_disponible_notificado_trasladado')->nullable()->comment('stator_disponible_notificado_trasladado');
            $table->string('ok_logistico_rectifier')->nullable()->comment('ok_logistico_rectifier');
            $table->string('ok_tecnico_rectifier')->nullable()->comment('ok_tecnico_rectifier');
            $table->text('comentarios_internos_rectifier')->nullable()->comment('comentarios_internos_rectifier');
            $table->string('stock_rectifier')->nullable()->comment('stock_rectifier');
            $table->integer('cantidad_necesaria_rectifier')->nullable()->comment('cantidad_necesaria_rectifier');
            $table->date('fecha_maxima_entrega_rectifier')->nullable()->comment('fecha_maxima_entrega_rectifier');
            $table->string('rectifier_disponible_notificado_trasladado')->nullable()->comment('rectifier_disponible_notificado_trasladado');
            $table->string('ok_logistico_ecu')->nullable()->comment('ok_logistico_ecu');
            $table->string('ok_tecnico_ecu')->nullable()->comment('ok_tecnico_ecu');
            $table->text('comentarios_internos_ecu')->nullable()->comment('comentarios_internos_ecu');
            $table->string('stock_ecu')->nullable()->comment('stock_ecu');
            $table->integer('cantidad_necesaria_ecu')->nullable()->comment('cantidad_necesaria_ecu');
            $table->date('fecha_maxima_entrega_ecu')->nullable()->comment('fecha_maxima_entrega_ecu');
            $table->text('ecu_disponible_notificado_trasladado')->nullable()->comment('ecu_disponible_notificado_trasladado');
            $table->string('ok_logistico_regulator_brush_holder')->nullable()->comment('ok_logistico_regulator_brush_holder');
            $table->string('ok_tecnico_regulator_brush_holder')->nullable()->comment('ok_tecnico_regulator_brush_holder');
            $table->text('comentarios_internos_regulator_brush_holder')->nullable()->comment('comentarios_internos_regulator_brush_holder');
            $table->string('stock_regulator_brush_holder')->nullable()->comment('stock_regulator_brush_holder');
            $table->integer('cantidad_necesaria_regulator_brush_holder')->nullable()->comment('cantidad_necesaria_regulator_brush_holder');
            $table->date('fecha_maxima_entrega_regulator_brush_holder')->nullable()->comment('fecha_maxima_entrega_regulator_brush_holder');
            $table->text('regulator_brush_holder_disponible_notificado_trasladado')->nullable()->comment('regulator_brush_holder_disponible_notificado_trasladado');
            $table->string('ok_logistico_drive_end_shield')->nullable()->comment('ok_logistico_drive_end_shield');
            $table->string('ok_tecnico_drive_end_shield')->nullable()->comment('ok_tecnico_drive_end_shield');
            $table->text('comentarios_internos_drive_end_shield')->nullable()->comment('comentarios_internos_drive_end_shield');
            $table->string('stock_drive_end_shield')->nullable()->comment('stock_drive_end_shield');
            $table->integer('cantidad_necesaria_drive_end_shield')->nullable()->comment('cantidad_necesaria_drive_end_shield');
            $table->date('fecha_maxima_entrega_drive_end_shield')->nullable()->comment('fecha_maxima_entrega_drive_end_shield');
            $table->string('drive_end_shield_disponible_notificado_trasladado')->nullable()->comment('drive_end_shield_disponible_notificado_trasladado');
            $table->string('ok_logistico_slip_ring_end_shield')->nullable()->comment('ok_logistico_slip_ring_end_shield');
            $table->string('ok_tecnico_slip_ring_end_shield')->nullable()->comment('ok_tecnico_slip_ring_end_shield');
            $table->text('comentarios_internos_slip_ring_end_shield')->nullable()->comment('comentarios_internos_slip_ring_end_shield');
            $table->integer('cantidad_necesaria_slip_ring_end_shield')->nullable()->comment('cantidad_necesaria_slip_ring_end_shield');
            $table->date('fecha_maxima_entrega_slip_ring_end_shield')->nullable()->comment('fecha_maxima_entrega_slip_ring_end_shield');
            $table->string('slip_ring_end_shield_disponible_notificado_trasladado')->nullable()->comment('slip_ring_end_shield_disponible_notificado_trasladado');
            $table->string('ok_logistico_protective_cap')->nullable()->comment('ok_logistico_protective_cap');
            $table->string('ok_tecnico_protective_cap')->nullable()->comment('ok_tecnico_protective_cap');
            $table->text('comentarios_internos_protective_cap')->nullable()->comment('comentarios_internos_protective_cap');
            $table->string('stock_protective_cap')->nullable()->comment('stock_protective_cap');
            $table->integer('cantidad_necesaria_protective_cap')->nullable()->comment('cantidad_necesaria_protective_cap');
            $table->date('fecha_maxima_entrega_protective_cap')->nullable()->comment('fecha_maxima_entrega_protective_cap');
            $table->text('protective_cap_disponible_notificado_trasladado')->nullable()->comment('protective_cap_disponible_notificado_trasladado');
            $table->string('ok_logistico_pulley')->nullable()->comment('ok_logistico_pulley');
            $table->string('ok_tecnico_pulley')->nullable()->comment('ok_tecnico_pulley');
            $table->text('comentarios_internos_pulley')->nullable()->comment('comentarios_internos_pulley');
            $table->string('stock_pulley')->nullable()->comment('stock_pulley');
            $table->integer('cantidad_necesaria_pulley')->nullable()->comment('cantidad_necesaria_pulley');
            $table->date('fecha_maxima_entrega_pulley')->nullable()->comment('fecha_maxima_entrega_pulley');
            $table->text('pulley_disponible_notificado_trasladado')->nullable()->comment('pulley_disponible_notificado_trasladado');
            $table->string('ok_logistico_pulley_cap')->nullable()->comment('ok_logistico_pulley_cap');
            $table->string('ok_tecnico_pulley_cap')->nullable()->comment('ok_tecnico_pulley_cap');
            $table->text('comentarios_internos_pulley_cap')->nullable()->comment('comentarios_internos_pulley_cap');
            $table->string('stock_pulley_cap')->nullable()->comment('stock_pulley_cap');
            $table->integer('cantidad_necesaria_pulley_cap')->nullable()->comment('cantidad_necesaria_pulley_cap');
            $table->date('fecha_maxima_entrega_pulley_cap')->nullable()->comment('fecha_maxima_entrega_pulley_cap');
            $table->string('pulley_cap_disponible_notificado_trasladado')->nullable()->comment('pulley_cap_disponible_notificado_trasladado');
            $table->string('ok_logistico_bearing')->nullable()->comment('ok_logistico_bearing');
            $table->string('ok_tecnico_bearing')->nullable()->comment('ok_tecnico_bearing');
            $table->text('comentarios_internos_bearing')->nullable()->comment('comentarios_internos_bearing');
            $table->string('stock_bearing')->nullable()->comment('stock_bearing');
            $table->integer('cantidad_necesaria_bearing')->nullable()->comment('cantidad_necesaria_bearing');
            $table->date('fecha_maxima_entrega_bearing')->nullable()->comment('fecha_maxima_entrega_bearing');
            $table->string('bearing_disponible_notificado_trasladado')->nullable()->comment('bearing_disponible_notificado_trasladado');
            $table->string('ok_logistico_others')->nullable()->comment('ok_logistico_others');
            $table->string('ok_tecnico_others')->nullable()->comment('ok_tecnico_others');
            $table->text('comentarios_internos_others')->nullable()->comment('comentarios_internos_others');
            $table->string('stock_others')->nullable()->comment('stock_others');
            $table->integer('cantidad_necesaria_others')->nullable()->comment('cantidad_necesaria_others');
            $table->date('fecha_maxima_entrega_others')->nullable()->comment('fecha_maxima_entrega_others');
            $table->text('others_disponible_notificado_trasladado')->nullable()->comment('others_disponible_notificado_trasladado');
            $table->string('ok_logistico_connection_plate_isolation_part')->nullable()->comment('ok_logistico_connection_plate_isolation_part');
            $table->string('ok_tecnico_connection_plate_isolation_part')->nullable()->comment('ok_tecnico_connection_plate_isolation_part');
            $table->text('comentarios_internos_connection_plate_isolation_part')->nullable()->comment('comentarios_internos_connection_plate_isolation_part');
            $table->string('stock_connection_plate_isolation_part')->nullable()->comment('stock_connection_plate_isolation_part');
            $table->integer('cantidad_necesaria_connection_plate_isolation_part')->nullable()->comment('cantidad_necesaria_connection_plate_isolation_part');
            $table->date('fecha_maxima_entrega_connection_plate_isolation_part')->nullable()->comment('fecha_maxima_entrega_connection_plate_isolation_part');
            $table->text('connection_plate_isolation_part_disponible_notificado_trasladado')->nullable()->comment('connection_plate_isolation_part_disponible_notificado_trasladado');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('optiomizacioncos');
    }
}
