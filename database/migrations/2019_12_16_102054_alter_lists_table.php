<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lists', function (Blueprint $table) {
            $table->bigInteger('procesos_id')->nullable()->unsigned()->after('empleados_id');
            $table->bigInteger('lineas_id')->nullable()->unsigned()->after('procesos_id');
        });

        Schema::table('lists', function (Blueprint $table) {
            $table->foreign('procesos_id')->references('id')->on('procesos');
            $table->foreign('lineas_id')->references('id')->on('lineas');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
