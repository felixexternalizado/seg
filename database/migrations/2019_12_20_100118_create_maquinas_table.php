<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaquinasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maquinas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('linea_id')->unsigned();
            $table->foreign('linea_id')->references('id')->on('lineas');
            $table->bigInteger('zona_id')->unsigned();
            $table->foreign('zona_id')->references('id')->on('zonasdelinea');
            $table->string('numero_serie');
            $table->string('referencia');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maquinas');
    }
}
