<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->bigInteger('empleados_id')->after('password')->unsigned()->nullable();
            $table->bigInteger('creator_id')->after('empleados_id')->unsigned()->nullable();
            $table->boolean('admin')->after('job_id')->nullable()->default(false);
            $table->dropForeign('users_job_id_foreign');
            $table->dropIndex('users_job_id_foreign');
            $table->longText('aplicaciones')->nullable()->change();
        });
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('empleados_id')->references('id')->on('empleados');
            $table->foreign('creator_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
