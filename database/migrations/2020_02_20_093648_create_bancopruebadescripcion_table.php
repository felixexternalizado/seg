<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBancopruebadescripcionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bancopruebadescripcion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('dmc')->nullable();
            $table->string('resultado')->nullable();
            $table->integer('nPruebas')->nullable();
            $table->integer('nMq')->nullable();
            $table->dateTime('fecha')->nullable();
            $table->string('catalogo')->nullable();
            $table->string('sAislamiento')->nullable();
            $table->string('sAcoplamiento')->nullable();
            $table->string('vPicoTension')->nullable();
            $table->string('sPicoTension')->nullable();
            $table->string('pPicoTension')->nullable();
            $table->string('rPicoTension')->nullable();
            $table->string('carpeta')->nullable();
            $table->string('banco')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bancopruebadescripcion');
    }
}
