<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterNoconformidadesTable20191216 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('noconformidades', function (Blueprint $table) {
            $table->text('fase')->nullable()->change();
            $table->text('resumen')->nullable()->change();
            $table->text('descripcion')->nullable()->change();
            $table->text('valor')->nullable()->change();
            $table->text('comentario')->nullable()->change();
            $table->text('fechaInicio')->nullable()->change();
            $table->text('fechaFinal')->nullable()->change();
            $table->text('empleados_id')->nullable()->change();
            $table->text('evaluador')->nullable()->change();
            $table->text('actuaciones')->nullable()->change();
            $table->string('estado')->default('hacer')->change();
            $table->text('opl')->nullable()->after('actuaciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
