<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfirmacionProcesos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


		Schema::create('confirmacionProcesos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('procesos_id')->unsigned();
            $table->string('resumen');
			$table->string('evaluador');
			$table->string('fechaInicio');
			$table->string('fechaFinal');
			$table->string('plantilla');
			$table->string('version');
			$table->string('porcentajefallopermitido');
			$table->string('porcentajefalloreal');
			$table->string('linea');
			$table->string('estado')->default('borrador');
            $table->timestamps();


        });

		Schema::table('confirmacionProcesos', function (Blueprint $table) {
			 $table->foreign('procesos_id')->references('id')->on('procesos');
		});

		Schema::create('confirmacionProcesosPreguntas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('confirmacionProcesos_id')->unsigned();
			$table->string('fase');
			$table->string('resumen');
			$table->string('descripcion');
			$table->string('tipo');
			$table->string('valor');
			$table->string('comentario');
			$table->string('foto');
			$table->string('evaluador');
			$table->string('hora');
			$table->string('condicion');
			$table->string('limite1');
			$table->string('limite2');
			$table->string('reglamento');
			$table->bigInteger('noConformidades_id')->unsigned();
            $table->timestamps();
        });
		Schema::table('confirmacionProcesosPreguntas', function (Blueprint $table) {
			$table->foreign('confirmacionProcesos_id')->references('id')->on('confirmacionProcesos');
		 });

		Schema::create('noConformidades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fase');
			$table->string('resumen');
			$table->string('descripcion');
			$table->string('valor');
			$table->string('comentario');
			$table->string('fechaInicio');
			$table->string('fechaFinal');
			$table->string('empleados_id'); //responsable
			$table->string('evaluador');
			$table->string('actuaciones');
			$table->string('estado')->default('borrador');
			$table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

		 Schema::dropIfExists('confirmacionDeProcesos');
		 Schema::dropIfExists('confirmacionDeProcesosPreguntas');
		 Schema::dropIfExists('noConformidades');

    }
}
