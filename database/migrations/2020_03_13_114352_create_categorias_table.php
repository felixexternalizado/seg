<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categorias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titulo')->nullable()->default('');
            $table->string('icono')->nullable()->default('');
            $table->string('color')->nullable()->default('');
            $table->timestamps();
        });
        Schema::table('aplicaciones', function (Blueprint $table) {
            $table->renameColumn('nombre', 'titulo');
        });
        Schema::table('aplicaciones', function (Blueprint $table) {
            $table->bigInteger('categoria_id')->after('id')->unsigned();
            $table->foreign('categoria_id')->references('id')->on('categorias');
            $table->string('enlace')->after('titulo')->nullable();
        });
        Schema::create('aplicaciones_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('aplicacion_id')->unsigned();
            $table->foreign('aplicacion_id')->references('id')->on('aplicaciones');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categorias');
    }
}
