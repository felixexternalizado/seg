<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ExecuteSqls20200123 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('checklist_cambiocatalogo_registros', function (Blueprint $table) {
            $table->string('condicional')->nullable()->default('')->after('opciones');
        });
        Schema::dropIfExists('lists');
        Schema::dropIfExists('jobs');
        Schema::dropIfExists('lines');
        Schema::dropIfExists('seguridadcargo');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
