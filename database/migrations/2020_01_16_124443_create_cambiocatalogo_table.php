<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCambiocatalogoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cambiocatalogo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('empleado_id')->unsigned()->after('id');
            $table->foreign('empleado_id')->references('id')->on('empleados');
            $table->string('alternador');
            $table->string('fechaFinal');
            $table->integer('totalMal');
            $table->integer('totalBien');
            $table->integer('porcentajeBien');
            $table->integer('totalPreguntas');
            $table->string('estado')->default('borrador');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cambiocatalogo');
    }
}
