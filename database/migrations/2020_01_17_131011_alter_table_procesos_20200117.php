<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableProcesos20200117 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('procesos', function (Blueprint $table) {
            $table->string('url')->after('resumen');
            $table->string('carpeta')->after('url');
        });

        Schema::table('procesoscalidad', function (Blueprint $table) {
            $table->string('url')->after('resumen');
            $table->string('carpeta')->after('url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
