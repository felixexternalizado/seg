<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Noconformidades;
use Faker\Generator as Faker;

$factory->define(Noconformidades::class, function (Faker $faker) {
    $arr_app = ['confirmacionesproceso','registroscalidad','cambiocatalogo'];
    $rand = rand(0,2);
    if($rand === 0) {
        $arr_id = \App\Confirmacionprocesos::get('id')->toArray();
        $id = $arr_id[rand(0,count($arr_id)-1)];
    } elseif($rand === 1) {
        $arr_id = \App\Registrocalidad::get('id')->toArray();
        $id = $arr_id[rand(0,count($arr_id)-1)];
    } elseif($rand === 2) {
        $arr_id = \App\CambioCatalogo::get('id')->toArray();
        $id = $arr_id[rand(0,count($arr_id)-1)];
    }

    return [
        'aplicacion_id' => $id['id'],
        'fase' => 'Inicio',
        'resumen' => $faker->text,
        'descripcion' => $faker->text,
        'valor'  => $faker->text,
        'comentario' => $faker->text,
        'fechaInicio'  => $faker->date(),
        'fechaFinal' => $faker->date(),
        'empleados_id' => 1,
        'evaluador' => 2,
        'actuaciones' => '-',
        'opl' => $arr_app[$rand],
        'estado' => 'planificar',
    ];
});
