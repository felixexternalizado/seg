<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\OptimizacionCos;
use Faker\Generator as Faker;

$factory->define(OptimizacionCos::class, function (Faker $faker) {
    $arr_x = ['', 'X'];
    rand(0, 1);

    return [
        'numero_orden' => $faker->randomNumber(),
        'estado' => $faker->word,
        'numero_pieza' => $faker->randomNumber(),
        'orden_venta' => $faker->word,
        'cliente' => $faker->firstName,
        'tipo_plataforma' => 'PL',
        'tipo' => 'ROTOR PL7QPM-FE',
        'tipo_muestra' => 'C',
        'orden_cantidad_total' => $faker->randomNumber(),
        'cantidad_enviada' => $faker->randomNumber(),
        'cantidad_pendiente_entrega' => $faker->randomNumber(),
        'kit_termopar' => '',
        'observacion_orden' => $faker->text,
        'incoterm' => 'FCA',
        'detalles_envio' => $faker->text,
        'fecha_llegada_SGES_COS' => $faker->date(),
        'kwt' => $faker->date(),
        'fecha_envio_confirmada_SGES_COS' => $faker->date(),
        'fecha_envio_modificada' => $faker->word,
        'fecha_entrega_real_COST_CLP' => $faker->word,
        'fecha_envio_real_exTRETO' => $faker->word,
        'fecha_maxima_entrega' => $faker->date(),
        'pps_ckd_montaje' => $faker->randomLetter,
        'ok_logistico' => $faker->randomLetter,
        'ok_tecnico' => $faker->randomLetter,
        'internal_remarks_COS' => $faker->text,
        'revisar' => "OK",
        'ok_logistico_rotor' => $faker->randomLetter,
        'ok_tecnico_rotor' => $faker->randomLetter,
        'comentarios_internos_rotor' => $faker->text,
        'stock_rotor' => $faker->randomNumber().
            "(".$faker->randomNumber().".".$faker->randomNumber().")+".
            $faker->randomNumber().
            "(".$faker->randomNumber().".".$faker->randomNumber().")",
        'cantidad_necesaria_rotor' => $faker->randomNumber(),
        'fecha_maxima_entrega_rotor' => $faker->date(),
        'rotor_disponible_notificado_trasladado' => $faker->date(),
        'cantidad_necesaria_stator' => $faker->randomNumber(),
        'cantidad_necesaria_rectifier' => $faker->randomNumber(),
        'cantidad_necesaria_ecu' => $faker->randomNumber(),
        'cantidad_necesaria_regulator_brush_holder' => $faker->randomNumber(),
        'cantidad_necesaria_drive_end_shield' => $faker->randomNumber(),
        'cantidad_necesaria_slip_ring_end_shield' => $faker->randomNumber(),
        'cantidad_necesaria_protective_cap' => $faker->randomNumber(),
        'cantidad_necesaria_pulley' => $faker->randomNumber(),
        'cantidad_necesaria_pulley_cap' => $faker->randomNumber(),
        'cantidad_necesaria_bearing' => $faker->randomNumber(),
        'mes_tomado_medir_cumplimentacion' => $faker->monthName().'-'.$faker->dayOfMonth,
        'otd' => "1"
    ];
});
