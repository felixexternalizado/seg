<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\LaboratorioRegistro;
use Faker\Generator as Faker;

$factory->define(LaboratorioRegistro::class, function (Faker $faker) {

    $arr_id = \App\Line::get('id')->toArray();
    $linea_id = $arr_id[rand(0,count($arr_id)-1)];

    $arr_zona_id = \App\Zona::where('linea_id',$linea_id)->get('id')->toArray();
    $zona_id = $arr_zona_id[rand(0,count($arr_zona_id)-1)];

    return [
        'linea_id' => $linea_id['id'],
        'zona_id' => $zona_id['id'],
        'numero_pieza' => $faker->randomNumber(),
        'catalogo' => $faker->randomNumber(),
        'nombre_pieza' => $faker->word,
        'fecha' => $faker->dateTimeThisMonth(),
        'estado' => 'pendiente'
    ];
});
