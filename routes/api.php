<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'Auth\LoginController@apiLogin');

Route::get('getjson/{tipo}/','ListController@calendario'); //Antiguo Calendario //TODO temporal, enviar token a esta url

Route::get('getjson/{tipo}/{id?}','ListController@getJson'); //Checklist //TODO temporal, enviar token a esta url

Route::get('bancos/exportExcel/', 'BancoController@exportarExcel');

Route::get('conexionSAP', 'CambioCatalogoController@conexionSAP');

Route::get('bancos/leerXML', 'BancoController@leerXML');

Route::get('flashing/leerXML', 'FlashingTraceController@leerXML');

//<editor-fold desc="GenerateWithFactories">
Route::get('factory/generarOPL', 'GenerateController@generateOPL');
Route::get('factory/generarLab', 'GenerateController@generateLab');
Route::get('factory/generarCOS', 'GenerateController@generateCOS');
//</editor-fold>



Route::group(['middleware' => 'dms'], function(){

    Route::resource('/configuracion', 'ConfigController');

    Route::resource('/permisos', 'PermisoController');

    Route::resource('/aplicaciones', 'AplicacionesController');

    Route::get('/allAplicaciones', 'AplicacionesController@sendApps');

    Route::resource('/categoriasAplicaciones', 'CategoriaController');

    Route::get('usuarios/auth', 'UserController@auth');

    Route::post('usuarios/cambiarAplicacion/{id}', 'UserController@cambiarAplicacion');

    Route::post('usuarios/cambiarcontrasena', 'UserController@changePassword');

    Route::resource('/usuarios', 'UserController');

    Route::resource('/noconformidades', 'NoconformidadesController');

    Route::resource('/confirmacionproceso', 'ConfirmacionprocesosController');

    Route::post('confirmacionproceso/repetirPlanificacion/{confpr}', 'ConfirmacionprocesosController@repetirPlanificacion');

    Route::get('lineas/infolinea/{linea}', 'LineController@infoLinea');

    Route::get('lineas/infolineas', 'LineController@infoLineas');

    Route::resource('/lineas', 'LineController');

    Route::get('procesos/{line}/{anio}', 'ListController@trabajadores'); //Planificador

    Route::resource('/procesos', 'ProcesosController');

    Route::resource('/procesoscalidad', 'ProcesosCalidadController');

    Route::resource('/empleados', 'EmpleadosController');

    Route::resource('/registroscalidad', 'RegistrocalidadController');

    Route::resource('/cambiocatalogo', 'CambioCatalogoController');

    Route::resource('/maquinas' , 'MaquinaController');

    Route::resource('/zonas' , 'ZonaController');

    Route::resource('/manosdeobra' , 'ManodeobraController');

    Route::resource('/cargos' , 'CargosController');

    Route::resource('/seguridadcargo', 'CargoslineasEmpleadosController');

    Route::get('listas/{user}/{anio}', 'ListController@listaPorUsuario');

    Route::get('recursos' , 'RecursosController@enviarRecursos');

    Route::post('sendFoto/{id}/', 'ListController@sendFoto');

    Route::get('sendFase/{tipo}/', 'ListController@sendFase');

    Route::put('guardarRegistro/{tipo}/{id}', 'ListController@guardarRegistro');

    Route::post('guardarRegistros/{tipo}', 'ListController@guardarMultiplesRegistros');

    Route::delete('borrarChecklist/{tipo}/{id}', 'ListController@borrarChecklist');

    Route::get('finalizarChecklist/{tipo}/{id}', 'ListController@finalizarChecklist');

    Route::get('getRegistrosAplicacion/{tipo}', 'ListController@getRegistrosAplicacion'); //Nuevo Calendario

    Route::get('getRegistroAplicacion/{tipo}/{id}', 'ListController@getRegistroAplicacion'); //Ver un registro del calendario

    Route::get('ejecutar/laboratorio', 'LaboratorioRegistroController@piezasEnviadas');

    Route::resource('/registroslaboratorio', 'LaboratorioRegistroController');

    //<editor-fold desc="Flashing">
    Route::get('flashing/carpetas', 'FlashingController@carpetas');

    Route::post('flashing/crearLinea', 'FlashingController@crearLinea');

    Route::delete('flashing/eliminarLinea', 'FlashingController@EliminarLinea');

    Route::get('flashing/{carpeta}/piezas', 'FlashingController@catalogos');

    Route::get('flashing/{carpeta}/{pieza}/entornos', 'FlashingController@entornos');

    Route::get('flashing/{linea}/{catalogo}/{entorno}/{secuencia}/archivos', 'FlashingController@archivos');

    Route::get('flashing/{linea}/{catalogo}/{entorno}/{secuencia}/{archivo}', 'FlashingController@contenidoArchivo');

    Route::post('flashing/{linea}/{catalogo}/{entorno}/{secuencia}/{archivo}', 'FlashingController@guardarArchivo');

    Route::post('flashing/nuevaPieza', 'FlashingController@nuevoCatalogo');

    Route::post('flashing/copiarCatalogo/{orig}', 'FlashingController@copiarCatalogo');

    Route::post('flashing/renombrarCatalogo/{orig}', 'FlashingController@renombrarCatalogo');

    Route::delete('flashing/eliminarCatalogo', 'FlashingController@eliminarCatalogo');

    Route::post('flashing/cambiarEntorno', 'FlashingController@cambiarEntorno');

    Route::post('flashing/desactivar', 'FlashingController@desactivar');

    Route::post('flashing/duplicarVersion/{orig}', 'FlashingController@duplicarVersion');

    Route::post('flashing/nuevaVersion', 'FlashingController@nuevaVersion');

    Route::delete('flashing/eliminarVersion/{carpeta}', 'FlashingController@eliminarVersion');

    Route::get('visorflashing/config', 'FlashingTraceController@config');
    
    Route::get('visorflashing/{banco?}/{carpeta?}/{paso?}/{fecha?}', 'FlashingTraceController@index');

    Route::resource('/flashing', 'FlashingController');
    //</editor-fold>

    Route::resource('/optimizacioncos','OptimizacionCosController');

    Route::get('bancos/config', 'BancoController@config');

    Route::get('bancos/servicio', 'BancoController@servicio');

    Route::get('{tabla}/{banco?}/{carpeta?}/{paso?}/{fecha?}', 'BancoController@index'); //EL BANCO PUEDE SER EL DMC EN EL ÚLTIMO LISTADO

    Route::resource('/bancos', 'BancoController');
});



