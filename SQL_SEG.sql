ALTER TABLE `checklist_confpro_registros`
	ADD COLUMN `condicional` VARCHAR(255) NULL DEFAULT '' AFTER `opciones`;
ALTER TABLE `checklist_regcalidad_registros`
	ADD COLUMN `condicional` VARCHAR(255) NULL DEFAULT '' AFTER `opciones`;

-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         5.7.24 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para tabla evaluacion.cambiocatalogo
CREATE TABLE IF NOT EXISTS `cambiocatalogo` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `empleado_id` bigint(20) unsigned NOT NULL,
  `alternador` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `totalMal` int(11) NOT NULL,
  `totalBien` int(11) NOT NULL,
  `porcentajeBien` int(11) NOT NULL,
  `totalPreguntas` int(11) NOT NULL,
  `estado` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'borrador',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cambiocatalogo_empleado_id_foreign` (`empleado_id`),
  CONSTRAINT `cambiocatalogo_empleado_id_foreign` FOREIGN KEY (`empleado_id`) REFERENCES `empleados` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=UTF8MB4_UNICODE_CI;


CREATE TABLE IF NOT EXISTS `checklist_cambiocatalogo_registros` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cambiocatalogo_id` bigint(20) unsigned NOT NULL,
  `indice` int(11) NOT NULL DEFAULT '0',
  `valor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `fase` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `operacion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comentario` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `noconformidad` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `limite1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `limite2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tipo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `instrucciones` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `validador` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `opciones` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `requerido` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `valormanual` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ncFlag` tinyint(4) NOT NULL DEFAULT '0',
  `horaEntrada` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `checklist_cambiocatalogo_cambiocatalogo_id_foreign` (`cambiocatalogo_id`),
  CONSTRAINT `checklist_cambiocatalogo_cambiocatalogo_id_foreign` FOREIGN KEY (`cambiocatalogo_id`) REFERENCES `cambiocatalogo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=UTF8MB4_UNICODE_CI;

CREATE TABLE IF NOT EXISTS `documentos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cambiocatalogo_id` bigint(20) unsigned NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `urlx` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documentos_cambiocatalogo_id_foreign` (`cambiocatalogo_id`),
  CONSTRAINT `documentos_cambiocatalogo_id_foreign` FOREIGN KEY (`cambiocatalogo_id`) REFERENCES `cambiocatalogo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `procesos`
	ADD COLUMN `url` VARCHAR(255) NULL DEFAULT '' AFTER `resumen`;
ALTER TABLE `procesos`
	ADD COLUMN `carpeta` VARCHAR(255) NULL DEFAULT '' AFTER `url`;

ALTER TABLE `procesoscalidad`
	ADD COLUMN `url` VARCHAR(255) NULL DEFAULT '' AFTER `resumen`;
ALTER TABLE `procesoscalidad`
	ADD COLUMN `carpeta` VARCHAR(255) NULL DEFAULT '' AFTER `url`;

UPDATE procesos SET carpeta = confirmacion_procesos;
UPDATE procesoscalidad SET carpeta = registros_calidad;

UPDATE procesos SET url = CONCAT(LOWER(REPLACE(resumen,' ','_')),'.xlsx');
UPDATE procesoscalidad SET url = CONCAT(LOWER(REPLACE(resumen,' ','_')),'.xlsx');

/***********************************************************************************/
/*																											  */
/*													20-01-2020											  */
/*																											  */
/***********************************************************************************/

ALTER TABLE users DROP INDEX users_name_unique;

/***********************************************************************************/
/*																											  */
/*													21-01-2020											  */
/*																											  */
/***********************************************************************************/

ALTER TABLE cambiocatalogo ADD COLUMN fechaFinal VARCHAR(255) NOT NULL AFTER `alternador`;

UPDATE noconformidades SET confirmacionProcesos_id = registrocalidad_id WHERE confirmacionProcesos_id IS NULL;

ALTER TABLE noconformidades DROP FOREIGN KEY noconformidades_registrocalidad_id_foreign;
ALTER TABLE noconformidades DROP INDEX noconformidades_registrocalidad_id_foreign;
ALTER TABLE noconformidades DROP registrocalidad_id;

ALTER TABLE noconformidades DROP FOREIGN KEY noconformidades_confirmacionprocesos_id_foreign;
ALTER TABLE noconformidades DROP INDEX noconformidades_confirmacionprocesos_id_foreign;

ALTER TABLE noconformidades CHANGE COLUMN confirmacionProcesos_id aplicacion_id BIGINT(20) UNSIGNED NULL DEFAULT NULL AFTER id;

/***********************************************************************************/
/*																											  */
/*													23-01-2020											  */
/*																											  */
/***********************************************************************************/

ALTER TABLE checklist_cambiocatalogo_registros ADD COLUMN condicional VARCHAR(255) NULL DEFAULT '' AFTER opciones;

DROP TABLE lists;
DROP TABLE jobs;
DROP TABLE `lines`;
DROP TABLE seguridadcargo;

ALTER TABLE cambiocatalogo ADD COLUMN fechaInicio VARCHAR(255) NOT NULL AFTER alternador;

/***********************************************************************************/
/*																											  */
/*													27-01-2020											  */
/*																											  */
/***********************************************************************************/

ALTER TABLE cambiocatalogo	ADD COLUMN piezasRealizadas INT(11) NOT NULL DEFAULT '0' AFTER totalPreguntas;
ALTER TABLE cambiocatalogo	ADD COLUMN piezasEsperadas INT(11) NOT NULL DEFAULT '0' AFTER piezasRealizadas;
ALTER TABLE cambiocatalogo	ADD COLUMN piezasPerdidas INT(11) NOT NULL DEFAULT '0' AFTER piezasEsperadas;
ALTER TABLE cambiocatalogo	ADD COLUMN tiempoConsumido INT(11) NOT NULL DEFAULT '0' AFTER piezasPerdidas;

/***********************************************************************************/
/*																											  */
/*													28-01-2020											  */
/*																											  */
/***********************************************************************************/


CREATE TABLE IF NOT EXISTS `bancopasos` (
  `fecha` date NOT NULL,
  `inicio` time NOT NULL,
  `id` int(11) NOT NULL,
  `dmc` varchar(50) DEFAULT NULL,
  `paso` int(11) NOT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `etapa` int(11) DEFAULT NULL,
  `minimo` float DEFAULT NULL,
  `maximo` float DEFAULT NULL,
  `media` float DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `nOk` int(11) DEFAULT NULL,
  `nOkPer` float DEFAULT NULL,
  `vV1` varchar(50) DEFAULT NULL,
  `sV1` varchar(50) DEFAULT NULL,
  `liV1` varchar(50) DEFAULT NULL,
  `lsV1` varchar(50) DEFAULT NULL,
  `vV2` varchar(50) DEFAULT NULL,
  `sV2` varchar(50) DEFAULT NULL,
  `liV2` varchar(50) DEFAULT NULL,
  `lsV2` varchar(50) DEFAULT NULL,
  `vV3` varchar(50) DEFAULT NULL,
  `sV3` varchar(50) DEFAULT NULL,
  `liV3` varchar(50) DEFAULT NULL,
  `lsV3` varchar(50) DEFAULT NULL,
  `vV4` varchar(50) DEFAULT NULL,
  `sV4` varchar(50) DEFAULT NULL,
  `liV4` varchar(50) DEFAULT NULL,
  `lsV4` varchar(50) DEFAULT NULL,
  `carpeta` varchar(250) NOT NULL,
  `banco` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `bancopasos`
	CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT AFTER `inicio`,
	ADD PRIMARY KEY (`id`);

CREATE TABLE IF NOT EXISTS `bancopruebas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `paso` int(11) NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `etapa` int(11) NOT NULL,
  `minimo` int(11) NOT NULL,
  `maximo` int(11) NOT NULL,
  `media` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `nOk` int(11) NOT NULL,
  `nOkPer` int(11) NOT NULL,
  `carpeta` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `banco` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `bancoresumen` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `fecha` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `banco` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `carpeta` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `catalogo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inicio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nPruebas` int(11) NOT NULL,
  `ok` int(11) NOT NULL,
  `okPer` int(11) NOT NULL,
  `ko` int(11) NOT NULL,
  `nOkPer` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=UTF8MB4_UNICODE_CI;

ALTER TABLE users ADD COLUMN _tokenDMS TEXT NULL DEFAULT '' AFTER aplicaciones;

ALTER TABLE noconformidades ADD COLUMN fechaCierre TEXT NULL AFTER fechaFinal;

ALTER TABLE registrosdecalidad DROP COLUMN json;
ALTER TABLE registrosdecalidad ADD COLUMN zona_id BIGINT(20) UNSIGNED NULL AFTER linea_id;
ALTER TABLE registrosdecalidad ADD CONSTRAINT registrosdecalidad_zona_id_foreign FOREIGN KEY (zona_id) REFERENCES zonasdelinea (id);

ALTER TABLE empleados ADD COLUMN default_zona_id BIGINT(20) UNSIGNED NULL AFTER id;
ALTER TABLE empleados ADD CONSTRAINT empleados_default_zona_id_foreign FOREIGN KEY (default_zona_id) REFERENCES zonasdelinea (id);

ALTER TABLE bancopasos ADD COLUMN etapaProceso VARCHAR(50) NULL DEFAULT NULL AFTER descripcion;

CREATE TABLE IF NOT EXISTS `registrosdelaboratorio` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `linea_id` bigint(20) unsigned NOT NULL,
  `numero_pieza` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `catalogo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre_pieza` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha` datetime NOT NULL,
  `estado` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pendiente',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `registrosdelaboratorio_linea_id_foreign` (`linea_id`),
  CONSTRAINT `registrosdelaboratorio_linea_id_foreign` FOREIGN KEY (`linea_id`) REFERENCES `lineas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `bancopruebadescripcion` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `dmc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resultado` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nPruebas` int(11) NOT NULL,
  `nMq` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `catalogo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sAislamiento` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sAcoplamiento` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vPicoTension` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sPicoTension` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pPicoTension` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rPicoTension` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `carpeta` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `banco` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `cambiocatalogo` ADD COLUMN `linea_id` BIGINT(20) UNSIGNED NULL AFTER `empleado_id`;
ALTER TABLE `cambiocatalogo` ADD CONSTRAINT `cambiocatalogo_linea_id_foreign` FOREIGN KEY (`linea_id`) REFERENCES `lineas` (`id`);

ALTER TABLE zonasdelinea ADD COLUMN coordenadas VARCHAR(50) NULL DEFAULT NULL AFTER resumen;
ALTER TABLE lineas ADD COLUMN coordenadas VARCHAR(50) NULL DEFAULT NULL AFTER resumen;

ALTER TABLE empleados ADD COLUMN default_linea_id BIGINT(20) UNSIGNED NULL AFTER id;
ALTER TABLE empleados ADD CONSTRAINT empleados_default_linea_id_foreign FOREIGN KEY (default_linea_id) REFERENCES lineas (id);

ALTER TABLE `confirmacionprocesos`
	ADD COLUMN `original_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL AFTER `id`,
	ADD CONSTRAINT `confirmacionprocesos_original_id_foreign` FOREIGN KEY (`original_id`) REFERENCES `confirmacionprocesos` (`id`);

ALTER TABLE `checklist_confpro_registros`
	ADD COLUMN `nombre_foto` VARCHAR(255) NOT NULL DEFAULT '' AFTER `foto`;

CREATE TABLE IF NOT EXISTS `optimizacioncos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `numero_orden` int(11) DEFAULT NULL COMMENT 'order_number',
  `estado` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'status',
  `numero_pieza` int(11) DEFAULT NULL COMMENT 'part_number',
  `orden_venta` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'sales_order',
  `cliente` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'customer',
  `tipo_plataforma` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'type_platform',
  `tipo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'type',
  `tipo_muestra` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'type_sample',
  `orden_cantidad_total` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'total_quantity_order',
  `cantidad_enviada` int(11) DEFAULT NULL COMMENT 'delivered_quantity',
  `cantidad_pendiente_entrega` int(11) DEFAULT NULL COMMENT 'quantity_pending_deliver',
  `kit_termopar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'kit_thermocouple',
  `observacion_orden` text COLLATE utf8mb4_unicode_ci COMMENT 'order_remarks',
  `incoterm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'incoterm',
  `detalles_envio` text COLLATE utf8mb4_unicode_ci COMMENT 'details_sending',
  `fecha_llegada_SGES_COS` date DEFAULT NULL COMMENT 'arrival_date_SGES_COS',
  `kwt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'kwt',
  `fecha_envio_confirmada_SGES_COS` date DEFAULT NULL COMMENT 'shipping_date_confirmed_SGES_COS',
  `fecha_envio_modificada` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'shipping_date_modified',
  `fecha_entrega_real_COS_CLP` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'real_delivery_date_COS_CLP',
  `fecha_envio_real_exTRETO` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'real_shipping_date_exTRETO',
  `fecha_maxima_entrega` date DEFAULT NULL COMMENT 'fecha_maxima_entrega',
  `pps_ckd_montaje` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'pps_ckd_montaje',
  `ok_logistico` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_logistico',
  `ok_tecnico` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_tecnico',
  `internal_remarks_COS` text COLLATE utf8mb4_unicode_ci COMMENT 'internal_remarks_COS',
  `revisar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mes_tomado_medir_cumplimentacion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'mes_tomado_medir_cumplimentacion',
  `otd` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'otd',
  `cumplimentacion_motivos_atraso` text COLLATE utf8mb4_unicode_ci COMMENT 'cumplimentacion_motivos_atraso',
  `desviacion_tecnica` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'desviacion_tecnica',
  `motivos_comentarios` text COLLATE utf8mb4_unicode_ci COMMENT 'motivos_comentarios',
  `medida_correctiva` text COLLATE utf8mb4_unicode_ci COMMENT 'medida_correctiva',
  `quien` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'quien',
  `actions` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'actions',
  `rotor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ok_logistico_rotor` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_logistico_rotor',
  `ok_tecnico_rotor` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_tecnico_rotor',
  `comentarios_internos_rotor` text COLLATE utf8mb4_unicode_ci COMMENT 'comentarios_internos_rotor',
  `stock_rotor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'stock_rotor',
  `cantidad_necesaria_rotor` int(11) DEFAULT NULL COMMENT 'cantidad_necesaria_rotor',
  `fecha_maxima_entrega_rotor` date DEFAULT NULL COMMENT 'fecha_maxima_entrega_rotor',
  `rotor_disponible_notificado_trasladado` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'rotor_disponible_notificado_trasladado',
  `stator` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ok_logistico_stator` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_logistico_stator',
  `ok_tecnico_stator` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_tecnico_stator',
  `comentarios_internos_stator` text COLLATE utf8mb4_unicode_ci COMMENT 'comentarios_internos_stator',
  `stock_stator` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'stock_stator',
  `cantidad_necesaria_stator` int(11) DEFAULT NULL COMMENT 'cantidad_necesaria_stator',
  `fecha_maxima_entrega_stator` date DEFAULT NULL COMMENT 'fecha_maxima_entrega_stator',
  `stator_disponible_notificado_trasladado` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'stator_disponible_notificado_trasladado',
  `rectifier` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ok_logistico_rectifier` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_logistico_rectifier',
  `ok_tecnico_rectifier` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_tecnico_rectifier',
  `comentarios_internos_rectifier` text COLLATE utf8mb4_unicode_ci COMMENT 'comentarios_internos_rectifier',
  `stock_rectifier` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'stock_rectifier',
  `cantidad_necesaria_rectifier` int(11) DEFAULT NULL COMMENT 'cantidad_necesaria_rectifier',
  `fecha_maxima_entrega_rectifier` date DEFAULT NULL COMMENT 'fecha_maxima_entrega_rectifier',
  `rectifier_disponible_notificado_trasladado` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'rectifier_disponible_notificado_trasladado',
  `ecu` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ok_logistico_ecu` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_logistico_ecu',
  `ok_tecnico_ecu` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_tecnico_ecu',
  `comentarios_internos_ecu` text COLLATE utf8mb4_unicode_ci COMMENT 'comentarios_internos_ecu',
  `stock_ecu` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'stock_ecu',
  `cantidad_necesaria_ecu` int(11) DEFAULT NULL COMMENT 'cantidad_necesaria_ecu',
  `fecha_maxima_entrega_ecu` date DEFAULT NULL COMMENT 'fecha_maxima_entrega_ecu',
  `ecu_disponible_notificado_trasladado` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ecu_disponible_notificado_trasladado',
  `regulator_brush_holder` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ok_logistico_regulator_brush_holder` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_logistico_regulator_brush_holder',
  `ok_tecnico_regulator_brush_holder` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_tecnico_regulator_brush_holder',
  `comentarios_internos_regulator_brush_holder` text COLLATE utf8mb4_unicode_ci COMMENT 'comentarios_internos_regulator_brush_holder',
  `stock_regulator_brush_holder` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'stock_regulator_brush_holder',
  `cantidad_necesaria_regulator_brush_holder` int(11) DEFAULT NULL COMMENT 'cantidad_necesaria_regulator_brush_holder',
  `fecha_maxima_entrega_regulator_brush_holder` date DEFAULT NULL COMMENT 'fecha_maxima_entrega_regulator_brush_holder',
  `regulator_brush_holder_disponible_notificado_trasladado` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'regulator_brush_holder_disponible_notificado_trasladado',
  `drive_end_shield` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ok_logistico_drive_end_shield` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_logistico_drive_end_shield',
  `ok_tecnico_drive_end_shield` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_tecnico_drive_end_shield',
  `comentarios_internos_drive_end_shield` text COLLATE utf8mb4_unicode_ci COMMENT 'comentarios_internos_drive_end_shield',
  `stock_drive_end_shield` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'stock_drive_end_shield',
  `cantidad_necesaria_drive_end_shield` int(11) DEFAULT NULL COMMENT 'cantidad_necesaria_drive_end_shield',
  `fecha_maxima_entrega_drive_end_shield` date DEFAULT NULL COMMENT 'fecha_maxima_entrega_drive_end_shield',
  `drive_end_shield_disponible_notificado_trasladado` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'drive_end_shield_disponible_notificado_trasladado',
  `slip_ring_end_shield` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ok_logistico_slip_ring_end_shield` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_logistico_slip_ring_end_shield',
  `ok_tecnico_slip_ring_end_shield` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_tecnico_slip_ring_end_shield',
  `comentarios_internos_slip_ring_end_shield` text COLLATE utf8mb4_unicode_ci COMMENT 'comentarios_internos_slip_ring_end_shield',
  `cantidad_necesaria_slip_ring_end_shield` int(11) DEFAULT NULL COMMENT 'cantidad_necesaria_slip_ring_end_shield',
  `fecha_maxima_entrega_slip_ring_end_shield` date DEFAULT NULL COMMENT 'fecha_maxima_entrega_slip_ring_end_shield',
  `slip_ring_end_shield_disponible_notificado_trasladado` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'slip_ring_end_shield_disponible_notificado_trasladado',
  `protective_cap` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ok_logistico_protective_cap` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_logistico_protective_cap',
  `ok_tecnico_protective_cap` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_tecnico_protective_cap',
  `comentarios_internos_protective_cap` text COLLATE utf8mb4_unicode_ci COMMENT 'comentarios_internos_protective_cap',
  `stock_protective_cap` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'stock_protective_cap',
  `cantidad_necesaria_protective_cap` int(11) DEFAULT NULL COMMENT 'cantidad_necesaria_protective_cap',
  `fecha_maxima_entrega_protective_cap` date DEFAULT NULL COMMENT 'fecha_maxima_entrega_protective_cap',
  `protective_cap_disponible_notificado_trasladado` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'protective_cap_disponible_notificado_trasladado',
  `pulley` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ok_logistico_pulley` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_logistico_pulley',
  `ok_tecnico_pulley` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_tecnico_pulley',
  `comentarios_internos_pulley` text COLLATE utf8mb4_unicode_ci COMMENT 'comentarios_internos_pulley',
  `stock_pulley` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'stock_pulley',
  `cantidad_necesaria_pulley` int(11) DEFAULT NULL COMMENT 'cantidad_necesaria_pulley',
  `fecha_maxima_entrega_pulley` date DEFAULT NULL COMMENT 'fecha_maxima_entrega_pulley',
  `pulley_disponible_notificado_trasladado` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'pulley_disponible_notificado_trasladado',
  `pulley_cap` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ok_logistico_pulley_cap` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_logistico_pulley_cap',
  `ok_tecnico_pulley_cap` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_tecnico_pulley_cap',
  `comentarios_internos_pulley_cap` text COLLATE utf8mb4_unicode_ci COMMENT 'comentarios_internos_pulley_cap',
  `stock_pulley_cap` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'stock_pulley_cap',
  `cantidad_necesaria_pulley_cap` int(11) DEFAULT NULL COMMENT 'cantidad_necesaria_pulley_cap',
  `fecha_maxima_entrega_pulley_cap` date DEFAULT NULL COMMENT 'fecha_maxima_entrega_pulley_cap',
  `pulley_cap_disponible_notificado_trasladado` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'pulley_cap_disponible_notificado_trasladado',
  `bearing` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ok_logistico_bearing` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_logistico_bearing',
  `ok_tecnico_bearing` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_tecnico_bearing',
  `comentarios_internos_bearing` text COLLATE utf8mb4_unicode_ci COMMENT 'comentarios_internos_bearing',
  `stock_bearing` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'stock_bearing',
  `cantidad_necesaria_bearing` int(11) DEFAULT NULL COMMENT 'cantidad_necesaria_bearing',
  `fecha_maxima_entrega_bearing` date DEFAULT NULL COMMENT 'fecha_maxima_entrega_bearing',
  `bearing_disponible_notificado_trasladado` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'bearing_disponible_notificado_trasladado',
  `others` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ok_logistico_others` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_logistico_others',
  `ok_tecnico_others` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_tecnico_others',
  `comentarios_internos_others` text COLLATE utf8mb4_unicode_ci COMMENT 'comentarios_internos_others',
  `stock_others` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'stock_others',
  `cantidad_necesaria_others` int(11) DEFAULT NULL COMMENT 'cantidad_necesaria_others',
  `fecha_maxima_entrega_others` date DEFAULT NULL COMMENT 'fecha_maxima_entrega_others',
  `others_disponible_notificado_trasladado` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'others_disponible_notificado_trasladado',
  `connection_plate_isolation_part` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ok_logistico_connection_plate_isolation_part` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_logistico_connection_plate_isolation_part',
  `ok_tecnico_connection_plate_isolation_part` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ok_tecnico_connection_plate_isolation_part',
  `comentarios_internos_connection_plate_isolation_part` text COLLATE utf8mb4_unicode_ci COMMENT 'comentarios_internos_connection_plate_isolation_part',
  `stock_connection_plate_isolation_part` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'stock_connection_plate_isolation_part',
  `cantidad_necesaria_connection_plate_isolation_part` int(11) DEFAULT NULL COMMENT 'cantidad_necesaria_connection_plate_isolation_part',
  `fecha_maxima_entrega_connection_plate_isolation_part` date DEFAULT NULL COMMENT 'fecha_maxima_entrega_connection_plate_isolation_part',
  `connection_plate_isolation_part_disponible_notificado_trasladado` text COLLATE utf8mb4_unicode_ci COMMENT 'connection_plate_isolation_part_disponible_notificado_trasladado',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `procesoscalidad`
	ADD COLUMN `linea_id` BIGINT(20) UNSIGNED DEFAULT NULL AFTER `resumen`,
	ADD CONSTRAINT `procesoscalidad_linea_id_foreign` FOREIGN KEY (`linea_id`) REFERENCES `lineas` (`id`);

ALTER TABLE `procesoscalidad`
	ADD COLUMN `zona_id` BIGINT(20) UNSIGNED DEFAULT NULL AFTER `linea_id`,
	ADD CONSTRAINT `procesoscalidad_zona_id_foreign` FOREIGN KEY (`zona_id`) REFERENCES `zona` (`id`);

ALTER TABLE `procesoscalidad`
	ADD COLUMN `activo` TINYINT(4)  DEFAULT 1 AFTER `zona_id`;

CREATE TABLE IF NOT EXISTS `permisos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `permisos_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `permiso_id` bigint(20) unsigned NOT NULL,
  `usuario_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permisos_users_permiso_id_foreign` (`permiso_id`),
  KEY `permisos_users_usuario_id_foreign` (`usuario_id`),
  CONSTRAINT `permisos_users_permiso_id_foreign` FOREIGN KEY (`permiso_id`) REFERENCES `permisos` (`id`),
  CONSTRAINT `permisos_users_usuario_id_foreign` FOREIGN KEY (`usuario_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
