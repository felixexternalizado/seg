<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Line extends Linea
{
    protected $table = 'lineas';

    protected $fillable = [
        'resumen','name','coordenadas'
    ];

    public function setNoConformidadesAttribute($value)
    {
        $confprs = Confirmacionprocesos::where('linea',$value)->get();
        $collectionnoconf = collect(new Noconformidades);
        foreach ($confprs as $confpr) {
            $noconfs = Noconformidades::where('aplicacion_id', $confpr->id)->where('opl', 'confirmacionprocesos')->get();
            foreach ($noconfs as $noconf) {
                $collectionnoconf->add($noconf);
            }
        }
        $this->attributes['noconformidadesactivas'] = $collectionnoconf->where('estado', '!=', 'cerrado')->count();
        $this->attributes['noconformidadestotal'] = $collectionnoconf->count();
    }

    public function cargosLineas() {
        return $this->hasMany('App\Cargoslineasempleados', 'lineas_id');
    }

    public function cargos () {
        return $this->hasMany('App\Cargoslineasempleados');
    }

    public function registrolaboratorio () {
        return $this->hasMany('App\LaboratorioRegistro', 'linea_id');
    }

    public function cambiocatalogos () {
        return $this->hasMany('App\CambioCatalogo', 'linea_id');
    }

    public function confirmacionprocesos () {
        return $this->hasMany('App\Confirmacionprocesos', 'linea');
    }

    public function registrocalidad () {
        return $this->hasMany('App\Registrocalidad', 'linea_id');
    }

    public function laboratorioregistros () {
        return $this->hasMany('App\LaboratorioRegistro', 'linea_id');
    }

    public function zonas () {
        return $this->hasMany('App\Zona', 'linea_id');
    }

}
