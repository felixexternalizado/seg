<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Procesoscalidad extends Model
{
    protected $table = 'procesoscalidad';
    protected $fillable = [
        'resumen',
        'linea_id',
        'zona_id',
        'activo',
    ];

    public function linea() {
        return $this->belongsTo('App\Line', 'linea_id');
    }

    public function zona() {
        return $this->belongsTo('App\Zona', 'zona_id');
    }

}
