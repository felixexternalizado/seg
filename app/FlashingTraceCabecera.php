<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlashingTraceCabecera extends Model
{
    protected $table = 'flashingtrace_cabecera';
    
    protected $fillable = [
        'customer',
        'part_number',
        'dmc',
        'plant',
        'manufacture_date',
        'manufacture_time',
        'name',
        'os_x0020_version',
        'dot_x0020_net_x0020_version',
        'flasher_x0020_version',
        'section',
        'reference',
        'entry_point',
        'rework_counter',
        'data_in_1',
        'data_in_2',
        'data_in_3',
        'data_in_4',
        'data_in_5',
        'data_in_6',
        'data_in_7',
        'data_in_8',
        'data_in_9',
        'data_in_10',
        'result',
        'next_entry_point',
        'error_code',
        'cycle_time',
        'data_out_1',
        'data_out_2',
        'data_out_3',
        'data_out_4',
        'data_out_5',
        'data_out_6',
        'data_out_7',
        'data_out_8',
        'data_out_9',
        'data_out_10',
        'mes_1',
        'mes_2',
        'mes_3',
        'mes_4',
        'mes_5',
        'nombre',
        'carpeta',

    ];
}
