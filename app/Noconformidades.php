<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Noconformidades extends Model
{
    protected $fillable = [
        'aplicacion_id',
        'fase',
        'resumen',
        'descripcion',
        'valor',
        'comentario',
        'fechaInicio',
        'fechaFinal',
        'fechaCierre',
        'empleados_id',
        'evaluador',
        'actuaciones',
        'opl',
        'estado',
    ];

    public function setAttributeEmpleado()
    {
        $noconf = Noconformidades::where('id',$this->id)->first();
        $empl = Empleados::where('id',$noconf->empleados_id)->first();
        $noconf->attributes['empleado'] = $empl->nombre;
    }

    public function setAttributeEvaluador($key, $value)
    {
        $noconf = Noconformidades::where('id',$this->id)->first();
        $eval = Empleados::where('id',$noconf->evaluador)->first();
        $noconf->attributes['evaluador'] = $eval->nombre;
    }
}
