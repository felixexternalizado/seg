<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zona extends Model
{
    protected $table = 'zonasdelinea';

    protected $fillable = [
        'linea_id', 'resumen','coordenadas'
    ];

    public function linea () {
        return $this->belongsTo('App\Line', 'linea_id');
    }

    public function regcalidad () {
        return $this->hasMany('App\Registrocalidad', 'zona_id');
    }

    public function registrolaboratorio () {
        return $this->hasMany('App\LaboratorioRegistro', 'zona_id');
    }

    public function registrocalidad () {
        return $this->hasMany('App\Registrocalidad', 'linea_id');
    }

    public function laboratorioregistros () {
        return $this->hasMany('App\LaboratorioRegistro', 'linea_id');
    }


}
