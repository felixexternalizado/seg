<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ChecklistConfirmacionesprocesoRegistros extends Model
{
    protected $table = 'checklist_confpro_registros';

    protected $fillable = [
        'confirmacionproceso_id',
        'indice',
        'valor',
        'fase',
        'foto',
        'fotoInstrucciones',
        'descripcion',
        'operacion',
        'comentario',
        'noconformidad',
        'limite1',
        'limite2',
        'tipo',
        'instrucciones',
        'validador',
//        'opciones',
        'valormanual',
        'ncFlag',
        'horaEntrada',
        'requerido'
    ];

}

