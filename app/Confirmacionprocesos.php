<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Confirmacionprocesos extends Model
{
    protected $fillable = [
        'original_id',
        'procesos_id',
        'empleados_id',
        'resumen',
        'evaluador',
        'fechaInicio',
        'fechaFinal',
        'plantilla',
        'version',
        'porcentajefallopermitido',
        'porcentajefalloreal',
        'totalMal',
        'totalBien',
        'porcentajeBien',
        'totalPreguntas',
        'linea',
        'estado',
        'json'
    ];

    public function registros() {
            return $this->hasMany('App\ChecklistConfirmacionesprocesoRegistros', 'confirmacionproceso_id');
    }

    public function empleado() {
        return $this->belongsTo('App\Empleados', 'empleados_id');
    }

    public function setSemanaAttribute($value)
    {
        $year = substr($value,0,4);
        $month = substr($value, 5,2);
        $day =substr($value, 8,2);
        $o = Dias::where('dia',$day)->where('mes', $month)->where('anno', $year)->first('semana');

        if(!isset($o->semana)) {
            $this->attributes['semana'] = null;
        } else {
            $this->attributes['semana'] = $o->semana;
        }
    }

    public function setEliminarAttribute($proceso) {
        $proceso->attributes['eliminar'] = $proceso->estado != 'borrador' ? false : true;
    }

    public function documentos() {
        return [];
    }

    public function noconformidades () {
        return $this->hasMany('App\Noconformidades', 'aplicacion_id')->where('opl', 'confirmacionesproceso');
    }

}
