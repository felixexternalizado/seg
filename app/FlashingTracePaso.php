<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlashingTracePaso extends Model
{
    protected $table = 'flashingtrace_pasos';
    
    protected $fillable = [
        'flashingtrace_cabecera_id',
        'step',
        'step_result',
        'description',
        'value_ascii',
        'min',
        'value',
        'max',
        'dimension',
        'section',
        'info',
        'nombre',
        'carpeta',
    ];
}
