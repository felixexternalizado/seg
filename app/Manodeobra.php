<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manodeobra extends Model
{
    protected $table = 'manodeobra';

    protected $fillable = [
        'linea_id', 'zona_id', 'empleado_id', 'fecha', 'turno'
    ];

    public function linea() {
        return $this->belongsTo('App\Line', 'linea_id');
    }
    public function empleado() {
        return $this->belongsTo('App\Empleados', 'empleado_id');
    }
    public function zona() {
        return $this->belongsTo('App\Zona', 'zona_id');
    }
}
