<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BancoPruebaDescripcion extends Model
{
    protected $table = 'bancopruebadescripcion';
    protected $fillable = [
        'id',
        'dmc',
        'resultado',
        'nPruebas',
        'nMq',
        'fecha',
        'catalogo',
        'sAislamiento',
        'sAcoplamiento',
        'vPicoTension',
        'sPicoTension',
        'pPicoTension',
        'rPicoTension',
        'carpeta',
        'banco',
    ];
}
