<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Procesos extends Model
{
    protected $fillable = [
        'lineas_id', 'resumen'
    ];
}
