<?php


/********************************************************************
 *  clase estatica que convierte mediante VBA documentos de excel y word a otros formatos
 *
 *
 */

namespace App\Library;

class officeClass
{
    static function msword_convert($old, $new)
    {
        // Verify that the old document exists.
        if (!is_file($old)) {
            throw new ErrorException("File not found.");
        }
        $old = realpath($old);
        // Force user to unlink the target file before converting (do not mess with overwrites).
        if (is_file($new)) {
            throw new ErrorException("Destination file already exists.");
        }


        // Connect to Word
        if (!class_exists('COM')) {
            throw new ErrorException('COM extension is not enabled.');
        }
        $word = new COM("word.application");
        if (!$word) {
            throw new ErrorException('Unable to instantiate Word COM object.');
        }
        // Check the Word version.
        if ($word->Version < 12) {
            $word->Quit();
            throw new ErrorException('The version of Word is too old.');
        }
        // $word->visible = 1;
        try {
            // Documents.Open: http://msdn.microsoft.com/en-us/library/office/ff835182%28v=office.14%29.aspx
            $word->Documents->Open($old, FALSE, TRUE);
        } catch (Exception $e) {
            $word->Quit();
            throw $e;
        }
        // Document.Convert: http://msdn.microsoft.com/en-us/library/office/bb243727(v=office.12).aspx
        try {
            $word->ActiveDocument->Convert();
        } catch (Exception $e) {
            // The convert call is unavailable when it is unnecessary.
            // Thus, suppress the error.
        }
        // WdSaveFormat: http://msdn.microsoft.com/en-us/library/office/bb238158%28v=office.12%29.aspx
        static $wdSaveFormats = array(
            'docx' => 16,
            'html' => 10,
            'rtf' => 6,
            'txt' => 2,
            'doc' => 0,
            'pdf' => 17,
        );
        $newSaveFormat = 16;
        if (preg_match('@\.(.{3,4})$@', $new, $arr)) {
            if (isset($wdSaveFormats[$arr[1]])) {
                $newSaveFormat = $wdSaveFormats[$arr[1]];
            }
        }
        $rethrow = NULL;
        try {
            // Document.SaveAs: http://msdn.microsoft.com/en-us/library/office/bb221597%28v=office.12%29.aspx
            $word->ActiveDocument->SaveAs($new, $newSaveFormat);
        } catch (Exception $rethrow) {
            // We still want to close the document.
        }
        try {
            // Document.Close: http://msdn.microsoft.com/en-us/library/office/bb214403(v=office.12).aspx
            $word->ActiveDocument->Close(FALSE);
        } catch (Exception $e) {
        }
        try {
            // Application.Quit: http://msdn.microsoft.com/en-us/library/office/bb215475(v=office.12).aspx
            $word->Quit();
        } catch (Exception $e) {
        }
        if (isset($rethrow)) {
            throw $rethrow;
        }
    }

    static function msexcel_convert($old, $new)
    {
        // Verify that the old document exists.
        if (!is_file($old)) {
            throw new ErrorException("File not found.");
        }
        $old = realpath($old);
        // Force user to unlink the target file before converting (do not mess with overwrites).
        if (is_file($new)) {
            @unlink($new);
            // throw new ErrorException("Destination file already exists.");
        }
        if (is_file($new . ".pdf")) {
            @unlink($new . ".pdf");
            // throw new ErrorException("Destination file already exists.");
        }
        // Connect to excel
        if (!class_exists('COM')) {
            throw new ErrorException('COM extension is not enabled.');
        }
        $excel = new COM("excel.application");
        if (!$excel) {
            throw new ErrorException('Unable to instantiate excel COM object.');
        }
        // Check the excel version.
        if ($excel->Version < 12) {
            $excel->Quit();
            throw new ErrorException('The version of excel is too old.');
        }
        // $excel->visible = 1;
        try {
            // Workbooks.Open: http://msdn.microsoft.com/en-us/library/office/bb179167%28v=office.12%29.aspx
            $excel->Workbooks->Open($old, FALSE, TRUE);
        } catch (Exception $e) {
            $excel->Quit();
            throw $e;
        }
        // XlFileFormat: http://msdn.microsoft.com/en-us/library/office/bb241279%28v=office.12%29.aspx
        static $xlFileFormat = array(
            'xlsx' => 51,
            'xls' => 43,
            'csv' => 6,
            'html' => '44',
        );
        $newSaveFormat = 51;
        if (preg_match('@\.(.{3,4})$@', $new, $arr)) {
            if (isset($xlFileFormat[$arr[1]])) {
                $newSaveFormat = $xlFileFormat[$arr[1]];
            }
        }
        try {
            // Workbook.SaveAs: http://msdn.microsoft.com/en-us/library/office/bb214129%28v=office.12%29.aspx
            // $excel->ActiveWorkbook->SaveAs($new, $newSaveFormat);

        } catch (Exception $rethrow) {
            // We still want to close the document.
        }
        try {
            $excel->ActiveWorkbook->ExportAsFixedFormat(0, $new);
        } catch (Exception $rethrow) {
            // We still want to close the document.
        }

        try {
            // Workbook.Close: http://msdn.microsoft.com/en-us/library/office/bb179153%28v=office.12%29.aspx
            $excel->ActiveWorkbook->Close(FALSE);
        } catch (Exception $e) {
        }
        try {
            // Application.Quit: http://msdn.microsoft.com/en-us/library/office/bb215475(v=office.12).aspx
            $excel->Quit();
        } catch (Exception $e) {
        }
        if (isset($rethrow)) {
            throw $rethrow;
        }
    }


    static function msexcel_convert_plus($old, $new, $cambios = [], $foto)
    {
        // Verify that the old document exists.
        if (!is_file($old)) {
            throw new ErrorException("File not found.");
        }
        $old = realpath($old);

        // Force user to unlink the target file before converting (do not mess with overwrites).
        if (is_file($new)) {
            @unlink($new);
            // throw new ErrorException("Destination file already exists.");
        }

        if (is_file($new . ".pdf")) {
            @unlink($new . ".pdf");
            // throw new ErrorException("Destination file already exists.");
        }

        // Connect to excel
        if (!class_exists('COM')) {
            throw new ErrorException('COM extension is not enabled.');
        }

        $excel = new COM("excel.application");
        if (!$excel) {
            throw new ErrorException('Unable to instantiate excel COM object.');
        }
        // Check the excel version.
        if ($excel->Version < 12) {
            $excel->Quit();
            throw new ErrorException('The version of excel is too old.');
        }

        // $excel->visible = 1;
        try {
            // Workbooks.Open: http://msdn.microsoft.com/en-us/library/office/bb179167%28v=office.12%29.aspx
            $excel->Workbooks->Open($old, FALSE, TRUE);
        } catch (Exception $e) {
            $excel->Quit();
            throw $e;
        }
        // XlFileFormat: http://msdn.microsoft.com/en-us/library/office/bb241279%28v=office.12%29.aspx
        static $xlFileFormat = array(
            'xlsx' => 51,
            'xls' => 43,
            'csv' => 6,
            'html' => '44',
        );
        $newSaveFormat = 51;
        if (preg_match('@\.(.{3,4})$@', $new, $arr)) {
            if (isset($xlFileFormat[$arr[1]])) {
                $newSaveFormat = $xlFileFormat[$arr[1]];
            }
        }

        try {
            foreach ($cambios as $celda => $valor) {
                $excel->ActiveSheet->Cells(round($celda[1]), ord($celda[0]) - 64)->Value = $valor;

            }

        } catch (Exception $rethrow) {
            // We still want to close the document.
        }


        try {
            $cell = $excel->ActiveSheet->Cells(round($celda[1]), ord($celda[0]) - 64)->Select();
            //$Pic=$excel->ActiveSheet->Pictures->Insert($foto)->Select();
            //$excel->ActiveSheet->Shapes->AddPicture( $foto,0,0,0,0, -1, -1);
            //$Pic->Width = 200;
            //$Pic->Height = 200;

        } catch (Exception $rethrow) {
            // We still want to close the document.
        }


        try {
            // Workbook.SaveAs: http://msdn.microsoft.com/en-us/library/office/bb214129%28v=office.12%29.aspx
            //$excel->ActiveWorkbook->SaveAs($new, $newSaveFormat);

        } catch (Exception $rethrow) {
            // We still want to close the document.
        }

        try {

            $excel->Range("A1:J37")->Select();
            // $excel->Range("I37")->Activate();
            $excel->Selection->ExportAsFixedFormat(0, $new, 0, 0, 0);
            //$excel->ActiveWorkbook->ExportAsFixedFormat(0,$new,0,0,0);
        } catch (Exception $rethrow) {
            // We still want to close the document.
        }

        try {
            // Workbook.Close: http://msdn.microsoft.com/en-us/library/office/bb179153%28v=office.12%29.aspx
            $excel->ActiveWorkbook->Close(FALSE);
        } catch (Exception $e) {
        }
        try {
            // Application.Quit: http://msdn.microsoft.com/en-us/library/office/bb215475(v=office.12).aspx
            $excel->Quit();
        } catch (Exception $e) {
        }
        if (isset($rethrow)) {
            throw $rethrow;
        }
    }

}


