<?php

namespace App\Library;
/********************************************************************
 *  clase que permite extraer diferentes documentos de SAP mediante VBA
 *
 *
 */
class SAP
{
    var $user = "CM43TRE";
    var $pass = "poup4445";
    var $catalogo = "";
    var $catalogoLote = "";

    var $fecha = "";
    var $materiales = [];

    var $docdir = "";
    var $tmpldir = "";
    var $vbadir = "";

    var $emulador = true;


    public function conectar()
    {
        return $this->execVBS("__conectarSAP", []);
    }

    public function downloadListaMateriales()
    {

        return $this->execVBS("__ListaMateriales", [$this->user, $this->pass, $this->catalogo, $this->fecha, $this->docdir . $this->catalogoLote]);
    }

    public function getListaMateriales()
    {

        //echo PHP_EOL."entrando en getListaMateriales";
        $data = [];
        if (!file_exists($this->docdir . $this->catalogoLote . DIRECTORY_SEPARATOR . $this->catalogo . "_LISTA" . ".TXT")) sleep(20);
        if (file_exists($this->docdir . $this->catalogoLote . DIRECTORY_SEPARATOR . $this->catalogo . "_LISTA" . ".TXT")) {
            //echo PHP_EOL."encontrado ".$this->docdir.$this->catalogo.DIRECTORY_SEPARATOR.$this->catalogo."_LISTA".".txt";

            $content = file($this->docdir . $this->catalogoLote . DIRECTORY_SEPARATOR . $this->catalogo . "_LISTA" . ".TXT");

            $cabecera = false;
            $resultados = false;
            foreach ($content as $linea) {

                //echo " recorriendo ListaMateriales";
                if ($resultados) {
                    $linea = str_replace(chr(9), "*", $linea);
                    $linea = str_replace("****", "*", $linea);
                    $linea = str_replace("***", "*", $linea);
                    $linea = str_replace("**", "*", $linea);
                    $linea = str_replace("*", "*", $linea);

                    //$data[] = ["id"=>trim(substr($linea,0,5)),"cantidad"=>trim(substr($linea,5,10)),"cantidad"=>trim(substr($linea,5,10))];
                    $datos = explode("*", $linea);
                    if (substr($datos[0], 3, 1) == ".") {
                        if (substr($datos[0], 0, 1) < 9) {
                            $data[] = ["id" => $datos[0], "ref" => trim(str_replace(".", "", $datos[4]))];
                        } else {
                            $data[] = ["id" => $datos[0], "ref" => trim(str_replace(".", "", $datos[1]))];
                        }

                    }
                }

                if ($cabecera == true) {
                    $resultados = true;
                }
                if (substr($linea, 0, 5) == "Pos.F") {
                    $cabecera = true;
                };

            }
        }
        $this->materiales = $data;
        //echo ("recodiga la lista".print_r($data,1));
    }

    public function downloadPlanoConjunto()
    {
        return $this->execVBS("__PlanoConjunto", [$this->user, $this->pass, $this->catalogo, $this->fecha, $this->docdir . $this->catalogoLote]);
    }

    public function downloadPlanoOferta()
    {
        return $this->execVBS("__PlanoOferta", [$this->user, $this->pass, $this->catalogo, $this->fecha, $this->docdir . $this->catalogoLote]);
    }

    public function downloadMarcajes()
    {
        if (count($this->materiales) < 1) $this->getListaMateriales();
        $ref = $ref2 = "xxx";

        if (substr($this->catalogo, 0, 4) == "0124") {
            $ref = "998.0";
            $ref2 = "998.1";
        }
        if (substr($this->catalogo, 0, 4) == "0126") {
            $ref = "100.0";
            $ref2 = "997.1";
        }
        //echo PHP_EOL."INICIADO";
        foreach ($this->materiales as $linea) {
            //echo PHP_EOL.$linea["id"]."---".$ref."---".$ref2;
            if ($linea["id"] == $ref || $linea["id"] == $ref2) {
                $linea["id"] == $ref ? $this->downloadPlanoMarcaje1() : $this->downloadPlanoMarcaje2();
                if (file_exists($this->tmpldir . $linea["ref"] . '.pdf')) {
                    @copy(
                        $this->tmpldir . $linea["ref"] . '.pdf',
                        $this->docdir . $this->catalogoLote . DIRECTORY_SEPARATOR . $this->catalogo . "_" . $linea["ref"] . '.MARCA.PDF');
                    //echo 'copiado'.$linea["ref"].'.pdf';
                    //$this->tmpldir.$linea["ref"].'.pdf';);
                } else {
                    //echo 'no encontrado'.$this->tmpldir.$linea["ref"].'.pdf';
                    //$this->tmpldir.$linea["ref"].'.pdf';
                }
            }
        }
    }

    public function downloadPlanoMarcaje1()
    {
        return $this->execVBS("__PlanoMarcaje1", [$this->user, $this->pass, $this->catalogo, $this->fecha, $this->docdir . $this->catalogoLote]);
    }

    public function downloadPlanoMarcaje2()
    {
        return $this->execVBS("__PlanoMarcaje2", [$this->user, $this->pass, $this->catalogo, $this->fecha, $this->docdir . $this->catalogoLote]);
    }

    public function downloadIndice($indice)
    {
        return $this->execVBS("__DocumentoIndex", [$this->user, $this->pass, $this->catalogo, $this->fecha, $this->docdir . $this->catalogoLote, $indice]);
    }

    public function __construct($catalogoLote, $vbadir, $docdir, $tmpldir, $emulador = false)
    {

        $parte = explode(".", $catalogoLote);
        $this->catalogo = $parte[0];
        $this->catalogoLote = $catalogoLote;
        $this->vbadir = $vbadir;
        $this->docdir = $docdir;
        $this->tmpldir = $tmpldir;
        $this->fecha = $this->format_date(date("d-m-Y"), "dateSAP");
        $this->emulador = $emulador;

        //la fecha puede salir tambien del $catalogoLote;

    }

    private function execVBS($file, $args)
    {
        $errores = 0;

        $arglist = implode(" ", $args);
        //file_put_contents("c:\php\server\montaje.bat","wscript ".$this->appdir.$file.".vbs ".$arglist);

        if (!$this->emulador) system("wscript " . $this->vbadir . $file . ".vbs " . $arglist, $errores);
        if ($this->emulador) {

            switch ($file) {
                case "__ListaMateriales":
                    copy($this->docdir . "emulacion" . DIRECTORY_SEPARATOR . "LISTA.HTM", $this->docdir . $this->catalogoLote . DIRECTORY_SEPARATOR . $this->catalogo . "_LISTA.HTM");
                    copy($this->docdir . "emulacion" . DIRECTORY_SEPARATOR . "LISTA.TXT", $this->docdir . $this->catalogoLote . DIRECTORY_SEPARATOR . $this->catalogo . "_LISTA.TXT");
                    break;
                case "__PlanoOferta":
                    copy($this->docdir . "emulacion" . DIRECTORY_SEPARATOR . "PLANOOFERTA.PDF", $this->docdir . $this->catalogoLote . DIRECTORY_SEPARATOR . $this->catalogo . "_PLANOOOFERTA.PDF");
                    break;
                case "__PlanoConjunto":
                    copy($this->docdir . "emulacion" . DIRECTORY_SEPARATOR . "PLANOCONJUNTO.PDF", $this->docdir . $this->catalogoLote . DIRECTORY_SEPARATOR . $this->catalogo . "_PLANOCONJUNTO.PDF");
                    break;
                case "__PlanoMarcaje1":
                    copy($this->docdir . "emulacion" . DIRECTORY_SEPARATOR . "MARCAJE1.PDF", $this->docdir . $this->catalogoLote . DIRECTORY_SEPARATOR . $this->catalogo . "_MARCAJE1.PDF");
                    copy($this->docdir . "emulacion" . DIRECTORY_SEPARATOR . "MARCAJE1.PDF", $this->docdir . $this->catalogoLote . DIRECTORY_SEPARATOR . $this->catalogo . "_MARCAJE1.MARCA.PDF");
                    break;
                case "__PlanoMarcaje2":
                    copy($this->docdir . "emulacion" . DIRECTORY_SEPARATOR . "MARCAJE1.PDF", $this->docdir . $this->catalogoLote . DIRECTORY_SEPARATOR . $this->catalogo . "_MARCAJE2.PDF");
                    break;
                case "__DocumentoIndex":
                    copy($this->docdir . "emulacion" . DIRECTORY_SEPARATOR . "MARCAJE1.PDF", $this->docdir . $this->catalogoLote . DIRECTORY_SEPARATOR . $this->catalogo . "_INDICE.PDF");
                    break;
            }

        }
        return $errores;
    }

    private function format_date($original = '', $format = "%m/%d/%Y")
    {
        $format = ($format == 'date' ? "%m-%d-%Y" : $format);
        $format = ($format == 'dateSAP' ? "%d.%m.%Y" : $format);
        $format = ($format == 'datetime' ? "%m-%d-%Y %H:%M:%S" : $format);
        $format = ($format == 'mysql-date' ? "%Y-%m-%d" : $format);
        $format = ($format == 'mysql-datetime' ? "%Y-%m-%d %H:%M:%S" : $format);
        return (!empty($original) ? strftime($format, strtotime($original)) : "");
    }
}

