' LISTA DE ARGUMENTOS
' 1. usuario de Sap
' 2. password de Sap
' 3. Numero de alternador
' 4. fecha actual
' 5. directorio de descarga
' 6. ref del index a descargar									
	
on error resume next				

	'conectamos con SAP
	Set SapGui = GetObject("SAPGUI")

	'si no hemos conectado es porque no está iniciado, lo iniciamos
	If Err.Number <> 0 Then
		If Err.Number= -2147221020 then
			Dim WshShell
			Set WshShell = WScript.CreateObject("WScript.Shell")
			WshShell.Run "saplogon.exe", 1
			WScript.Sleep 5000
			Set SapGui = GetObject("SAPGUI")
		Else
			WScript.Quit 99
		end if	
	end if

	'logueamos en SAP
	Set Appl = SapGui.GetScriptingEngine
	Set Connection = Appl.Openconnection("PSG: SGDE Production", True)
	Set Session = Connection.Children(0)
	session.findById("wnd[0]/usr/txtRSYST-BNAME").text = Wscript.Arguments(0) 'variable usuario de SAP
	session.findById("wnd[0]/usr/pwdRSYST-BCODE").text = Wscript.Arguments(1) 'variable password de SAP
	session.findById("wnd[0]/usr/pwdRSYST-BCODE").caretPosition = 8
	session.findById("wnd[0]").sendVKey 0


	'si hay multiple logon, continue this logon and end others
	If session.Children.Count > 1 Then
		session.findById("wnd[1]/usr/radMULTI_LOGON_OPT2").Select
		session.findById("wnd[1]/usr/radMULTI_LOGON_OPT2").SetFocus
		session.findById("wnd[1]/tbar[0]/btn[0]").press
	End If


	'iniciamos los enlaces con los objetos de SAP
	If Not IsObject(Application) Then
	   Set SapGuiAuto = GetObject("SAPGUI")
	   Set Application = SapGuiAuto.GetScriptingEngine
	End If
	If Not IsObject(Connection) Then
	   Set Connection = Application.Children(0)
	End If
	If Not IsObject(session) Then
	   Set session = Connection.Children(0)
	End If
	If IsObject(WScript) Then
	   WScript.ConnectObject session, "on"
	   WScript.ConnectObject Application, "on"
	End If


'descargamos el fichero index
session.findById("wnd[0]/usr/cntlIMAGE_CONTAINER/shellcont/shell/shellcont[0]/shell").selectedNode = "F00007"
session.findById("wnd[0]/usr/cntlIMAGE_CONTAINER/shellcont/shell/shellcont[0]/shell").doubleClickNode "F00007"
session.findById("wnd[0]/usr/ctxtRC29L-MATNR").Text =  Wscript.Arguments(2) +"-"+ Wscript.Arguments(5)+"PS" 'variable documento a extraer
session.findById("wnd[0]/usr/ctxtRC29L-CAPID").Text = ""
session.findById("wnd[0]/usr/ctxtRC29L-CAPID").SetFocus
session.findById("wnd[0]").sendVKey 4
session.findById("wnd[1]/usr/lbl[12,10]").SetFocus
session.findById("wnd[1]").sendVKey 2
session.findById("wnd[0]/usr/ctxtRC29L-DATUV").Text = Wscript.Arguments(3)
session.findById("wnd[0]/usr/ctxtRC29L-DATUV").SetFocus
session.findById("wnd[0]/tbar[1]/btn[8]").press
session.findById("wnd[0]/tbar[1]/btn[45]").press
session.findById("wnd[1]/usr/subSUBSCREEN_STEPLOOP:SAPLSPO5:0150/sub:SAPLSPO5:0150/radSPOPLI-SELFLAG[3,0]").Select
session.findById("wnd[1]/usr/subSUBSCREEN_STEPLOOP:SAPLSPO5:0150/sub:SAPLSPO5:0150/radSPOPLI-SELFLAG[3,0]").SetFocus
session.findById("wnd[1]/tbar[0]/btn[0]").press
session.findById("wnd[1]/usr/ctxtDY_PATH").SetFocus
session.findById("wnd[1]").sendVKey 4
Msgbox Wscript.Arguments(4) +"\" ,0,  Wscript.Arguments(2)+"_"+"INDEX_"+Wscript.Arguments(5)+"PS"+".HTM"
session.findById("wnd[2]/usr/ctxtDY_PATH").Text = Wscript.Arguments(4) +"\" 'variable del directorio final
session.findById("wnd[2]/usr/ctxtDY_FILENAME").Text = Wscript.Arguments(2)+"_"+Wscript.Arguments(5)+"PS"+".HTM" 'variable nombre de documento final
session.findById("wnd[2]/tbar[0]/btn[11]").press
session.findById("wnd[1]/usr/ctxtDY_FILENAME").SetFocus
session.findById("wnd[1]/tbar[0]/btn[11]").press
session.findById("wnd[0]/tbar[0]/btn[3]").press

'cerramos la ventana abierta
session.findById("wnd[0]").close
session.findById("wnd[1]/usr/btnSPOP-OPTION1").press

'finalizamos la ejecucion
If Err.Number <> 0  and err.number<> -2147221020 Then
	WScript.Quit 99
else
	WScript.Quit 0
end if
 