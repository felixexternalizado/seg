' LISTA DE ARGUMENTOS
' 1. usuario de Sap
' 2. password de Sap
' 3. Numero de alternador
' 4. fecha actual
' 5. directorio de descarga
on error resume next
	


	'conectamos con SAP
	Set SapGui = GetObject("SAPGUI")

	'si no hemos conectado es porque no está iniciado, lo iniciamos
	If Err.Number <> 0 Then
		If Err.Number= -2147221020 then
			Dim WshShell
			Set WshShell = WScript.CreateObject("WScript.Shell")
			WshShell.Run "saplogon.exe", 1
			WScript.Sleep 5000
			Set SapGui = GetObject("SAPGUI")
		Else
			WScript.Quit 99
		end if	
	end if

	'logueamos en SAP
	Set Appl = SapGui.GetScriptingEngine
	Set Connection = Appl.Openconnection("PSG: SGDE Production", True)
	Set Session = Connection.Children(0)
	session.findById("wnd[0]/usr/txtRSYST-BNAME").text = Wscript.Arguments(0) 'variable usuario de SAP
	session.findById("wnd[0]/usr/pwdRSYST-BCODE").text = Wscript.Arguments(1) 'variable password de SAP
	session.findById("wnd[0]/usr/pwdRSYST-BCODE").caretPosition = 8
	session.findById("wnd[0]").sendVKey 0


	'si hay multiple logon, continue this logon and end others
	If session.Children.Count > 1 Then
		session.findById("wnd[1]/usr/radMULTI_LOGON_OPT2").Select
		session.findById("wnd[1]/usr/radMULTI_LOGON_OPT2").SetFocus
		session.findById("wnd[1]/tbar[0]/btn[0]").press
	End If


	'iniciamos los enlaces con los objetos de SAP
	If Not IsObject(Application) Then
	   Set SapGuiAuto = GetObject("SAPGUI")
	   Set Application = SapGuiAuto.GetScriptingEngine
	End If
	If Not IsObject(Connection) Then
	   Set Connection = Application.Children(0)
	End If
	If Not IsObject(session) Then
	   Set session = Connection.Children(0)
	End If
	If IsObject(WScript) Then
	   WScript.ConnectObject session, "on"
	   WScript.ConnectObject Application, "on"
	End If
	
				

'ENTRADA EN LISTA DE PIEZAS
session.findById("wnd[0]/usr/cntlIMAGE_CONTAINER/shellcont/shell/shellcont[0]/shell").selectedNode = "F00044"
session.findById("wnd[0]/usr/cntlIMAGE_CONTAINER/shellcont/shell/shellcont[0]/shell").doubleClickNode "F00044"
session.findById("wnd[0]/usr/chkPA_AENNR").selected = true
session.findById("wnd[0]/usr/ctxtPA_MATNR").text = Wscript.Arguments(2) 'variable numero alternador
session.findById("wnd[0]/usr/ctxtPA_WERKS").text = "9000"
session.findById("wnd[0]/usr/ctxtPA_STLAN").text = "3"
session.findById("wnd[0]/usr/txtPA_STLAL").text = "1"
session.findById("wnd[0]/usr/ctxtPA_DATUV").text = Wscript.Arguments(3) 'variable Wscript.Arguments(3)
session.findById("wnd[0]/usr/chkPA_AENNR").setFocus
session.findById("wnd[0]/tbar[1]/btn[8]").press

'EXTRACCION PLANO DE CONJUNTO
session.findById("wnd[0]/usr/lbl[115,3]").SetFocus
session.findById("wnd[0]/usr/lbl[115,3]").caretPosition = 7
session.findById("wnd[0]").sendVKey 2
session.findById("wnd[0]/usr/tabsTAB_MAIN/tabpTSMAIN/ssubSCR_MAIN:SAPLCV110:0102/cntlCTL_FILES1/shellcont/shell/shellcont[1]/shell").selectNode "          1"
session.findById("wnd[0]/usr/tabsTAB_MAIN/tabpTSMAIN/ssubSCR_MAIN:SAPLCV110:0102/cntlCTL_FILES1/shellcont/shell/shellcont[1]/shell").nodeContextMenu "          1"
session.findById("wnd[0]/usr/tabsTAB_MAIN/tabpTSMAIN/ssubSCR_MAIN:SAPLCV110:0102/cntlCTL_FILES1/shellcont/shell/shellcont[1]/shell").selectContextMenuItem "CF_EXP_COPY"
session.findById("wnd[1]/usr/ctxtDRAW-FILEP").Text = Wscript.Arguments(4)+"\"+Wscript.Arguments(2)+"_PLANOCONJUNTO.PDF" 'directorio plano conjunto
session.findById("wnd[1]/usr/ctxtDRAW-FILEP").SetFocus
session.findById("wnd[1]/usr/ctxtDRAW-FILEP").caretPosition = 124
session.findById("wnd[1]/tbar[0]/btn[0]").press
session.findById("wnd[0]/tbar[0]/btn[3]").press


'cerramos la ventana abierta
session.findById("wnd[0]").close
session.findById("wnd[1]/usr/btnSPOP-OPTION1").press

'finalizamos la ejecucion
If Err.Number <> 0  and err.number<> -2147221020 Then
	WScript.Quit 99
else
	WScript.Quit 0
end if


