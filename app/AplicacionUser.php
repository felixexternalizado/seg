<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AplicacionUser extends Model
{
    protected $table = 'aplicaciones_users';

    protected $fillable = [
        'aplicacion_id','user_id'
    ];

}
