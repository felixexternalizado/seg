<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Confirmacionprocesospreguntas extends Model
{
    protected $fillable = [
        'confirmacionProcesos_id',
        'fase',
        'resumen',
        'descripcion',
        'tipo',
        'valor',
        'comentario',
        'foto',
        'evaluador',
        'hora',
        'condicion',
        'limite1',
        'limite2',
        'reglamento',
        'noConformidades_id',
    ];
}
