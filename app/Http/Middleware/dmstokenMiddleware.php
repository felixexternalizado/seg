<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class dmstokenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $auth = $request->header('Authorization');

        if(!$auth) return response('Usuario no autorizado', 401);

        $user = User::where('_tokenDMS',explode(' ', $auth)[1])->first();

        if (!$user) return response('Usuario no autorizado', 401);

        Auth::login($user);

        return $next($request);
    }
}
