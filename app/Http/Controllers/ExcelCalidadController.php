<?php

namespace App\Http\Controllers;

use App\Empleados;
use App\Zona;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;

class ExcelCalidadController extends AppController
{
    public $objPHPExcel;
    public $arraytabla = [];

    public function getJson(\App\Procesoscalidad $fichero,$empleado, $request){

        $linea = $request->linea;
        $tipo_frecuencia = $request->tipo;

        $valor = $request->valor;
        if($tipo_frecuencia === 'semana') {
            $tipo_frecuencia = explode(' ',$request->valor)[0];
            $valor = explode(' ',$request->valor)[1];
        }

        $zona = Zona::findOrFail($request->zona);

        $carpeta = explode('.',$fichero->resumen)[0];

        if(!isset(explode('.',$fichero->resumen)[1])) return (['message' => utf8_encode('No está especificada la carpeta en el nombre del proceso.')]);

        $file = strtolower(str_replace(' ', '_', explode('.',$fichero->resumen)[1]) . '.xlsx');

        $storage = $this->storage;

        $dir = '';

        for ($x = 2; $x < count(scandir($storage.'/registros_calidad')); $x++) {
            $scandir = scandir($storage.'/registros_calidad');
            $dir = $storage . '/registros_calidad/' . $scandir[$x] . '/' . $carpeta . '/' . $file;

            if(!file_exists($dir)) return (['message' => 'Error al encontrar la plantilla del proceso /registros_calidad/' . $scandir[$x] . '/' . $carpeta . '/' . $file]);
        }
        if($dir == '' && file_exists($dir)) {
            return (['message' => 'Error al encontrar la plantilla del proceso /registros_calidad/' . $scandir[$x-1] . '/' . $carpeta . '/' . $file]);
        }

        //Leemos la segunda hoja de la plantilla para comprobar si el cambio de catalogo introducido existe en esa segunda hoja.
        $this->arraytabla = $this->getTabla('registroscalidad',$fichero, $request);
        if(isset($this->arraytabla['message'])) return $this->arraytabla;

        $this->getExcel($dir);

        $fichero->resumen = explode('.',$fichero->resumen)[1];

        $empleado = \App\Empleados::where('id', $empleado)->first();
        $linea = \App\Line::where('id', $linea)->first();

        $fases = array();
        $datos = array();
        $date = Carbon::now();
        $date = $date->format('Ymdhis');
        $argumento = str_replace(' ','',$fichero->resumen).'.'.$date;
        $materiales = '';

        for($i=12;$i<500;$i++){
            $result = $this->getRow($i, $argumento, $empleado, $linea, $materiales, $instrucciones=[], $tipo_frecuencia, $zona, $valor);
            if ($result) { $datos[] = $result; }

            $fase = $result['fase'];
            if(isset($fases[$fase])){
                $fases[$fase]++;
            }else{
                $fases[$fase]=1;
            }
        }
        $fases = array_keys($fases);
        return ["entidad"=>["id"=>	$fichero->resumen,"fases"=>$fases,"registros"=>$datos]];
    }

    public function getExcel($fichero){
        $inputFileType = PHPExcel_IOFactory::identify($fichero);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $this->objPHPExcel = $objReader->load($fichero); // Empty Sheet
        $this->objPHPExcel->setActiveSheetIndex(0);
    }

    public function getRow($i, $argumento, $empleado, $linea, $materiales=[], $instrucciones=[], $tipo_frecuencia, $zona, $valorconcreto){
        $fase = $this->getCelda('B'.$i);
        $arr_ev = '';
        if(strlen($fase)>0){
            $descripcion = $this->getCelda('C'.$i);
            $frecuencia = $this->getCelda('D'.$i);

            $explode = explode(':',$frecuencia);

            if(strtolower($explode[0]) != strtolower($tipo_frecuencia) && $explode[0] != null) {
                return false;
            }

            $operacion = $this->getCelda('E'.$i);
            $instrucciones = str_replace(chr(10),'<br>',$this->getCelda('F'.$i));
            $instrucciones = str_replace('\n','<br>',$instrucciones);
            $condicional = $this->condicionalExcel($this->getCelda('H'.$i));
            $noconformidad = strval($this->getCelda('I'.$i));
            $limite1 = $this->getCelda('J'.$i);
            $limite2 = $this->getCelda('K'.$i);
            $formato = $this->getCelda('L'.$i);
            $evaluadores = strval($this->getCelda('M'.$i));


            if($evaluadores != '') {
                $evaluadores = explode(';',$evaluadores);
                foreach ($evaluadores as $evaluador) {
                    if(strtolower($evaluador) == 'responsable') {
                        $ev = Empleados::where('id',auth()->user()->empleados_id)->first();
                        if(isset($ev) && $ev->responsable_id > 0) {
                            $arr_ev.= $ev->responsable_id.',';
                        }

                    } else {
                        $ev = Empleados::where('email', $evaluador)->first('id');
                        if(isset($ev) && $ev->id > 0) {
                            $arr_ev.= $ev->id.',';
                        }
                    }

                }
            }

            $fotoInstrucciones = $this->getCelda('O'.$i) == '' ? $this->getCelda('O'.$i) : $_SERVER['HTTP_HOST'].'/_storage/fotos_plantillas/'.$this->getCelda('O'.$i);

            $fechafin = $this->getCelda('N'.$i);
            $now = Carbon::now();
            $fechafin = $now->addDays($fechafin);
            $comentario = '';

            if($this->getCelda('G'.$i)!=""){ //respuesta
                $respuesta = $this->getCelda('G'.$i);
            }else{
                $respuesta = "ETIQUETA";
            }
            $valor="";

            if(isset($valorconcreto) && isset($explode[1]) && $valorconcreto != $explode[1] ) {
                return false;
            }

            switch(strtoupper($respuesta)){
                case "ID":								//OK
                    $tipo ="!"; //checkeado
                    $validador="";
                    $opciones ="";
                    $requerido = true;
                    $valor=$argumento;
                    break;
                case "AHORA":								//OK
                    $tipo ="!A"; //checkeado
                    $validador="";
                    $opciones ="";
                    $requerido = true;
                    $valor = Carbon::now()->format('Y-m-d H:i:s');
                    break;
                case "HORA":								//OK
                    $tipo ="HORA"; //checkeado
                    $validador="";
                    $opciones ="";
                    $requerido = true;
                    $valor = Carbon::now()->format('H:i:s');
                    break;
                case "LINEA":								//OK
                    $tipo ="!L"; //checkeado
                    $validador="";
                    $opciones ="";
                    $requerido = true;
                    $valor=$linea->resumen;
                    break;
                case "USER":								//OK
                    $tipo ="!U"; //checkeado
                    $validador="";
                    $opciones ="";
                    $requerido = true;
                    $valor=$empleado->nombre;
                    break;
                case "OK":								//OK
                    $tipo ="O"; //checkeado
                    $validador="OK";
                    $opciones =[];
                    $requerido = true;
                    break;
                case "SI":								//SI
                    $tipo="B"; //booleano
                    $validador="SI";
                    $opciones =[];
                    $requerido = true;
                    break;
                case "TEXTO":							//TEXTO
                    $tipo = "T"; //texto
                    $validador="";
                    $opciones =[];
                    $requerido = false;
                    break;
                case "TEXTO OBLIGATORIO":				//TEXTO OBLIGATORIO
                    $tipo = "T"; //numero
                    $validador="";
                    $opciones =[];
                    $requerido = true;
                    break;
                case "NUMERO":							//NUMERO
                    $tipo = "N"; //numero
                    $validador="";
                    $opciones =[];
                    $requerido = false;
                    break;
                case "NUMERO OBLIGATORIO":				//NUMERO OBLIGATORIO
                    $tipo = "N"; //numero
                    $validador="";
                    $opciones =[];
                    $requerido = true;
                    break;
                case "FOTO":
                    $tipo = "F"; //foto
                    $validador="";
                    $opciones =[];
                    $requerido = true;
                    break;
                case "IMAGEN":
                    $tipo = "I"; //imagen
                    $validador="";
                    $opciones =[];
                    $requerido = true;
                    break;
                case "ETIQUETA":
                    $tipo = "E"; //etiqueta
                    $validador="";
                    $opciones =[];
                    $requerido = false;
                    break;
                case "VALORACION5":
                    $tipo = "S"; //etiqueta
                    $validador="";
                    $opciones =[];
                    $requerido = false;
                    break;
                case "QR":
                    $tipo = "QR"; //qr
                    $validador="";
                    $opciones =[];
                    $requerido = false;
                    break;
                default:

                    if (preg_match('/BARCODE:/',$respuesta) >= 1){
                        $tipo = "barcode"; //barcode
                        $validador="";
                        $opciones =$this->getBarcode($respuesta);
                        $requerido = false;
                    }

                    elseif(substr($respuesta,0,1)=="@"){	//*cargar...
                        if(substr($respuesta,0,6)=="@cargar") $tipo ="A"; //ACCION
                        else $tipo = "ejecutar";

                        $validador = "";
                        $comando = explode(":",trim(substr($respuesta,1)));
                        @$opciones=["titulo"=>$comando[0],"accion"=>$comando[1]];
                        $requerido = false;

                    }elseif(substr($respuesta,0,1)=="*"){	//*060.0  // *A3

                        $opciones[]="*";
                        $tipo ="H"; //LISTA
                        $validador = "UNICO";
                        $opciones=[];
                        if(isnumeric(substr($respuesta,1,1))){	//*060.0
                            foreach($materiales as $material){
                                if(substr($material["id"],0,4)==substr($respuesta,1,4)){
                                    $opciones[]=$material["ref"];
                                }

                            }
                        }else{// *A3
                            $opciones = explode(",", $instrucciones[substr($respuesta,1)]);
                        }
                        $opciones[]="*";
                        $requerido = true;

                    }else{
                        if(strpos($respuesta,";")>0){
                           //F00M148602;F00M146634;NO
                                $tipo ="select"; //lista
                                $validador = "LISTA";
                                $respuesta = str_replace('?','',$respuesta);
                                $respuesta = explode(';', $respuesta);
                                $item = array();
                                foreach ($respuesta as $res) {
                                    $a = explode('#', $res);
                                    $item[$a[0]] = $a[1];
                                }
                                $opciones = $item;
                                $requerido = true;
                        }else{							//F00M990180
                            $tipo ="T"; //lista
                            $validador = "UNICO";
                            $opciones = [trim($respuesta)];
                            $requerido = true;
                        }


                    }
            }
            return ["indice"=>$i-11,"valor"=>$valor,"fase"=>"F".$fase,"descripcion"=>$descripcion,"operacion"=>$operacion,
                "frecuencia" => $frecuencia,"condicional"=>$condicional,"comentario"=>$comentario,
                "noconformidad"=>$noconformidad,"limite1"=>$limite1,"limite2"=>$limite2,'formato' => $formato,
                "tipo"=>$tipo,'instrucciones' =>$instrucciones,
                "validador"=>$validador,"opciones"=>$opciones, "requerido"=>$requerido,/*, "variable"=>$variable*/
                "evaluador" => $arr_ev, "fechafin" => $fechafin, 'fotoInstrucciones' => $fotoInstrucciones];
        }


        return false;

    }

    public function getCelda($celda){
        $code = $this->objPHPExcel->getActiveSheet()->getCell($celda)->getCalculatedValue();
        if(strstr($code,'=')==true)    $code = $this->objPHPExcel->getActiveSheet()->getCell($celda)->getCalculatedValue();
        return $code;

    }

    private function getTabla($tipo,$process,$request) {
        $array = [];
        $nombre_excel = explode('.', $process->resumen)[1];
        $nombre_excel = str_replace(' ','_', $nombre_excel);
        $carpeta = explode('.', $process->resumen)[0];
        $ruta = $carpeta . '/' . strtolower($nombre_excel);
        $dir = $this->aplicaciones[$tipo]['carpeta_procesos'] . '/' . $ruta  . '.xlsx';

        if(!is_file($dir)) return $array;

        $inputFileType = PHPExcel_IOFactory::identify($dir);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $this->objPHPExcel = $objReader->load($dir); // Empty Sheet

        $nhojas = $this->objPHPExcel->getSheetCount();
        $ultimahoja = $nhojas - 1;
        if($ultimahoja < 1) return $array;

        $this->objPHPExcel->setActiveSheetIndex(1);
        $high = $this->objPHPExcel->getActiveSheet(1)->getHighestDataRow();

        for($i=3;$i<$high;$i++) { //SUSTITUIR POR WHILE
            $respuesta = strtolower($this->getCelda('B'.$i));
            $referencia = $this->getCelda('C'.$i);

            if($referencia == $request->valor && $request->tipo == 'catalogo') {
	            $limite1 = $this->getCelda('D'.$i);
	            $limite2 = $this->getCelda('E'.$i);
	            $decimales = $this->getCelda('F'.$i);
	
	            $arr = [
	                'referencia' => $referencia,
	                'limite1' => $limite1,
	                'limite2' => $limite2,
	                'decimales' => $decimales,
	            ];
	            $array[$respuesta][] = $arr;
            }
        }
        if(empty($array) && $request->tipo == 'catalogo') return ['message' => 'No se ha encontrado el catálogo en la tabla de Espiras y Resistencias',404];

        return $array;
    }
}
