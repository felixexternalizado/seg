<?php

namespace App\Http\Controllers;

use App\FlashingTrace;
use App\FlashingTracePaso;
use App\FlashingTracePrueba;
use App\FlashingTraceCabecera;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class FlashingTraceController extends Controller
{
	protected $lang = 'es';
    //TABLA SIEMPRE SERA visorflashing
    //VISTA SERA resumen, pruebas, pasos, paso, dmc

    public function index(Request $request, $tabla, $nombre = null, $carpeta = null, $paso = null, $fecha = null)
    {
        try {
            if ($tabla === 'pruebas') {
                $model = 'App\\FlashingTracePrueba';
                $campo = 'nombre';
                $cols = ['paso', 'fecha', 'descripcion', 'etapa', 'minimo', 'maximo', 'media', 'total', 'nOk', 'nOkPer'];
            } elseif ($tabla === 'pasos') {
                $model = 'App\\BancoPaso';
                $campo = 'nombre';
                $cols = ['dmc', 'inicio', 'vV1', 'vV2', 'vV3', 'vV4'];
            } elseif ($tabla === 'paso') {
                $model = 'App\\BancoPaso';
                $campo = 'dmc';
                $cols = ['paso', 'descripcion', 'etapaProceso', 'etapa', 'nPruebas',
                    'vV1', 'sV1', 'liV1', 'lsV1',
                    'vV2', 'sV2', 'liV2', 'lsV2',
                    'vV3', 'sV3', 'liV3', 'lsV3',
                    'vV4', 'sV4', 'liV4', 'lsV4'];
            } elseif ($tabla === 'dmc') {
                $model = 'App\\BancoPruebaDescripcion';
                $campo = 'dmc';
                $cols = ['dmc', 'fecha', 'nombre', 'catalogo','nPruebas',
                    'inicio', 'ok', 'ko'];
            } else {
            	$model = 'App\\FlashingTrace';
                $cols = ['fecha', 'banco', 'catalogo', 'inicio', 'fin', 'nPruebas', 'ok', 'okPer', 'ko', 'nOkPer'];
            }
            $query = $model::query();

            if($tabla === 'pasos' || $tabla === 'paso' || $tabla === 'dmc') {
                if (isset($banco) && $tabla === 'dmc') {

                    $arr_bancos = explode(' ', $banco);
                    $query->whereIN('dmc', $arr_bancos);

                    if (isset($request->filters)) {
                        $fechas = [];
                        foreach ($request->filters as $filter) {
                            $filter = json_decode($filter);

                            if ($filter->field !== 'fecha') continue;

                            if ($filter->operator === '=') $query->where('fecha', Carbon::createFromFormat('d/m/Y', $filter->value[0])->format('Y-m-d H:i:s'));

                            if ($filter->operator === 'between') {
                                $fechas[] = Carbon::createFromFormat('d/m/Y', $filter->value[0])->format('Y-m-d');
                                $fechas[] = Carbon::createFromFormat('d/m/Y', $filter->value[1])->format('Y-m-d');
                                $query->whereBetween('fecha', $fechas);
                            }
                        }
                    }

                    $recordsFiltered = $query->count();

                    $query = $this->skipOrder($request, $query);

                    $data = $query->get();
                    foreach($data as $d) {
                        $d->inicio = Carbon::createFromFormat('Y-m-d H:i:s', $d->fecha)->format('H:i:s');
                        $d->fecha = Carbon::createFromFormat('Y-m-d H:i:s', $d->fecha)->format('d/m/Y');
                        if($d->resultado === 'BUENO') {
                            $d->ok = 1;
                            $d->ko = 0;
                        } else {
                            $d->ok = 0;
                            $d->ko = 1;
                        }
                    }


                    $output = [
                        'data' => $data,
                        'recordsFiltered' => $recordsFiltered,
                    ];

                } else {
                    if (isset($paso)) $query->where('paso', $paso);
                    if (isset($fecha)) {
                        $date = Carbon::createFromFormat('Ymd', $fecha)->format('Y-m-d');
                        $query->where('fecha', $date)->where('banco', $banco)->where('carpeta', $carpeta);
                    } else {
                        $query->where('dmc', $banco);
                    }

                    $query = $this->readFilters($query, $request, $cols);

                    $query->where('descripcion', '!=', '');

                    $recordsFiltered = $query->count();

                    if($tabla !== 'paso') $query = $this->skipOrder($request, $query);

                    $data = $query->get();

                    if ($tabla === 'paso') {
                        if(isset($request->select_prueba) && $request->select_prueba != '') {
                            $cabecera = BancoPruebaDescripcion::where('dmc', $request->banco)->where('nPruebas',$request->select_prueba)->first();
                        } elseif ($request->select_prueba === '') {
                            $query = BancoPruebaDescripcion::where('dmc', $request->banco);
                            $count = $query->count();
                            if($count == 1) $cabecera = $query->first();
                        }

                        for($i = 7; $i >=0; $i--){

                            if(!isset($request['iSortCol_'.$i])) continue;

                            if($request['sSortDir_'.$i] == 'asc') {
                                $data = $data->sortBy($request['mDataProp_'.$request['iSortCol_'.$i]]);
                            } else {
                                $data = $data->sortByDesc($request['mDataProp_'.$request['iSortCol_'.$i]]);
                            }
                        }
                    }
                    if($tabla == 'pasos') {
                        foreach ($data as $d) {
                            $d->etapaProceso = $d->etapaProceso == 'B' ? $d->etapaProceso = 'OK' : 'NOK';
                        }
                    }

                    $output = [
                        'data' => $data,
                        'recordsFiltered' => $recordsFiltered,
                    ];
                    if(isset($cabecera)) {
                        $output['cabecera'] = $cabecera;
                    }
                }
            } else {
                if($tabla === 'pruebas') {
                    $query->where($campo,$nombre)->where('carpeta', $carpeta);
                    $query->where('description', '!=', '');
                }

                $query = $this->readFilters($query, $request, $cols);

                $recordsFiltered = $query->count();

                $query = $this->skipOrder($request,$query);

                $data = $query->get();
               
                if($tabla === 'pruebas') {
                    foreach ($data as $item) {
                        $item->formatedfecha = Carbon::createFromFormat('Y-m-d', $item->fecha)->format('Ymd');
                    }
                }

                $output = [
                    'data' => $data,
                    'recordsFiltered' => $recordsFiltered,
                ];
            }

            return response($output,200);
        } catch (\Exception $e) {
            throw($e);
        }
    }

    public function config(Request $request)
    {
        if (isset($request->lang)) {
            if ($request->lang == 'en') {
                App::setLocale($request->lang); //Se cambia el idioma al inglÃƒÂ©s
            }
        }
        $output = [];
        if ($request->tabla == 'visorflashing') {
            $output['dataUrl'] = url('/api/visorflashing?');
            $output['nextUrl'] = "/pruebas/@@nombre@@/@@carpeta@@/";
            $output['cabecera'] = [
                "Tabla" => trans('diccionario.Resumen')
            ];

            $output['order'] = [
                 [
                    'col' => 0,
                    'dir' => 'desc'
                ],
                [
                    'col' => 1,
                    'dir' => 'asc'
                ],
                [
                    'col' => 3,
                    'dir' => 'desc'
                ],
            ];

            $output['columns'] = [
                array('data' => 'fecha', 'title' => trans('diccionario.Fecha'), 'width' => '100px', 'sortable' => true),
                array('data' => 'nombre', 'title' => trans('diccionario.Nombre'), 'min-width' => '150px','sortable' => true),
                array('data' => 'catalogo', 'title' => trans('diccionario.Catalogo'), 'width' => '150px', 'sortable' => true),
                array('data' => 'inicio', 'title' => trans('diccionario.Inicio'), 'width' => '75px', 'sortable' => true),
                array('data' => 'fin', 'title' => trans('diccionario.Fin'), 'width' => '75px', 'sortable' => true),
                array('data' => 'nPruebas', 'title' => trans('diccionario.Total de pruebas'), 'headerClass' => 'text-right', 'htmlClass' => 'text-right', 'width' => '175px', 'sortable' => true),
                array('data' => 'ok', 'title' => 'OK', 'headerClass' => 'text-right', 'htmlClass' => 'text-right', 'width' => '125px', 'sortable' => true),
                array('data' => 'okPer', 'title' => '%OK', 'headerClass' => 'text-right', 'format' => '2Decimales', 'htmlClass' => 'text-right', 'width' => '100px', 'sortable' => true),
                array('data' => 'ko', 'title' => 'NOK', 'headerClass' => 'text-right', 'htmlClass' => 'text-right', 'width' => '125px', 'sortable' => true),
                array('data' => 'nOkPer', 'title' => '%NOK', 'headerClass' => 'text-right', 'format' => '2Decimales', 'htmlClass' => 'text-right', 'width' => '100px', 'sortable' => true)
            ];

            $output['filters'] = [
                ["field" => "fecha", "name" => trans('diccionario.Fecha').' (dd/mm/YYYY)', "operators" => [
                    ["text" => trans('diccionario.entre'), "value" => 'between'],["text" => trans('diccionario.igual'), "value" => '=']
                ]],
                ["field" => "nombre", "name" => trans('diccionario.Nombre'), "operators" => [
                    ["text" => trans('diccionario.igual'), "value" => "="], ["text" => trans('diccionario.contiene'), "value" => "%"]
                ]],
                ["field" => "catalogo", "name" => trans('diccionario.Catalogo'), "operators" => [
                    ["text" => trans('diccionario.igual'), "value" => "="], ["text" => trans('diccionario.contiene'), "value" => "%"]
                ]],
                ["field" => "nPruebas", "name" => trans('diccionario.Total de pruebas'), "operators" => [
                    ["text" => trans('diccionario.entre'), "value" => 'between'],["text" => trans('diccionario.igual'), "value" => '='],
                    ["text" => trans('diccionario.mayor'), "value" => '>'],["text" => trans('diccionario.menor'), "value" => '<']
                ]]
            ];
        } elseif ($request->tabla == 'pruebas') {
            $output['dataUrl'] = url('/api/visorflashing/pruebas/' . $request->banco . '/' . $request->carpeta . '?');
            $output['nextUrl'] = "/pasos/@@banco@@/@@carpeta@@/@@paso@@/@@formatedfecha@@/";
            $output['cabecera'] = [
                "Tabla" => trans('diccionario.Pruebas'),
                'Banco' => $request->banco,
                'Carpeta' => $request->carpeta,
            ];

            $pruebas = FlashingTracePrueba::select('fecha')->where('nombre', $request->banco)->where('carpeta', $request->carpeta)->groupBy('fecha')->get();
            $aux_array[] = [
                'text' => trans('diccionario.select_fecha'),
                'value' => '',
            ];
            foreach($pruebas as $prueba) {
                $aux_array[] = [
                    'text' => $prueba->fecha,
                    'value' => $prueba->fecha,
                ];
            }

            $output['selects'] = [
                [
                    'field' => 'fecha',
                    'options' => $aux_array,
                ],
            ];

            $output['order'] = [
                [
                    'col' => 0,
                    'dir' => 'asc'
                ],
            ];

            $output['columns'] = [
                array('data' => 'paso', 'title' => trans('diccionario.Paso'), 'width' => '50px', 'sortable' => true),
                array('data' => 'fecha', 'title' => trans('diccionario.Fecha'), 'width' => '100px', 'sortable' => true),
                array('data' => 'descripcion', 'title' => trans('diccionario.Descripcion'), 'min-width' => '425px'),
                array('data' => 'etapa', 'title' => trans('diccionario.Etapa'), 'width' => '100px'),
                array('data' => 'minimo', 'title' => trans('diccionario.Minimo'), 'headerClass' => 'text-right', 'htmlClass' => 'text-right', 'width' => '100px'),
                array('data' => 'media', 'title' => trans('diccionario.Media'), 'format' => '2Decimales', 'headerClass' => 'text-right', 'htmlClass' => 'text-right', 'width' => '100px'),
                array('data' => 'maximo', 'title' => trans('diccionario.Maximo'), 'headerClass' => 'text-right', 'htmlClass' => 'text-right', 'width' => '100px'),
                array('data' => 'total', 'title' => trans('diccionario.Total de pruebas'), 'headerClass' => 'text-right', 'htmlClass' => 'text-right', 'width' => '150px'),
                array('data' => 'nOk', 'title' => 'NOK', 'headerClass' => 'text-right', 'htmlClass' => 'text-right', 'width' => '50px'),
                array('data' => 'nOkPer', 'title' => '%NOK', 'format' => '2Decimales', 'headerClass' => 'text-right', 'htmlClass' => 'text-right', 'width' => '75px'),
            ];

            $output['filters'] = [
                ["field" => "paso", "name" => trans('diccionario.Paso'), "operators" => [
                    ["text" => trans('diccionario.igual'), "value" => "="], ["text" => trans('diccionario.contiene'), "value" => "%"]
                ]],
                ["field" => "etapa", "name" => trans('diccionario.Etapa'), "operators" => [
                    ["text" => trans('diccionario.igual'), "value" => "="], ["text" => trans('diccionario.contiene'), "value" => "%"]
                ]],
            ];
        } elseif ($request->tabla == 'pasos') {
            $output['dataUrl'] = url('/api/pasos/' . $request->banco . '/' . $request->carpeta . '/' . $request->paso . '/' . $request->fecha . '?');
            $output['nextUrl'] = "/paso/@@dmc@@/";
            $output['cabecera'] = [
                "Tabla" => trans('diccionario.Pasos'),
                'Pasos' => $request->paso,
                'Carpeta' => $request->carpeta
            ];

            $output['order'] = [
                [
                    'col' => 1,
                    'dir' => 'desc'
                ],
            ];

            $output['columns'] = [
                array('data' => 'dmc', 'title' => 'DMC', 'width' => '225px', 'sortable' => true),
                array('data' => 'inicio', 'title' => trans('diccionario.Inicio'), 'width' => '75px', 'sortable' => true),
                array('data' => 'etapaProceso', 'title' => trans('diccionario.estado'), 'width' => '75px'),
                array('data' => 'vV1', 'title' => trans('diccionario.Valor 1'), 'min-width' => '125px', 'width' => 'calc((100vw - 482px - 2rem)/4)', 'sortable' => true),
                array('data' => 'vV2', 'title' => trans('diccionario.Valor 2'), 'min-width' => '125px', 'width' => 'calc((100vw - 482px - 2rem)/4)', 'sortable' => true),
                array('data' => 'vV3', 'title' => trans('diccionario.Valor 3'), 'min-width' => '125px', 'width' => 'calc((100vw - 482px - 2rem)/4)', 'sortable' => true),
                array('data' => 'vV4', 'title' => trans('diccionario.Valor 4'), 'min-width' => '125px', 'width' => 'calc((100vw - 482px - 2rem)/4)', 'sortable' => true),
            ];

            $output['filters'] = [
                ["field" => "dmc", "name" => 'DMC', "operators" => [
                    ["text" => trans('diccionario.igual'), "value" => "="], ["text" => trans('diccionario.contiene'), "value" => "%"]
                ]],
            ];
        } elseif ($request->tabla == 'paso') {
            if(isset($request->carpeta)) $output['dataUrl'] = url('/api/paso/' . $request->banco . '/'. $request->carpeta . '?'); //$request->banco en este caso es el dmc y $carpeta en este caso es la fecha
            $output['dataUrl'] = url('/api/paso/' . $request->banco . '/'. $request->carpeta . '?'); //$request->banco en este caso es el dmc
            $output['nextUrl'] = null;
            $output['cabecera'] = [
                "Tabla" => trans('diccionario.Paso'),
                'DMC' => $request->banco, //$request->banco en este caso es el dmc
            ];

            $output['order'] = [
                [
                    'col' => 0,
                    'dir' => 'asc'
                ],
                [
                    'col' => 3,
                    'dir' => 'asc'
                ],
            ];

            $bpds = BancoPruebaDescripcion::where('dmc',$request->banco)->get('nPruebas');
            if($bpds->count() > 0) {
                $aux_array[] = [
                    'text' => trans('diccionario.nprueba'),
                    'value' => '',
                ];
                foreach ($bpds as $bpd) {
                    $aux_array[] = [
                        'text' => trans('diccionario.prueba').' '.$bpd->nPruebas,
                        'value' => $bpd->nPruebas
                    ];
                }

                $output['selects'] = [
                    [
                        'field' => 'prueba',
                        'options' => $aux_array,
                        'value' => '',
                    ],
                ];
            }

            $output['columns'] = [
                array('data' => 'paso', 'title' => trans('diccionario.Paso'), 'width' => '50px', 'sortable' => true),
                array('data' => 'descripcion', 'title' => trans('diccionario.Descripcion'), 'min-width' => '325px'),
                array('data' => 'etapa', 'title' => trans('diccionario.Etapa'), 'width' => '75px'),
                array('data' => 'nPruebas', 'title' => trans('diccionario.nprueba'), 'width' => '125px', 'sortable' => true),
                array('data' => 'vV1', 'title' => trans('diccionario.vv1'), 'width' => '200px', 'htmlTitle' => trans('diccionario.li').': @@liV1@@ '.trans('diccionario.ls').': @@lsV1@@'),
                array('data' => 'vV2', 'title' => trans('diccionario.vv2'), 'width' => '200px', 'htmlTitle' => trans('diccionario.li').': @@liV2@@ '.trans('diccionario.ls').': @@lsV2@@'),
                array('data' => 'vV3', 'title' => trans('diccionario.vv3'), 'width' => '200px', 'htmlTitle' => trans('diccionario.li').': @@liV3@@ '.trans('diccionario.ls').': @@lsV3@@'),
                array('data' => 'vV4', 'title' => trans('diccionario.vv4'), 'width' => '200px', 'htmlTitle' => trans('diccionario.li').': @@liV4@@ '.trans('diccionario.ls').': @@lsV4@@'),
            ];

            $output['filters'] = [];

        }elseif ($request->tabla == 'dmc') {
            $output['dataUrl'] = url('/api/dmc/' . $request->banco . '?'); //$request->banco en este caso es el dmc
            $output['nextUrl'] = "/paso/@@dmc@@/";
            $output['cabecera'] = [
                "Tabla" => trans('diccionario.resultados_dmc'),
            ];

            $output['order'] = [
                [
                    'col' => 0,
                    'dir' => 'desc'
                ],
                [
                    'col' => 1,
                    'dir' => 'desc'
                ],
            ];

            $output['columns'] = [
                array('data' => 'dmc', 'title' => 'DMC', 'width' => '225px','sortable' => true),
                array('data' => 'fecha', 'title' => trans('diccionario.Fecha'), 'width' => '100px','sortable' => true),
                array('data' => 'nPruebas', 'title' => trans('diccionario.nprueba'), 'width' => '125px', 'sortable' => true),
                array('data' => 'banco', 'title' => trans('diccionario.Banco'), 'sortable' => true),
                array('data' => 'catalogo', 'title' => trans('diccionario.Catalogo'), 'width' => '150px'),
                array('data' => 'inicio', 'title' => trans('diccionario.Inicio'), 'width' => '75px','sortable' => true),
                array('data' => 'ok', 'title' => 'OK', 'width' => '50px'),
                array('data' => 'ko', 'title' => 'NOK', 'width' => '50px'),
            ];

            $output['filters'] = [
                ["field" => "fecha", "name" => trans('diccionario.Fecha'), "operators" => [
                    ["text" => trans('diccionario.entre'), "value" => 'between'],["text" => trans('diccionario.igual'), "value" => '=']
                ]],
            ];
        }

        App::setLocale($this->lang); //Se deshace el cambio de idioma.

        return response($output, 200);

    }

    public function leerXML()
    {
        try {
            $storage = $this->storage . '/FlashingTraceability/';
            //BUSCA TODAS LAS CARPETAS BAJO DATOS
            $ok = $this->buscarCarpetas($storage);

            return response("Datos importados de los excel con Ã©xito");
        } catch (\Exception $e) {
            throw($e);
        }
    }

    private function buscarCarpetas($path, $flasher = null)
    {
        $stop = false;

        foreach (scandir($path) as $elemento) { //HASTA QUE NO ENCUENTRA UN FICHERO, SIGUE BUSCANDO EN SUBCARPETAS
            $estado_servicio = file_get_contents($this->storage.'/estado_servicio_flashing.txt');

            $estado_servicio = explode('=',$estado_servicio);

            if($estado_servicio[0] === 'estado' && $estado_servicio[1] === 'detenido') {echo 'El servicio esta detenido';die;}

            if ($stop) continue;

            if ($elemento == "." || $elemento == "..") continue;

            if (is_numeric(strpos($elemento, 'flasher')) || is_numeric(strpos($elemento, 'Flasher'))) $flasher = $elemento;

            if (is_dir($path . '/' . $elemento)) {
                //var_dump($path . '/' . $elemento.' Es una carpeta');
                $this->buscarCarpetas($path . '/' . $elemento, $flasher);
            } else {
                $stop = true;
                //var_dump($path . '/' . $elemento.' Es un archivo');
                //CUANDO ENCUENTRA UN ARCHIVO XML
                $this->listarArchivos($path, $flasher);
            }
        }
    }

    private function listarArchivos($path, $banco)
    {
        $totalOk = 0;
        $totalnOk = 0;
        $fecha = null;
        $inicio = null;
        $fin = null;
        $catalogo = 'error';
        $minValor = null;
        $maxValor = null;
        $pruebas = [];

        $flashingtrace = new FlashingTrace();

        $dir = opendir($path);
        //ABRE EL ARCHIVO XML

        $folder = explode('/', $path)[count(explode('/', $path)) - 1];
        $nombre = explode('/', $path)[count(explode('/', $path)) - 2];
        $carp = explode('#', $folder);

        $count = 0;
        while ($elemento = readdir($dir)) {

            if (is_dir($path . '/' . $elemento)) continue;

            if ($elemento == '.' || $elemento == '..') continue;

            if (substr($elemento, -2) === 'ok' || substr($elemento, -2) === 'ko') continue;

            $datosArchivo = $this->leerArchivo($path, $elemento, $pruebas);
           
            if ($datosArchivo) {
                
                if ($inicio == null || $inicio > $datosArchivo['hora']) $inicio = $datosArchivo['hora'];
                if ($fin == null || $fin < $datosArchivo['hora']) $fin = $datosArchivo['hora'];
                if ($fecha == null || $fecha < $datosArchivo['fecha']) $fecha = $datosArchivo['fecha'];

                if ($datosArchivo['ok']) {
                    rename($path . '/' . $elemento, $path . '/' . $elemento . '.ok');
                    $totalOk++;
                } else {
                    rename($path . '/' . $elemento, $path . '/' . $elemento . '.ko');
                    $totalnOk++;
                }
                $catalogo = $datosArchivo['catalogo'];
            }

            $arr_flasherpaso = $datosArchivo['arr_flasherpaso'];

            $generalInfo = $datosArchivo['generalInfo'];

            $generalResult = $datosArchivo['generalResult'];

            if($count == 0) {
                $formatFecha = date('Y-m-d', strtotime(str_replace(':', '-', $fecha)));
                $flashingtrace->fecha = $formatFecha;
                $flashingtrace->nombre = $nombre;
                $flashingtrace->carpeta = $carp[0];
                $flashingtrace->catalogo = $catalogo;
                $flashingtrace->inicio = $inicio;
                $flashingtrace->fin = $fin;
                $flashingtrace->nPruebas = 0;
                $flashingtrace->ok = $totalOk;
                $flashingtrace->okPer = 0;
                $flashingtrace->ko = $totalnOk;
                $flashingtrace->nOkPer = 0;
                $flashingtrace->save();
                $count++;
            }
            
            $cabecera = FlashingTraceCabecera::create([
                'customer' => is_array($generalInfo['Customer']) ? '' : $generalInfo['Customer'],
                'part_number' => is_array($catalogo) ? '' : $catalogo,
                'dmc' => $generalInfo['DMC'],
                'plant' => $generalInfo['Plant'],
                'manufacture_date' => Carbon::createFromFormat('d-m-Y',$datosArchivo['fecha'])->format('Y-m-d'),
                'manufacture_time' => $datosArchivo['hora'],
                'name' => $generalInfo['Flasher']['Name'],
                'os_x0020_version' => $generalInfo['Flasher']['OS_x0020_Version'],
                'dot_x0020_net_x0020_version' => $generalInfo['Flasher']['DOT_x0020_NET_x0020_Version'],
                'flasher_x0020_version' => $generalInfo['Flasher']['Flasher_x0020_Version'],
                'section' => $generalInfo['Flasher']['Section'],
                'reference' => is_array($generalInfo['FlashSequence']['Reference']) ? '' : $generalInfo['FlashSequence']['Reference'],
                'entry_point' => $generalInfo['FlashSequence']['EntryPoint'],
                'rework_counter' => $generalInfo['FlashSequence']['ReWorkCounter'],
                'data_in_1' => is_array($generalInfo['DataIn']['DataIn_1']) ? '' : $generalInfo['DataIn']['DataIn_1'],
                'data_in_2' => is_array($generalInfo['DataIn']['DataIn_2']) ? '' : $generalInfo['DataIn']['DataIn_2'],
                'data_in_3' => is_array($generalInfo['DataIn']['DataIn_3']) ? '' : $generalInfo['DataIn']['DataIn_3'],
                'data_in_4' => is_array($generalInfo['DataIn']['DataIn_4']) ? '' : $generalInfo['DataIn']['DataIn_4'],
                'data_in_5' => is_array($generalInfo['DataIn']['DataIn_5']) ? '' : $generalInfo['DataIn']['DataIn_5'],
                'data_in_6' => is_array($generalInfo['DataIn']['DataIn_6']) ? '' : $generalInfo['DataIn']['DataIn_6'],
                'data_in_7' => is_array($generalInfo['DataIn']['DataIn_7']) ? '' : $generalInfo['DataIn']['DataIn_7'],
                'data_in_8' => is_array($generalInfo['DataIn']['DataIn_8']) ? '' : $generalInfo['DataIn']['DataIn_8'],
                'data_in_9' => is_array($generalInfo['DataIn']['DataIn_9']) ? '' : $generalInfo['DataIn']['DataIn_9'],
                'data_in_10' => is_array($generalInfo['DataIn']['DataIn_10']) ? '' : $generalInfo['DataIn']['DataIn_10'],
                'result' => $generalResult['Result'],
                'next_entry_point' => is_array($generalResult['Next_Entry_Point']) ? '' : $generalResult['Next_Entry_Point'],
                'error_code' => is_array($generalResult['Error_Code']) ? '' : $generalResult['Error_Code'],
                'cycle_time' => $generalResult['Cycle_Time'],
                'data_out_1' => is_array($generalResult['DataOut_1']) ? '' : $generalResult['DataOut_1'],
                'data_out_2' => is_array($generalResult['DataOut_2']) ? '' : $generalResult['DataOut_2'],
                'data_out_3' => is_array($generalResult['DataOut_3']) ? '' : $generalResult['DataOut_3'],
                'data_out_4' => is_array($generalResult['DataOut_4']) ? '' : $generalResult['DataOut_4'],
                'data_out_5' => is_array($generalResult['DataOut_5']) ? '' : $generalResult['DataOut_5'],
                'data_out_6' => is_array($generalResult['DataOut_6']) ? '' : $generalResult['DataOut_6'],
                'data_out_7' => is_array($generalResult['DataOut_7']) ? '' : $generalResult['DataOut_7'],
                'data_out_8' => is_array($generalResult['DataOut_8']) ? '' : $generalResult['DataOut_8'],
                'data_out_9' => is_array($generalResult['DataOut_9']) ? '' : $generalResult['DataOut_9'],
                'data_out_10' => is_array($generalResult['DataOut_10']) ? '' : $generalResult['DataOut_10'],
                'mes_1' =>  is_array($generalResult['MES_1']) ? '' : $generalResult['MES_1'],
                'mes_2' =>  is_array($generalResult['MES_2']) ? '' : $generalResult['MES_2'],
                'mes_3' =>  is_array($generalResult['MES_3']) ? '' : $generalResult['MES_3'],
                'mes_4' =>  is_array($generalResult['MES_4']) ? '' : $generalResult['MES_4'],
                'mes_5' =>  is_array($generalResult['MES_5']) ? '' : $generalResult['MES_5'],
                'name' => $nombre,
                'carpeta' => $carp[0],
            ]);

            $arr_ps = [];
            foreach ($arr_flasherpaso as $paso) {
                $paso['flashingtrace_cabecera_id'] = $cabecera->id;
                $paso['nombre'] = $nombre;
                $paso['carpeta'] = $carp[0];
                $arr_ps[] = $paso;
            }
            FlashingTracePaso::insert($arr_ps);
        }

        if ($totalOk === 0 && $totalnOk === 0) return false;

        $totalPruebas = $totalOk + $totalnOk;

        $okPer = round(($totalOk / $totalPruebas * 100), 2);
        $nOkPer = round(($totalnOk / $totalPruebas * 100), 2);

        $flashingtrace->nPruebas = $totalPruebas;
        $flashingtrace->okPer = $okPer;
        $flashingtrace->nOkPer = $nOkPer;
        $flashingtrace->save();

        foreach ($pruebas as $i => $d) {

            $pruebas[$i]['media'] = round($d['media'] / $d['total'], 2);

            $pruebas[$i]['nOkPer'] = round($d['nOk'] * 100 / $d['total'], 2);

            $pruebas[$i]['carpeta'] = $carp[0];

        }

        if ($pruebas !== false) {
            $arr_pr = [];
            foreach ($pruebas as $p) {
                if (is_array($p['description'])) {
                    $p['description'] = '';
                }
                $p['fecha'] = $flashingtrace->manufacture_date;
                $p['paso'] = $p['paso']-1;
                $arr_pr[] = $p;
            }
            FlashingTracePrueba::insert($arr_pr);
        }


    }

    private function leerArchivo($path, $elemento, &$carpeta)
    {

        $nombre = explode('/', $path)[count(explode('/', $path)) - 2];

        $res = $this->xml2json($path . '/', $elemento);

        $arr_flasherpaso = [];

        for ($i = 0; $i < count($res['PasoXml']); $i++) {

            $paso = $res['PasoXml'][$i];
            $datosPaso = $this->leerPaso($path, $paso);

            $arr_flasherpaso[] = $datosPaso['arr_flasherpaso'];

            if (!isset($carpeta[$i])) {

                $carpeta[$i] = [
                    'nombre' => $nombre,
                    'minimo' => $datosPaso['arr_flasherpaso']['min'],
                    'maximo' => $datosPaso['arr_flasherpaso']['max'],
                    'media' => 0,
                    'total' => 0,
                    'nOk' => 0,
                    'paso' => $i + 1,
                    'section' => $datosPaso['arr_flasherpaso']['section'],
                    'description' => $datosPaso['arr_flasherpaso']['description']
                ];

            }

            $datosPaso['valor'] = 0;
            if ($datosPaso['valor'] > $carpeta[$i]['maximo']) $carpeta[$i]['maximo'] = $datosPaso['valor'];

            if ($datosPaso['valor'] < $carpeta[$i]['minimo']) $carpeta[$i]['minimo'] = $datosPaso['valor'];

            if ($datosPaso['arr_flasherpaso']['step_result'] !== 'OK') $carpeta[$i]['nOk']++;

            $carpeta[$i]['media'] += $datosPaso['valor'];

            $carpeta[$i]['total']++;

        }
        
        return [
            'fecha' => $res['GeneralInfo']['ManufactureData']['Date'],
            'hora' => $res['GeneralInfo']['ManufactureData']['Time'],
            'ok' => $res['Flash_Sequence_Result']['Result'] == 'OK',
            'catalogo' => is_array($res['GeneralInfo']['PartNumber']) ? '' : $res['GeneralInfo']['PartNumber'],
            'generalInfo' => $res['GeneralInfo'],
            'generalResult' => $res['Flash_Sequence_Result'],
            'arr_flasherpaso' => $arr_flasherpaso,
        ];

    }

    private function xml2json($path, $elemento)
    {

        $xmlfile = file_get_contents($path . '/' . $elemento);
        $xml = simplexml_load_string($xmlfile);

        $doc = (json_decode(json_encode($xml), true));

        return $doc;

    }
    
    private function leerPaso($path, $paso)
    {
        $arr_flasherpaso = [
            'step_result' => is_array($paso['Step_Result']) ? '' : $paso['Step_Result'],
            'description' => is_array($paso['Description']) ? '' : $paso['Description'],
            'value_ascii' => is_array($paso['Value_ASCII']) ? '' : $paso['Value_ASCII'],
            'step' => $paso['@attributes']['PosNr'],
            'min' => is_array($paso['Min']) ? 0 : $paso['Min'],
            'value' => is_array($paso['Value']) ? '' : $paso['Value'],
            'max' => is_array($paso['Max']) ? 0 : $paso['Max'],
            'dimension' => is_array($paso['Dimension']) ? '' : $paso['Dimension'],
            'section' => is_array($paso['Section']) ? '' : $paso['Section'],
            'info' => is_array($paso['Info']) ? '' : $paso['Info'],
        ];

        return [
            'arr_flasherpaso' => $arr_flasherpaso,
        ];

    }

    private function readFilters($query, $request, $cols = null)
    {
        if (isset($request->filters)) {
            if (!is_array($request->filters)) $request->filters = json_decode($request->filters);
            foreach ($request->filters as $filter) {
                if (is_string($filter)) $filter = json_decode($filter);

                if (!isset($filter->field) || !isset($filter->operator)) continue;

                if ($filter->field == 'fecha') {
                    if ($filter->operator == 'between') {

                        if ((!is_numeric($filter->value[0]) && is_string($filter->value[0]) && (strpos($filter->value[0], '-') >= 0 || strpos($filter->value[0], '/') >= 0))
                            && (!is_numeric($filter->value[1]) && is_string($filter->value[1]) && (strpos($filter->value[1], '-') >= 0 || strpos($filter->value[1], '/') >= 0))) {
                            $fechaDesde = $this->setDateAttribute($filter->value[0]);
                            $fechaHasta = $this->setDateAttribute($filter->value[1]);

                            if(is_array($fechaDesde) || is_array($fechaHasta)) return ['error' => 400];

                            $fechaDesde = Carbon::createFromFormat('Y-m-d', $fechaDesde);
                            $fechaHasta = Carbon::createFromFormat('Y-m-d', $fechaHasta);

                            if($fechaDesde->lt($fechaHasta)) {
                                $between = [$fechaDesde, $fechaHasta];
                            } else {
                                $between = [$fechaHasta, $fechaDesde];
                            }

                            $query->whereBetween($filter->field, $between);
                        } else {
                            return ['error' => 400];
                        }

                    } else {

                        if (!is_numeric($filter->value[0]) && is_string($filter->value[0]) && (strpos($filter->value[0], '-') >= 0 || strpos($filter->value[0], '/') >= 0)) {
                            $fechaDesde = $this->setDateAttribute($filter->value[0]);

                            if(is_array($fechaDesde)) return ['error' => 400];

                            $query->where($filter->field, $fechaDesde);
                        } else {
                            return ['error' => 400];
                        }

                    }
                } else {
                    if ($filter->operator === 'between') $query->whereBetween($filter->field, $filter->value);
                    elseif ($filter->operator === '%') $query->where($filter->field, 'like', '%' . $filter->value[0] . '%');
                    else $query->where($filter->field, $filter->operator, $filter->value[0]);
                }
            }
        }

        if (isset($request->select_fecha)) $query->where('fecha', $request->select_fecha);

        if (isset($request->select_prueba) && $request->select_prueba != '') {
//            $bpd = BancoPruebaDescripcion::where('dmc', $request->banco)->where('nPruebas', $request->select_prueba)->first();
//            $fecha = str_replace(' ', '', $bpd->fecha);
            $query->where('nPruebas', $request->select_prueba);
        }

        if (isset($request->sSearch) && $request->sSearch != '' && isset($cols)) {
            $query->where(function ($query) use ($request, $cols) {
                foreach ($cols as $col) {
                    $query->orWhere($col, 'like', '%' . $request->sSearch . '%');
                }
            }
            );
        }
        return $query;
    }

    private function setDateAttribute($date)
    {
        try {
            $approvedFormats = ['d/m/Y', 'Y-m-d', 'd-m-Y', 'y-m-d', 'd-m-y', 'd M Y'];
            foreach ($approvedFormats as $format) {
                if ($carbon = Carbon::createFromFormat($format, $date)) {
                    return $carbon->format('Y-m-d');
                } else {
                    continue;
                }
            }
        } catch (\Exception $e) {
            return ['message' => 'Las fechas introducidas tienen un formato incorrecto. El formato correcto es d/m/Y', 400];
        }
    }

    private function skipOrder($request, $query) {
        if (isset($request->iDisplayStart)) {
            $query->skip($request->iDisplayStart)->take($request->iDisplayLength);
        }

        $c = 0;
        foreach ($request->all() as $k => $v) {
            if (strpos($k, 'iSortCol_') === false) continue;

            $query->orderBy($request['mDataProp_' . $v], $request['sSortDir_' . $c]);
            $c++;

        }

        return $query;
    }
}
