<?php

namespace App\Http\Controllers\Auth;

use App\Cargos;
use App\Cargoslineasempleados;
use App\Empleados;
use App\Http\Controllers\Controller;
use App\Http\Controllers\RegistrocalidadController;
use App\Line;
use App\User;
use App\Zona;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    public function username () {

        return 'name';

    }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function apiLogin (Request $request) {

        $this->validate($request, [
            'password' => 'required',
            'name' => 'required'
        ]);

        /*if (Auth::attempt(['name' => $request->name, 'password' => $request->password])) {

            $user = Auth::user();

            $user->token = $user->createToken('api')->accessToken;
            return response($user);

        }*/
        if (Auth::attempt(['name' => $request->name, 'password' => $request->password])) {

            try {
                $user = Auth::user();//Conectamos al DMS para obtener un token
                $client = new Client([
                    'base_uri' => config('app.dms_url'),
                    'header' => [
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                    ]
                ]);
                $result = $client->post('login', [
                    'form_params' => [
                        'username' => $request->name,
                        'password' => $request->password,
                    ]
                ]);
                $user->_tokenDMS = json_decode($result->getBody(), true)['access_token'];
                $user->save();
                $user->token = $user->_tokenDMS;

                /* Lanzar cambio de turno y dia de la semana automaticamente cuando se conecta el PO de la linea, para el día y turno concreto. */
                $empleado = Empleados::findOrFail($user->empleados_id);
                if(isset($empleado->default_linea_id)) {
                    $cargoslinea = Cargoslineasempleados::where('empleados_id', $empleado->id)->where('lineas_id',$empleado->default_linea_id)->get();
                    if(isset($cargoslinea) && !$cargoslinea->isEmpty()) {
                        foreach ($cargoslinea as $cargolinea) {
                            $arr_cargos[] = $cargolinea->cargos_id;
                        }
                        $cargos = Cargos::whereIN('id',$arr_cargos)->where('resumen','like','%PO%')->get();
                        if(isset($cargos)&& !$cargos->isEmpty()) {

                            /*
                             * TURNO 1 = 6-14
                             * TURNO 2 = 14-22
                             * TURNO 3 = 22-6
                             * TURNO 4 S-D = 6-18
                             * TURNO 5 S-D = 18-6
                            */
                            $daysSpanish = [
                                0 => 'domingo',
                                1 => 'lunes',
                                2 => 'martes',
                                3 => 'miercoles',
                                4 => 'jueves',
                                5 => 'viernes',
                                6 => 'sabado',
                            ];


                            $hora = Carbon::now()->format('H');
                            $dia = $daysSpanish[Carbon::now()->dayOfWeek];

                            if($hora >= 6 && $hora < 14) {
                                $valor = '1';
                            } elseif($hora >= 14 && $hora < 22) {
                                $valor = '2';
                            } elseif($hora >= 22 || $hora < 6) {
                                $valor = '3';
                            }

                            if($dia === 'sabado' || $dia === 'domingo') {
                                if($hora >= 6 && $hora < 18) {
                                    $valor = '4';
                                } elseif($hora >= 18 || $hora < 6) {
                                    $valor = '5';
                                }
                            }

                            $linea = Line::findOrFail($empleado->default_linea_id);
                            $zonas = Zona::where('linea_id',$linea->id)->get();
                            if(!$zonas->isEmpty()) {

                                $client = new Client([
                                    'base_uri' => config('app.url').'/evaluacionProcesos/public/index.php/api/',
                                    'headers' => [
                                        'Content-Type' => 'application/json',
                                        'Accept' => 'application/json',
                                        'Authorization' => 'Bearer '.$user->token,
                                    ]
                                ]);

                                foreach ($zonas as $zona) {
                                    $result = $client->post('registroscalidad', [
                                        'form_params' => [
                                            'tipo' => 'semana',
                                            'valor' => $dia. ' ' . $valor,
                                            'linea' => $empleado->default_linea_id,
                                            'zona' => $zona->id,
                                            'auto' => true,
                                        ],
                                    ]);
                                    $result = $client->post('registroscalidad', [
                                        'form_params' => [
                                            'tipo' => 'turno',
                                            'valor' => $valor,
                                            'linea' => $empleado->default_linea_id,
                                            'zona' => $zona->id,
                                            'auto' => true,
                                        ],
                                    ]);
                                }
                            }
                        }
                    }

                }

                return response($user);
            } catch (\Exception $e) {
                throw($e);
            }
        }

        return response(['message' => 'Nombre de usuario o contraseña no coinciden'], 401);

    }

}
