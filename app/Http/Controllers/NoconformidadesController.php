<?php

namespace App\Http\Controllers;

use App\Mail\noConformidadEmail;
use App\Procesos;
use App\Procesoscalidad;
use Illuminate\Support\Facades\Mail;
use App\Confirmacionprocesos;
use App\Empleados;
use App\Noconformidades;
use App\Registrocalidad;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NoconformidadesController extends AppController
{
    public function index(Request $request)
    {
        if (!isset($request->tipo)) return response(['message' => 'Debe especificar el tipo de aplicacion'], 422);

        if (!isset($this->aplicaciones[$request->tipo])) return response(['message' => 'No existe la aplicacion especificada'], 422);

        $model = $this->aplicaciones[$request->tipo]['opl'];

        $nombres = $this->aplicaciones[$request->tipo]['nombre'];

        $query = Noconformidades::whereIn('opl', $nombres);

        $query = $this->readFilters($request, $query, $model);

        $noconfs = $query->get();

        $output = [
            'data' => [],
            'procesos' => [],
        ];

        if($noconfs->isNotEmpty()) {
            foreach ($noconfs as $noconf) {
                $nombre = $model::where('id', $noconf->aplicacion_id)->first();
                $noconf->nombreproceso = isset($nombre->resumen) ? $nombre->resumen : $nombre->alternador;

                if(isset($request->nombreproceso) && $request->nombreproceso != $noconf->nombreproceso) continue;

                $noconf->fechaInicio = Carbon::createFromFormat('Y-m-d',$noconf->fechaInicio)->format('d/m/Y');

                if(isset($noconf->fechaFinal) && $noconf->fechaFinal != null && $noconf->fechaFinal != '') {
                    $noconf->fechaFinal = explode(' ',$noconf->fechaFinal)[0];
                    $noconf->fechaFinal = Carbon::createFromFormat('Y-m-d',$noconf->fechaFinal)->format('d/m/Y');
                }
                if(isset($noconf->fechaCierre) && $noconf->fechaCierre != null && $noconf->fechaCierre != '') {
                    $noconf->fechaCierre = Carbon::createFromFormat('Y-m-d', $noconf->fechaCierre)->format('d/m/Y');
                }
                $noconf->resumen = $noconf->resumen.' -> '.$noconf->descripcion;
                
                if(isset($noconf->empleados_id)) {
                    $noconf->empleados_id = intval($noconf->empleados_id);
                    $empl = Empleados::where('id', $noconf->empleados_id)->first();
                    $noconf->empleado_nombre = isset($empl->nombre) ? $empl->nombre : '';
                } else {
                    $noconf->empleado_nombre = '';
                }
                
                if(isset($noconf->evaluador)) {
                    $noconf->evaluador = intval($noconf->evaluador);
                    $eval = Empleados::where('id', $noconf->evaluador)->first();
                    $noconf->evaluador_nombre = isset($eval->nombre) ? $eval->nombre: '';	
                } else {
                    $noconf->evaluador_nombre = '';
                }

                $output['data'][] = $noconf;
            }
        }

        $output['procesos'] = $request->tipo == 'confirmacionproceso' ? Procesos::get('resumen') : Procesoscalidad::get('resumen');

        return response($output);
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $date = Carbon::today();
            $date = $date->format('Y-m-d');

            $destinataros = null;

            /*
             * opl = "confirmacionesproceso" / "registroscalidad"
             * el id siempre es request->confirmacion_id
             * */
            $no_conf = new Noconformidades();
            $nombre = '';

            if (!isset($request->opl)) return response(['message' => 'Debe especificar el tipo de aplicación'], 422);

            $model = $this->aplicaciones[$request->opl]['opl'];
            $checklist = $this->aplicaciones[$request->opl]['checklist'];
            $tabla = $this->aplicaciones[$request->opl]['tabla'];
            $this->validate($request, [
                'confirmacion_id' => ['required', 'exists:' . $tabla . ',id'],
            ]);
            $model = $model::where('id', $request->confirmacion_id)->first();
            $fechaFinal = $model->fechaFinal;
            if (isset($request->fechaFinal) && $request->fechaFinal != '' && $request->fechaFinal != null) {
                $fechaFinal = $request->fechaFinal;
            }

            $pregunta = $checklist::where($this->aplicaciones[$request->opl]['foranea_checklist'],$request->confirmacion_id)
                ->where('id', $request->registro_id)
                ->first();

            $destinataros = '';
            if(isset($pregunta) && isset($pregunta->evaluador) && !empty($pregunta->evaluador)) {

                $destinataros = $pregunta->evaluador;

            }elseif (isset($request->empleados_id) && !empty($request->empleados_id)) {
                $destinataros = $request->empleados_id;
            }

            $no_conf->aplicacion_id = $request->confirmacion_id;
            $no_conf->fase = $request->fase;
            $no_conf->resumen = $request->resumen;
            $no_conf->descripcion = $request->descripcion;
            $no_conf->comentario = $request->comentario;
            $no_conf->valor = $request->valor;
            $no_conf->fechaInicio = $date;
            $no_conf->fechaFinal = $fechaFinal;
            $no_conf->empleados_id = $destinataros;
            $no_conf->evaluador = auth()->user()->empleados_id;
            $no_conf->actuaciones = $request->actuaciones;
            $no_conf->opl = $request->opl;
            $no_conf->estado = 'planificar';
            $no_conf->save();

            if (isset($no_conf->id) && $no_conf->id != null) {
                $no_conf = Noconformidades::where('id', $no_conf->id)->first();

                $model = $this->aplicaciones[$no_conf->opl]['opl'];
                $nombre = $model::where('id', $no_conf->aplicacion_id)->first();
                $no_conf->nombreproceso = isset($nombre->resumen) ? $nombre->resumen : $nombre->alternador;
                $no_conf->nombreproceso = $nombre->resumen;
                $empl = Empleados::whereIn('id', explode(',',$no_conf->empleados_id))->get();
                if(isset($empl)) {
                    $empleados_id = '';
                    foreach ($empl as $e) {
                        $empleados_id.= $e->nombre.',';
                    }
                    $no_conf->empleados_id = substr($empleados_id,0,-1);
                }
                if(isset($no_conf->evaluador)) {
                    $eval = Empleados::findOrFail($no_conf->evaluador);
                    $no_conf->evaluador = $eval->nombre;
                } else {
                    $no_conf->evaluador == '';
                }

                /*Envío de email al evaluador informando de la nueva conformidad*/
                try {
                    if(isset($empl)) {
                        foreach ($empl as $e) {
                            Mail::to($e->email)->send(new noConformidadEmail($no_conf));
                            if(is_array(Mail::failures()) && !empty(Mail::failures())) {
                                return response(Mail::failures());
                            }
                        }
                    }
                } catch (\Exception $e) {
                    return response ($e, 500);
                }
            }

            DB::commit();
            return response($no_conf);

        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function show($id)
    {
        try {
            DB::beginTransaction();
            $noconformidades = Noconformidades::where('id', $id)->first();
            if (!$noconformidades) return response(['message' => 'No existe el recurso solicitado'], 404);
            DB::commit();
            return response($noconformidades);
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $noconformidades = Noconformidades::findOrFail($id);

            if (!$noconformidades) return response(['message' => 'No existe el recurso solicitado'], 404);

            if(auth()->user()->empleados_id != $noconformidades->empleados_id && auth()->user()->empleados_id != $noconformidades->evaluador) {
                return response(['message' => 'No tiene permisos para modificar la OPL'], 403);
            }

            if((isset($request->fechaInicio) && empty($request->fechaInicio)) || (isset($request->fechaInicio) && empty($request->fechaFinal))) {
                return response(['message' => 'Fecha incorrecta'], 422);
            }

            if(isset($request->fechaFinal)) {
                $expl = explode(' ',$request->fechaFinal);
                if(isset($expl[0])) {
                    $ffinal = $expl[0];

                }
            }

            $noconformidades->update($request->all());
            if(isset($ffinal)) {
                $noconformidades->fechaFinal = $ffinal;
                $noconformidades->save();
            }

            if($request->estado == 'cerrado' && (empty($request->fechaCierre) || $request->fechaCierre === 'Invalid date') ) {
                $noconformidades->fechaCierre = Carbon::now()->format('Y-m-d');
            }
            $noconformidades->save();

            $empl = Empleados::where('id', $noconformidades->empleados_id)->first('nombre');
            $noconformidades->empleado_nombre = $empl->nombre;
            $eval = Empleados::where('id', $noconformidades->evaluador)->first('nombre');
            $noconformidades->evaluador_nombre = $eval->nombre;
            if (isset($noconformidades->fechaInicio) && $noconformidades->fechaInicio != '' && $noconformidades->fechaInicio != 'Invalid date') {
                $noconformidades->fechaInicio = Carbon::createFromFormat('Y-m-d', $noconformidades->fechaInicio)->format('d/m/Y');
            }
            if (isset($noconformidades->fechaFinal) && $noconformidades->fechaFinal != '' && $noconformidades->fechaFinal != 'Invalid date') {
                $noconformidades->fechaFinal = Carbon::createFromFormat('Y-m-d', $noconformidades->fechaFinal)->format('d/m/Y');
            }
            if (isset($noconformidades->fechaCierre)  && $noconformidades->fechaCierre != '' && $noconformidades->fechaCierre != 'Invalid date') {
                $noconformidades->fechaCierre = Carbon::createFromFormat('Y-m-d', $noconformidades->fechaCierre)->format('d/m/Y');
            }

            DB::commit();
            return response($noconformidades);
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            if ($id <= 0 || $id == null) return response(['message' => 'No existe el recurso solicitado'], 404);

            Noconformidades::where('id', $id)->delete();
            DB::commit();
            return response(['message' => 'ok']);
    }

    public function readFilters($request, $query, $model) {

        $campolinea = $this->aplicaciones[$request->tipo]['campolinea'];

        //<editor-fold desc="IF_ISSET_REQUESTS">

        if (!isset($request->cerrado) || empty($request->cerrado)) $query->where('estado', '!=', 'cerrado');

        if(isset($request->aplicacion_id) && is_array($request->aplicacion_id)) {

            $query->whereIn('aplicacion_id', $request->aplicacion_id);

        } elseif(isset($request->aplicacion_id)) {

            $query->where('aplicacion_id', $request->aplicacion_id);

        } elseif (isset($request->linea_id) && !isset($request->aplicacion_id) && !empty($campolinea)) {

            $apps = $model::where($campolinea, $request->linea_id)->get('id')->toArray();

            $query->whereIn('aplicacion_id', $apps);

        }
        //</editor-fold>

        //<editor-fold desc="Filters_And_Sorts">
        if(isset($request->sortBy) && !empty($request->sortBy) && isset($request->sortDesc)) {
            $request->sortDesc === false ? $query->orderBy($request->sortBy, 'ASC') : $query->orderBy($request->sortBy, 'DESC');
        }
//        if(isset($request->perPage) && isset($request->currentPage)) {
//            $query->skip(($request->currentPage - 1) * $request->perPage)->take($request->perPage);
//        }
        //</editor-fold>

        return $query;
    }

}
