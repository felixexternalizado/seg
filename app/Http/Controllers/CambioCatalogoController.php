<?php

namespace App\Http\Controllers;

use App\CambioCatalogo;
use App\ChecklistCambiocatalogoRegistros;
use App\Documento;
use App\Empleados;
use App\Library\officeClass;
use App\Library\SAP;
use App\Line;
use App\Noconformidades;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CambioCatalogoController extends Controller
{
    public function index(Request $request)
    {
        $query = CambioCatalogo::with('empleado')
            ->where('estado', '!=', 'cerrado');

        if(isset($request->linea_id)) $query->where('linea_id', $request->linea_id);

        $cc = $query->orderBy('fechaInicio','DESC')->get();

        return response($cc);
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $this->validate($request, [
                'alternador' => ['required'],
                'linea_id' => ['required', 'exists:lineas,id'],
            ]);

//$request->alternador = 1234.2020.01.16.14.00@LISTA
//$request->alternador = 1234.2020.01.16.14.00@INSTRUCCIONES
//$request->alternador = 1234.2020.01.16.14.00@PCONJUNTO
//$request->alternador = 1234.2020.01.16.14.00@POFERTA
//$request->alternador = 1234.2020.01.16.14.00@PMARCAJES

            $linea = Line::findOrFail($request->linea_id);

            $alternador = $request->alternador;

//            $array = [
//                'alternador' => $alternador,
//               'linea' => $request->linea_id,
//                'storage' => $this->storage.'/cambiocatalogo/plantillas',
//                'app' => 'cambiocatalogo'
//            ];

            $json = (new ExcelCatalogoController())->getJson($alternador,$linea);
            if (!is_array($json)) {
                $json = json_decode($json, true);
                if ($json[0] != 200) {
                    return response(['message' => $json['message']], 404);
                }
            }
            $empleado = Empleados::findOrFail(auth()->user()->empleados_id);

            $cambiocatalogo = CambioCatalogo::create([
                'alternador' => explode('@', $alternador)[0],
                'fechaInicio' => Carbon::now()->format('Y-m-d'),
                'fechaFinal' => '',
                'empleado_id' => $empleado->id,
                'linea_id' => $request->linea_id,
                'totalMal' => 0,
                'totalBien' => 0,
                'porcentajeBien' => 0,
                'totalPreguntas' => 0,
                'piezasRealizadas' => 0,
                'piezasEsperadas' => 0,
                'piezasPerdidas' => 0,
                'tiempoConsumido' => 0,
            ]);
            $totalPreguntas = 0;
            foreach ($json['entidad']['registros'] as $registro) {
                if (isset($registro)) {
                    $checkcamb = new ChecklistCambiocatalogoRegistros();
                    $checkcamb->cambiocatalogo_id = $cambiocatalogo->id;

                    if($registro['tipo'] == '!LI') $registro['valor'] = $linea->resumen;

                    foreach ($registro as $key => $value) {
                        $nombre = $key;
                        if ($nombre == 'tipo' && strpos($value, '!') === false) {
                            $totalPreguntas++;
                        }
                        if (is_array($value)) {
                            if (!empty($value)) {
                                $opciones = '';
                                foreach ($value as $key => $v) {
                                    $opciones .= $key . ':' . $v . ';';
                                }
                                $checkcamb->$nombre = utf8_encode($opciones);
                                $checkcamb->save();
                            }
                        } else {
                            if($value === null) $value = '';

                            $checkcamb->$nombre = $value;
                            $checkcamb->save();
                        }
                    }
                }
            }
            $cambiocatalogo->totalPreguntas = $totalPreguntas;
            $cambiocatalogo->save();

            $dirdoc = $this->storage . '/cambiocatalogo/documentos/';
            $documentos = $this->recuperarAdjuntos($dirdoc, $alternador);
            foreach ($documentos as $tipo => $ficheros) {
                foreach ($ficheros as $fichero) {
                    Documento::create([
                        'cambiocatalogo_id' => $cambiocatalogo->id,
                        'titulo' => $fichero['titulo'],
                        'urlx' => $fichero['urlx'],
                        'tipo' => $tipo,
                    ]);
                }

            }
            $cambiocatalogo->documentos = $documentos;
            $cambiocatalogo->empleado;
            $cambiocatalogo->registros;

            DB::commit();

            return response($cambiocatalogo);
        } catch (\Exception $e) {
            DB::rollback();
            throw($e);
        }


    }

    public function show()
    {

    }

    public function update()
    {

    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $noconformidades = Noconformidades::where('aplicacion_id', $id)
                ->where('opl','cambiocatalogo')->get()->toArray();
            if(isset($noconformidades) && !empty($noconformidades)) {
                return response(['message' => 'No puede eliminarse el cambio de catalogo, tiene No Conformidades asociadas'], 422);
            }
            ChecklistCambiocatalogoRegistros::where('cambiocatalogo_id',$id)->delete();
            Documento::where('cambiocatalogo_id',$id)->delete();
            CambioCatalogo::where('id', $id)->delete();
            DB::commit();
            return response(['message' => 'ok']);
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    function recuperarAdjuntos($dirdoc, $alternador)
    {
        $lista = ["documentos" => [], "marcadores" => []];
        $files = scandir($dirdoc . $alternador);
        foreach ($files as $file) {
            if ($file != "." && $file != "..") {
                $file = strtoupper($file);
                if (substr($file, -4) === ".PDF" || substr($file, -4) === ".HTM" || substr($file, -4) === ".PNG") { //|| substr($file,-4)===".TXT"){
                    if (substr($file, -10) === ".MARCA.PDF") {
                        $lista["marcadores"][] = ["titulo" => substr($file, strlen(explode(".", $alternador)[0]) + 1, -10), "urlx" => "" . $file];
                    } else {
                        $lista["documentos"][] = ["titulo" => substr($file, strlen(explode(".", $alternador)[0]) + 1, -4), "urlx" => "" . $file];
                    }
                }
            }
        }
        return $lista;
    }

    public function conexionSAP(Request $request)
    {
        $emularSAP = true;
        $emularEXCEL = true;
        $alternador = $request->id;
        //comprobar que enviamos un catalogo valido
        $documentosLista = ["LISTA", "INSTRUCCIONES", "PCONJUNTO", "POFERTA", "PMARCAJES", "INDICE"];

        if (isset($alternador)) $argumento = $alternador;

        if ($argumento == "") {
            return response(["message" => "Debe introducir el numero de catalago" ], 422);
        }
        //Ej: 0124643.2020.01.22.12.13@LISTA
        $argumentos = explode("@", $argumento);

//comprobar que enviamos un catalogo y un indice valido
        if (isset($argumentos[0]) && isset($argumentos[1])) {
            $catalogo = $argumentos[0];
            $documento = $argumentos[1];
        } else {
            return response(["message" => "Parametro de indice no declarado"], 422);
        }

//identificamos el numero del alternador dentro del valor del catalogo
        $numero = explode(".", $catalogo)[0];
        if ($numero == "") {
            echo response(["message" => "Debe introducir el numero de catalago valido"], 422);
            exit(0);
        }


        if (!in_array($documento, $documentosLista)) {
            return response(["message" => "Debe introducir un tipo de documento [$documento] valido"], 422);
        }


//creamos la carpeta de almacenamiento de ese catalogo por si no existe
        if (!file_exists($this->storage.'/cambiocatalogo/documentos/' . $catalogo)) {
            @mkdir($this->storage.'/cambiocatalogo/documentos/' . $catalogo);
            if (!file_exists($this->storage.'/cambiocatalogo/documentos/' . $catalogo)) {
                return response(["message" => "Error al crear la carpeta /documentos/" . $catalogo], 422);
            }
        }

//iniciar la conexion con SAP
        $sap = new SAP($catalogo, '../../../../integraciones/cambiocatalogo/sap/', $this->storage.'/cambiocatalogo/documentos/', $this->storage.'/cambiocatalogo/marcadores/', $emularSAP);

        if ($documento == "LISTA") {
            //descargar el fichero de lista de materiales

            $resultado = $sap->downloadListaMateriales();
            if ($resultado == 99) {
                return response(["message" => "Error obteniendo la lista de materiales"], 422);
            } else {
                //descargar la hoja de instrucciones
                if (!$emularEXCEL) officeClass::msexcel_convert_plus($this->storage.'/cambiocatalogo/instrucciones/instrucciones.xlsm', $this->storage.'/cambiocatalogo/documentos/' . $catalogo . DIRECTORY_SEPARATOR . $numero . "_INSTRUCCIONES.PDF", ["I4" => substr($numero, 4)], "");
                //if(!$emularEXCEL) officeClass::msexcel_convert_array($dir["ipl"],$dir["doc"].$catalogo.DIRECTORY_SEPARATOR.$numero."_INSTRUCCIONES.PHP",["I4"=>substr($numero,4)],"");
                if ($emularEXCEL) @copy($this->storage.'/cambiocatalogo/documentos/' . "emulacion" . DIRECTORY_SEPARATOR . "INSTRUCCIONES.PDF", $this->storage.'/cambiocatalogo/documentos/' . $catalogo . DIRECTORY_SEPARATOR . $numero . "_INSTRUCCIONES.PDF");
                if ($emularEXCEL) @copy($this->storage.'/cambiocatalogo/documentos/' . "emulacion" . DIRECTORY_SEPARATOR . "INSTRUCCIONES.php", $this->storage.'/cambiocatalogo/documentos/' . $catalogo . DIRECTORY_SEPARATOR . $numero . "_INSTRUCCIONES.php");
            }
        }

        if ($documento == "INSTRUCCIONES") {
            //ahora se descarga junto a la lista de de piezas
        }

        if ($documento == "PCONJUNTO") {
            //decargar el plano de conjunto
            $resultado = $sap->downloadPlanoConjunto();
            if ($resultado == 99) {
                return response(["message" => "Error obteniendo el plano de conjunto"], 422);
            }
        }
        if ($documento == "POFERTA") {
            //descargar el plano de oferta
            $resultado = $sap->downloadPlanoOferta();
            if ($resultado == 99) {
                return response(["message" => "Error obteniendo la plano de oferta"], 422);
            }
        }
        if ($documento == "PMARCAJES") {
            //descargar el fichero(s) de  marcajes
            $resultado = $sap->downloadMarcajes();
            if ($resultado == 99) {
                return response(["message" => "error obteniendo los documentos de marcaje"], 422);
            }
            //generar y descargar el fichero de instrucciones
        }

        if ($documento == "INDICE") {
            //descargar el fichero(s) de  marcajes
            if (!isset($argumento[2])) {
                return response(["message" => "Debe introducir un indice"], 422);
            }
            $resultado = $sap->downloadIndice($argumento[2]);
            if ($resultado == 99) {
                return response(["message" => "Debe introducir un indice valido"], 422);
            }
        }

        return response(["message" => $documento . " recuperado correctamente!"], 200);

    }
}
