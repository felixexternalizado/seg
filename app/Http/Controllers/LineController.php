<?php

namespace App\Http\Controllers;

use App\CambioCatalogo;
use App\Cargoslineasempleados;
use App\Confirmacionprocesos;
use App\Empleados;
use App\LaboratorioRegistro;
use App\Line;
use App\Manodeobra;
use App\Maquina;
use App\Noconformidades;
use App\Registrocalidad;
use App\Zona;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class LineController extends AppController
{
    public function index(Request $request)
    {
        $array_lineas = [];

        if(isset($request->panel) && $request->panel == 'true') {
            $query = Line::query();

            $model = new Line();

            $query = $this->absoluteFilter($request,$model,$query);

            $result = $query->get();

            return response ($result);
        }

        $cargoslineas = Cargoslineasempleados::where('empleados_id',auth()->user()->empleados_id)->get('lineas_id')->toArray();
        $default_line = Empleados::where('id',auth()->user()->empleados_id)->first('default_linea_id');
        array_push($cargoslineas,$default_line->default_linea_id);
        $query = Line::whereIN('id', $cargoslineas);

        if (isset($request->search) && $request->search != null && $request->search != '') {
            $query = $query->where('resumen', 'like', '%' . $request->search . '%');
        }
        $lineas = $query->orderBy('resumen', 'ASC')->get();
        foreach ($lineas as $linea) {
            $linea->setNoConformidadesAttribute($linea->id);
            if(file_exists($this->storage.'/infografia/'.$linea->resumen.'.png')) {
                $linea->url_imagen = 'http://'.$_SERVER['HTTP_HOST'].'/_storage/infografia/'.$linea->resumen.'.png';
                $array_lineas[] = $linea;
            }
        }
        if(isset($request->appData) && $request->appData == 'infograma') return response($array_lineas);

        return response($lineas);
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $validator = Validator::make($request->all(), [
                'resumen' => 'required|string|max:255',
            ]);
            if ($validator->fails()) {
                return $validator->errors();
            }

            $linea_proceso = Line::create([
                'resumen' => $request->resumen,
                'coordenadas' => $request->coordenadas
            ]);
            DB::commit();
            return response($linea_proceso);

        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function show($id)
    {
        try {
            DB::beginTransaction();
            $line = Line::where('id', $id)->first();

            if (!$line) return response(['message' => 'No existe la linea solicitada'], 404);
            DB::commit();
            return $line;
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function update(Request $request, $id)
    {
        $line = Line::findOrFail($id);

        $line->update($request->all());

        return response($line);
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            if ($id <= 0 || $id == null) return response(['message' => 'No existe el recurso solicitado'], 404);
            $cargoslineasempl = Cargoslineasempleados::where('lineas_id', $id)->get();
            if(isset($cargoslineasempl) && !$cargoslineasempl->isEmpty()) {
                return response(['message' => 'No puede eliminarse la linea, esta asociada a varios cargos y empleados'], 422);
            }
            $confpr = Confirmacionprocesos::where('linea', $id)->get();
            if(isset($confpr) && !$confpr->isEmpty()) {
                return response(['message' => 'No puede eliminarse la linea, esta asociada a varias confirmaciones de proceso'], 422);
            }
            $regcal = Registrocalidad::where('linea_id', $id)->get();
            if(isset($regcal) && !$regcal->isEmpty()) {
                return response(['message' => 'No puede eliminarse la linea, esta asociada a varios registros de calidad'], 422);
            }
//            $manodeobra = Manodeobra::where('linea_id', $id)
//                ->where(DB::raw('YEAR(fecha)'),Carbon::now()->year)
//                ->where(DB::raw('MONTH(fecha)'),Carbon::now()->month)
//                ->get();
            $manodeobra = Manodeobra::where('linea_id', $id)->get();
            if(isset($manodeobra) && !$manodeobra->isEmpty()) {
                return response(['message' => 'No puede eliminarse la linea, tiene turnos asignados'], 422);
//                $semana_actual = Dias::where('dia', Carbon::now()->day)
//                    ->where('mes', Carbon::now()->month)
//                    ->where('anno', Carbon::now()->year)
//                    ->first('semana');
//                foreach ($manodeobra as $item) {
//                    $semana = Dias::where('dia', explode('-',$item->fecha)[2])
//                        ->where('mes', explode('-',$item->fecha)[1])
//                        ->where('anno', explode('-',$item->fecha)[0])
//                        ->first('semana');
//                    if($semana->semana == $semana_actual->semana) {
//                        return response(['message' => 'No puede eliminarse el empleado, tiene turnos asignados esta semana'], 422);
//                    }
//                }
            }
            $maquina = Maquina::where('linea_id', $id)->get();
            if(isset($maquina) && !$maquina->isEmpty()) {
                return response(['message' => 'No puede eliminarse la linea, tiene máquinas asignadas'], 422);
            }
            $zona = Zona::where('linea_id', $id)->get();
            if(isset($zona) && !$zona->isEmpty()) {
                return response(['message' => 'No puede eliminarse la linea, tiene zonas asignadas'], 422);
            }
            Line::where('id', $id)->delete();
            DB::commit();
            return response(['message' => 'ok']);
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function infoLineas(Request $request) {

        $output = [];
        foreach (Line::all() as $line) {
            $array = $this->infoLinea($request, $line, false);
            $array['linea'] = $line;
            $output[] = $array;
        }
        return response($output);

    }

    public function infoLinea(Request $request, $linea, $resp = true) {

        $turnos = [
            ['06:00:00', '13:59:59'],
            ['14:00:00', '21:59:59'],
            ['22:00:00', '05:59:59'],
            ['06:00:00', '17:59:59'],
            ['18:00:00', '05:59:59']
        ];
        $turno = 1;
        $fecha = date('Y-m-d');
        $fechaFiltro = [];

        if (isset($request->turno)) $turno = $request->turno - 1;
        if (isset($request->fecha)) $fecha = $request->fecha;

        $fechaFiltro = [$fecha . ' ' . $turnos[$turno][0], $fecha . ' ' . $turnos[$turno][1]];

        if ($resp) $l = Line::findOrFail($linea);
        else $l = $linea;

        function aplicarFiltros ($modelo, $fechaFiltro, $estado = 'abierto') {

            $modelo->whereBetween('created_at', $fechaFiltro);

            if ($estado === 'abierto') $modelo->where('estado', '!=' , 'cerrado');
            else if ($estado === 'cerrado') $modelo->where('estado', '=' , 'cerrado');

            return $modelo;

        }

        $cambios = aplicarFiltros($l->cambiocatalogos(), $fechaFiltro);
        $confirmaciones = aplicarFiltros($l->confirmacionprocesos(), $fechaFiltro);
        $registroscalidad = aplicarFiltros($l->registrocalidad(), $fechaFiltro);
        $laboratorioregistros = aplicarFiltros($l->laboratorioregistros(), $fechaFiltro);

        $output = [];
        $output['data'] = [
            [
                'app' => 'cambiocatalogo',
                'title' => 'Cambio de catalogo',
                'body' =>'<div class="text-center display-4">'
                . $cambios->count()
                . ' / '
                . $cambios->count()
                . '</div>',
            ],
            [
                'app' => 'confirmacionesproceso',
                'title' => 'Confirmaciones de proceso',
                'body' => '<p>Confirmaciones pendientes: '
                        .$confirmaciones->count()
                        .'</p><p>No conformidades: '
                        .$confirmaciones->with('noconformidades')->get()->pluck('noconformidades')->flatten()->count()
                        .'</p>',
            ],
            [
                'app' => 'registroscalidad',
                'title' => 'Registros de calidad',
                'body' => '<p>Registros de calidad pendientes: '
                        .$registroscalidad->count()
                        .'</p><p>No conformidades: '
                        .$registroscalidad->with('noconformidades')->get()->pluck('noconformidades')->flatten()->count()
                        .'</p><p>Registros de laboratorio: '
                        .$laboratorioregistros->where('estado', '!=' , 'cerrado')->count()
                        .'</p>',
            ]
        ];

        if(!isset($request->aplicacion) || empty($request->aplicacion)) {

            $output['zonas'] = $l->zonas;
            foreach ($output['zonas'] as $zona) {
                $zona->contenido =
                    '<p>Registros de calidad: '
                    . aplicarFiltros($zona->registrocalidad(), $fechaFiltro)->count()
                    . '</p><p>Registros de laboratorio: '
                    . aplicarFiltros($zona->laboratorioregistros(), $fechaFiltro)->count()
                    . '</p>';

            }

        }
        else if ($request->aplicacion === 'registroscalidad') {

            $output['zonas'] = $l->zonas;
            foreach ($output['zonas'] as $zona) {
                $zona->contenido =
                    '<div class="text-center h3 mb-0">'
                    . '<a class="d-block text-white text-decoration-none"'
                    . 'href="/registroscalidad?linea=' . $l->id . '&zona=' . $zona->id . '" target="_blank">'
                    . aplicarFiltros($l->registrocalidad(), $fechaFiltro, 'cerrado')->where('zona_id', $zona->id)->count()
                    . ' / '
                    . aplicarFiltros($l->registrocalidad(), $fechaFiltro, 'todos')->where('zona_id', $zona->id)->count()
                    . '</a>'
                    . '</div>';

            }

        }
        else {
        // TODO: cambiar
            $model = $this->aplicaciones[$request->aplicacion]['opl'];
            $z = $this->aplicaciones[$request->aplicacion]['zona'];
            $zonas = $l->zonas;

            foreach ($zonas as $zona) {
                $query = $model::where('estado', '!=' , 'cerrado');
                if($z) {
                    $appcount = $query->where('zona_id', $zona->id)->count();
                } else {
                    $appcount = 0;
                }

                $zona->contenido = '<p>'.$this->aplicaciones[$request->aplicacion]['txt'].': '. $appcount . '</p>';
                if($request->aplicacion == 'registroscalidad') {
                    $zona->contenido.= '<p>Registros de laboratorio: ' . LaboratorioRegistro::where('zona_id', $zona->id)->where('estado', '!=' , 'cerrado')->count() . '</p>';
                }
            }
            $output['zonas'] = $zonas;
        }

        if ($resp) return response($output);

        return $output;
    }
}
