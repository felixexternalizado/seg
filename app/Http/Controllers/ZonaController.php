<?php

namespace App\Http\Controllers;

use App\Zona;
use App\Registrocalidad;
use App\LaboratorioRegistro;
use App\Procesoscalidad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ZonaController extends AppController
{
    public function index(Request $request)
    {
        $query = Zona::query()->with('linea');

        $model = new Zona();

        $query = $this->absoluteFilter($request,$model,$query);

        if(isset($request->linea_id)) $query->where('linea_id',$request->linea_id);

        $result = $query->get();

        return response($result);
    }

    public function store(Request $request) {
        try {
            DB::beginTransaction();

            $this->validate($request, [
                'linea_id' => ['required', 'exists:lineas,id'],
                'resumen' => ['required', 'string', 'max:255']
            ]);

            $zona = Zona::create([
                'linea_id' => $request->linea_id,
                'resumen' => $request->resumen,
                'coordenadas' => $request->coordenadas,
            ]);
            DB::commit();
            return response($zona);

        } catch(\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function show($id)
    {
        $zona = Zona::where('id',$id)->first();

        if(!$zona) return response(['message' => 'No existe la linea solicitada'], 404);

        return $zona;
    }

    public function update(Request $request, $id) {
        try {
            DB::beginTransaction();

            $zona = Zona::where('id',$id)->first();
            if (!$zona) return response(['message' => 'No existe el recurso solicitado'], 404);

            $this->validate($request, [
                'resumen' => ['required'],
            ]);

            $zona->update($request->all());

            DB::commit();
            return response($zona);

        } catch(\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function destroy($id) {
        try {
            DB::beginTransaction();
            if($id <= 0 || $id == null) return response(['message' => 'No existe el recurso solicitado'], 404);

            $regcal = Registrocalidad::where('zona_id', $id)->get();
            if(isset($regcal) && !$regcal->isEmpty()) {
                return response(['message' => 'No puede eliminarse la zona, esta asociada a varios registros de calidad'], 422);
            }
            $lab = LaboratorioRegistro::where('zona_id', $id)->get();
            if(isset($lab) && !$lab->isEmpty()) {
                return response(['message' => 'No puede eliminarse la zona, esta asociada a varios registros de laboratorio'], 422);
            }
            $proceso = Procesoscalidad::where('zona_id', $id)->get();
            if(isset($proceso) && !$proceso->isEmpty()) {
                return response(['message' => 'No puede eliminarse la zona, esta asociada a varios procesos de calidad'], 422);
            }

            Zona::where('id',$id)->delete();
            DB::commit();
            return response(['message' => 'ok']);
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }
}
