<?php

namespace App\Http\Controllers;

use App\AplicacionUser;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $storage = '../../_storage';
    //protected $storage = '../../../_storage'; //ESTE ES EL STORAGE DE seg.llood.net

    protected function checkApp($apps) {
        $u = AplicacionUser::where('user_id', auth()->user()->id)->where('aplicacion_id',$apps)->get();
        if($u->isEmpty()) return ['message' => 'No tiene permisos para acceder a esta aplicación'];
    }
}
