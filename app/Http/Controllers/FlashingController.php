<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

class FlashingController extends Controller
{

    protected $arr_file = [];

    public function carpetas()
    {
        return response($this->carpetasPermitidas(), 200);
    }


    public function crearLinea(Request $request)
    {
        $path = $this->storage . '/flashing/' . $request->nombre_linea;

        if (is_dir($path)) return response(['message' => 'La linea que desea crear ya existe.'], 200);

        mkdir($path, 777, true);

        return response(['message' => 'La linea ha sido creada'], 200);
    }

    public function eliminarLinea(Request $request)
    {
        $path = $this->storage . '/flashing/' . $request->nombre_linea;

        $this->arr_file = [];

        if (!is_dir($path)) return response(['message' => 'La ruta no es correcta'], 422);

        $files = $this->recorrerCarpetas($path);
        if (isset($files['code']) && $files['code'] != 200) return response(['message' => $files['message']], $files['code']);

        if (is_array($files) && !empty($files) && !isset($files['message'])) {
            return response(['message' => 'La linea solo puede eliminarse si no tiene secuencias.'], 422);
        }

        $this->eliminarCarpetas($path);

        return response(['message' => 'La linea ha sido eliminada'], 200);
    }

    public function catalogos($carpeta)
    {
        $path = $this->storage . '/flashing/' . $carpeta;

        if (!is_dir($path)) return response(['message' => 'La ruta es incorrecta.'], 422);

        $aux = scandir($path);

        $arr_piezas = [];
        foreach ($aux as $pieza) {
            if ($pieza == '.' || $pieza == '..') continue;
            $arr_piezas[] = $pieza;
        }
        return response($arr_piezas, 200);
    }

    public function entornos($carpeta, $pieza)
    {

        if (!in_array($carpeta, $this->carpetasPermitidas())) return response(['message' => 'La carpeta que esta deseando visualizar no existe.'], 404);

        $path = $this->storage . '/flashing/' . $carpeta . '/' . $pieza;

        if (!is_dir($path)) return response(['message' => 'La ruta no es correcta'], 422);

        $aux = scandir($path);

        $arr_entornos = [];
        foreach ($aux as $entorno) {

            if ($entorno == '.' || $entorno == '..') continue;

            foreach (scandir($path . '/' . $entorno) as $version) {

                if ($version == '.' || $version == '..') continue;

                if ($entorno == 'activa') $arr_entornos[$entorno] = $version;
                else $arr_entornos[$entorno][] = $version;

            }
        }

        return response($arr_entornos, 200);

    }

    public function archivos($linea, $catalogo, $entorno, $secuencia)
    {
        if (!in_array($linea, $this->carpetasPermitidas())) return response(['message' => 'La carpeta que esta deseando visualizar no existe.'], 404);

        $path = $this->storage . '/flashing/' . $linea . '/' . $catalogo . '/' . $entorno . '/' . $secuencia;

        if (!is_dir($path)) return response(['message' => 'La ruta no es correcta'], 422);

        $aux = scandir($path);

        $arrficheros = [];

        foreach ($aux as $file) {
            if ($file == '.' || $file == '..') continue;
            $arrficheros[] = $file;
        }
        return response($arrficheros, 200);
    }

    public function contenidoArchivo($linea, $catalogo, $entorno, $secuencia, $archivo)
    {
        if (!in_array($linea, $this->carpetasPermitidas())) return response(['message' => 'La carpeta que esta deseando visualizar no existe.'], 404);

        $path = $this->storage . '/flashing/' . $linea . '/' . $catalogo . '/' . $entorno . '/' . $secuencia . '/' . $archivo;

        if (!is_file($path)) return response(['message' => 'La ruta no es correcta'], 422);

        return response(file_get_contents($path), 200);
    }

    public function guardarArchivo(Request $request, $linea, $catalogo, $entorno, $secuencia, $archivo)
    {
        if (!in_array($linea, $this->carpetasPermitidas())) return response(['message' => 'La carpeta que esta deseando visualizar no existe.'], 404);

        $path = $this->storage . '/flashing/' . $linea . '/' . $catalogo . '/' . $entorno . '/' . $secuencia . '/' . $archivo;

        if (!is_file($path)) return response(['message' => 'La ruta no es correcta'], 422);

        file_put_contents($path, $request->data);

        return response(['message' => 'El archivo ha sido guardado correctamente'], 200);
    }

    public function nuevoCatalogo(Request $request)
    {

        if (!in_array($request->carpeta, $this->carpetasPermitidas())) return response(['message' => 'La carpeta en la que se desea crear el nuevo part number no existe.'], 404);

        if (is_dir($this->storage . '/flashing/' . $request->carpeta . '/' . $request->nombre_pieza)) return response(['message' => 'El part number ya existe'], 422);

        $carpeta = $request->carpeta;

        if ($request->nombre_pieza === '' || $request->nombre_pieza === null) return response(['message' => 'Debe introducir un nombre apropiado para el nuevo part number'], 422);

        $pieza = $request->nombre_pieza;

        mkdir($this->storage . '/flashing/' . $carpeta . '/' . $pieza, 777, true);
        mkdir($this->storage . '/flashing/' . $carpeta . '/' . $pieza . '/develop', 777, true);
        mkdir($this->storage . '/flashing/' . $carpeta . '/' . $pieza . '/prod', 777, true);
        mkdir($this->storage . '/flashing/' . $carpeta . '/' . $pieza . '/active', 777, true);

        return response(['message' => 'El part number ha sido creado con éxito.'], 200);
    }

    public function copiarCatalogo(Request $request, $orig)
    {
        $respuesta = $this->copiarPartNumber($request, $orig);

        if (is_array($respuesta) && $respuesta['code'] != 200) return response(['message' => $respuesta['message']], $respuesta['code']);

        return response(['message' => 'El part number ha sido movido con éxito.'], 200);
    }

    public function renombrarCatalogo(Request $request, $orig)
    {
        $respuesta = $this->copiarPartNumber($request, $orig);

        if (is_array($respuesta) && $respuesta['code'] != 200) return response(['message' => $respuesta['message']], $respuesta['code']);

        $pathoriginal = $this->storage . '/flashing/' . $request->carpeta . '/' . $orig;

        $this->eliminarCarpetas($pathoriginal);

        return response(['message' => 'El part number ha sido renombrado con éxito.'], 200);
    }

    public function eliminarCatalogo(Request $request)
    {
        $pathoriginal = $this->storage . '/flashing/' . $request->carpeta . '/' . $request->catalogo;

        $this->eliminarCarpetas($pathoriginal);

        return response(['message' => 'El part number ha sido eliminado con éxito.'], 200);
    }

    public function nuevaVersion(Request $request)
    {
        if (!in_array($request->carpeta, $this->carpetasPermitidas())) return response(['message' => 'La carpeta que esta deseando visualizar no existe.'], 404);

        $path = $this->crearCarpetaVersion($request);

        if (is_array($path)) return response($path, 422);

        foreach ($ficheros = $request->file('ficheros') as $fichero) {
            if (!$fichero->isValid()) return response(['message' => 'El archivo no es valido'], 422);

            $fichero->move($path, $fichero->getClientOriginalName());
        }

        return response(['message' => 'La versión ha sido creada con éxito.'], 200);
    }

    public function cambiarEntorno(Request $request)
    {
        if (!in_array($request->carpeta, $this->carpetasPermitidas())) return response(['message' => 'La linea que esta deseando visualizar no existe.'], 404);

        $path = $this->storage . '/flashing/' . $request->carpeta . '/' . $request->pieza . '/' . $request->entorno . '/' . $request->version;

        $activapath = $this->storage . '/flashing/' . $request->carpeta . '/' . $request->pieza . '/activa/' . $request->version;

        if (!is_dir($path)) return response(['message' => 'La carpeta que esta intentando mover no existe.'], 404);

        if ($request->entorno === 'dev') $entorno = 'prod';

        if ($request->entorno === 'prod') $entorno = 'dev';

        if ($request->entorno === 'prod' && ($request->activar)) $entorno = 'activa';

        if (($request->activar) && $request->entorno === 'dev') return response(['message' => 'Esta intentando mover una carpeta de desarrollo a activa'], 422);

        $newpath = $this->storage . '/flashing/' . $request->carpeta . '/' . $request->pieza . '/' . $entorno . '/' . $request->version;

        if ($request->activar) {
            foreach (scandir($this->storage . '/flashing/' . $request->carpeta . '/' . $request->pieza . '/' . $entorno) as $item) {
                if ($item == '.' || $item == '..') continue;
                $this->eliminarCarpetas($this->storage . '/flashing/' . $request->carpeta . '/' . $request->pieza . '/' . $entorno . '/' . $item);
            }
        }

        mkdir($newpath, 777, true);

        $this->copiarCarpeta($path, $newpath);

        if (isset($request->activar)) return response(['message' => 'La secuencia flash ' . $request->version . ' ha sido activada.'], 200);

        return response(['message' => 'La versión ha sido movida con éxito.'], 200);
    }

    public function desactivar(Request $request)
    {
        if (!in_array($request->linea, $this->carpetasPermitidas())) return response(['message' => 'La linea que esta deseando visualizar no existe.'], 404);

        $path = $this->storage . '/flashing/' . $request->linea . '/' . $request->catalogo . '/activa/';

        foreach (scandir($path) as $secuencia) {
            if ($secuencia == '.' || $secuencia == '..') continue;
            $this->eliminarCarpetas($path . '/' . $secuencia);
        }

        return response(['message' => 'La versión ha sido desactivada con éxito.'], 200);
    }

    public function duplicarVersion(Request $request, $orig)
    {
        if (!in_array($request->carpeta, $this->carpetasPermitidas())) return response(['message' => 'La carpeta que esta deseando visualizar no existe.'], 404);

        $path = $this->storage . '/flashing/' . $request->carpeta . '/' . $request->pieza . '/' . $request->entorno . '/' . $orig;

        if (!is_dir($path)) return response(['message' => 'La carpeta que esta duplicando no existe.'], 404);

        $newpath = $this->crearCarpetaVersion($request, $request->nombre);

        if (is_array($newpath)) return response($newpath, 422);

        $this->copiarCarpeta($path, $newpath);

        return response(['message' => 'La versión ha sido duplicada con éxito.'], 200);
    }

    public function eliminarVersion(Request $request, $version)
    {

        if (!in_array($request->carpeta, $this->carpetasPermitidas())) return response(['message' => 'La carpeta que esta deseando visualizar no existe.'], 404);

        $carpeta = $request->carpeta;
        $pieza = $request->pieza;
        $entorno = $request->entorno;

        $path = $this->storage . '/flashing/' . $carpeta . '/' . $pieza . '/' . $entorno . '/' . $version;

        $activapath = $this->storage . '/flashing/' . $carpeta . '/' . $pieza . '/activa/' . $version;

        if (!is_dir($path)) return response(['message' => 'La carpeta que desea eliminar no es un directorio o no existe'], 404);

        if (is_dir($activapath)) return response(['message' => 'La secuencia flash que desea eliminar esta activa, desactivela para poder eliminarla.'], 422);

        $this->eliminarCarpetas($path);

        return response(['message' => 'La carpeta ' . $version . ' y los archivos que contenia se han borrado'], 200);

    }

    private function carpetasPermitidas()
    {
        $path = $this->storage . '/flashing';

        $aux = scandir($path);

        $arr_lineas = [];
        foreach ($aux as $linea) {
            if ($linea == '.' || $linea == '..') continue;
            $arr_lineas[] = $linea;
        }

        return $arr_lineas;
    }

    private function crearCarpetaVersion($request, $version = null)
    {

        $carpeta = $request->carpeta;
        $pieza = $request->pieza;
        $entorno = $request->entorno;
        $version = $version === null ? $request->version : $version;

        $path = $this->storage . '/flashing/' . $carpeta . '/' . $pieza . '/' . $entorno . '/' . $version;

        if (is_dir($path)) return ['message' => 'La carpeta que se quiere crear ya existe'];

        mkdir($path, 777, true);

        return $path;
    }

    private function copiarCarpeta($path, $newpath)
    {
        foreach (scandir($path) as $item) {

            if ($item == '.' || $item == '..') continue;

            copy($path . '/' . $item, $newpath . '/' . $item);
        }
    }

    private function copiarPartNumber(Request $request, $original)
    {
        if (!in_array($request->carpeta, $this->carpetasPermitidas())) return ['message' => 'La carpeta en la que se desea crear el nuevo catálogo no existe.', 'code' => 404];

        $carpeta = $request->carpeta;

        if ($request->nombre_catalogo === '' || $request->nombre_catalogo === null) return ['message' => 'Debe introducir un nombre apropiado para el nuevo catálogo', 'code' => 422];

        $destino = $request->nombre_catalogo;

        if (!is_dir($this->storage . '/flashing/' . $carpeta . '/' . $destino)) {
            mkdir($this->storage . '/flashing/' . $carpeta . '/' . $destino, 777, true);
        } else {
            return ['message' => 'El catálogo ya existe', 'code' => 422];
        }

        $pathoriginal = $this->storage . '/flashing/' . $carpeta . '/' . $original;

        foreach (scandir($pathoriginal) as $entorno) {
            if ($entorno == '.' || $entorno == '..') continue;

            mkdir($this->storage . '/flashing/' . $carpeta . '/' . $destino . '/' . $entorno, 777, true);

            $path_old_entorno = $this->storage . '/flashing/' . $carpeta . '/' . $original . '/' . $entorno;

            $path_nuevo_entorno = $this->storage . '/flashing/' . $carpeta . '/' . $destino . '/' . $entorno;

            foreach (scandir($path_old_entorno) as $secuencia) {
                if ($secuencia == '.' || $secuencia == '..') continue;
                mkdir($path_nuevo_entorno . '/' . $secuencia, 777, true);

                $path_old_secuencia = $path_old_entorno . '/' . $secuencia;

                $path_nueva_secuencia = $path_nuevo_entorno . '/' . $secuencia;

                $this->copiarCarpeta($path_old_secuencia, $path_nueva_secuencia);

            }
        }

        return ['message' => 'Success', 'code' => 200];
    }

    private function recorrerCarpetas($path)
    {
        if (!is_dir($path)) return ['message' => 'La ruta no es correcta', 'code' => 422];

        foreach (scandir($path) as $item) {

            if (count(scandir($path)) == 2) return ['message' => 'OK', 'code' => 200];

            if ($item == '.' || $item == '..') continue;

            if (is_dir($path . '/' . $item)) $this->recorrerCarpetas($path . '/' . $item);

            if (is_file($path . '/' . $item)) {
                $this->arr_file[] = $path . '/' . $item;
            }
        }
        return $this->arr_file;
    }

    private function eliminarCarpetas($path)
    {
        if (!is_dir($path)) return ['message' => 'La ruta no es correcta', 'code' => 422];

        foreach (scandir($path) as $item) {

            if ($item == '.' || $item == '..') continue;

            if (is_dir($path . '/' . $item)) $this->eliminarCarpetas($path . '/' . $item);

            if (is_file($path . '/' . $item)) {
                unlink($path . '/' . $item);
            } else {
                rmdir($path . '/' . $item);
            }
        }

        rmdir($path);
    }
}
