<?php

namespace App\Http\Controllers;

use App\Aplicacion;
use App\AplicacionUser;
use App\Banco;
use App\BancoPaso;
use App\BancoPrueba;
use App\BancoPruebaDescripcion;
use App\Categoria;
use App\Confirmacionprocesos;
use App\Registrocalidad;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PHPExcel_IOFactory;

class AplicacionesController extends Controller
{

    //<editor-fold desc="CRUD">
    public function index()
    {
        return response(Aplicacion::with('categoria')->orderBy('categoria_id', 'ASC')->orderBy('orden', 'ASC')->get());
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'text' => ['required', 'string', 'max:255'],
            'link' => ['required', 'string'],
            'categoria_id' => ['exists:categorias,id'],
            'orden' => ['required', 'numeric'],
        ]);

        $aplicacion = Aplicacion::create([
            'text' => $request->text,
            'link' => $request->link,
            'categoria_id' => $request->categoria_id,
            'orden' => $request->orden,
        ]);


        return response($aplicacion);
    }

    public function show($id)
    {
        return response(Aplicacion::findOrFail($id)->with('categoria'));
    }

    public function update(Request $request, $id)
    {
        $aplicacion = Aplicacion::findOrFail($id);

        $aplicacion->update($request->all());

        return response($aplicacion);
    }

    public function destroy($id)
    {

        $appuser = AplicacionUser::where('aplicacion_id', $id)
            ->get();
        if (isset($appuser) && !$appuser->isEmpty()) {
            return response(['message' => 'No puede eliminarse la aplicación, tiene Usuarios asociados'], 422);
        }

        $aplicacion = Aplicacion::findOrFail($id);
        $aplicacion->delete();
    }

    public function sendApps(Request $request)
    {

        $array['apps'] = [];

        //SI ENVIA USUARIO, ENVIAR TODAS PERO CON TRUE FALSE
        if (isset($request->user_id)) $aux = $this->cat(Categoria::get(), $request->user_id);
        else $aux = $this->cat(Categoria::get());


        return response($aux, 200);
    }

    public function cat($categorias, $user_id = null)
    {

        $arr_appuser = [];
        $aux = [];
        foreach ($categorias as $categoria) {
            $buttons = [];
            if (isset($user_id) && $user_id != '' && $user_id != null) {
                $appuser = AplicacionUser::where('user_id', $user_id)->get('aplicacion_id')->toArray();
                foreach ($appuser as $key => $value) {
                    $arr_appuser[] = $value['aplicacion_id'];
                }
            }
            $aplications = Aplicacion::where('categoria_id', $categoria->id)->orderBy('orden', 'ASC')->get();

            foreach ($aplications as $aplication) {
                if (isset($user_id) && $user_id != '' && $user_id != null) {
                    if (in_array($aplication->id, $arr_appuser) && !empty($arr_appuser)) {
                        $buttons[] = [
                            'id' => $aplication->id,
                            'text' => $aplication->text,
                            'link' => $aplication->link,
                            'permiso' => true,
                        ];
                    } else {
                        $buttons[] = [
                            'id' => $aplication->id,
                            'text' => $aplication->text,
                            'link' => $aplication->link,
                            'permiso' => false,
                        ];
                    }
                } else {
                    $buttons[] = [
                        'id' => $aplication->id,
                        'text' => $aplication->text,
                        'link' => $aplication->link,
                    ];
                }
            }
            $aux[] = [
                'title' => $categoria->title,
                'icono' => $categoria->icon,
                'color' => $categoria->color,
                'buttons' => $buttons
            ];
        }
        return $aux;
    }
    //</editor-fold>

}
