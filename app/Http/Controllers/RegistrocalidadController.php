<?php

namespace App\Http\Controllers;

use App\Empleados;
use App\ChecklistRegistrosdecalidadRegistros;
use App\Maquina;
use App\Noconformidades;
use App\Procesoscalidad;
use App\Registrocalidad;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RegistrocalidadController extends AppController
{
    public function index(Request $request)
    {
        try {
            DB::beginTransaction();

            $query = Registrocalidad::query();
            $result = array();
            if (isset($request->maquina_referencia) && $request->maquina_referencia != null
                && isset($request->maquina_id) && $request->maquina_id != null) {
                $maquina = Maquina::select('id', 'referencia')->where('referencia', $request->maquina_referencia)->first()->toArray();
                $query->where('maquina_id', $maquina['id']);
                $query->where('maquina_id', $request->maquina_id);
            } elseif (isset($request->maquina_referencia) && $request->maquina_referencia != null) {
                $maquina = Maquina::select('id', 'referencia')->where('referencia', $request->maquina_referencia)->first()->toArray();
                $query->where('maquina_id', $maquina['id']);
            } elseif (isset($request->maquina_id) && $request->maquina_id != null) {
                $maquina = Maquina::where('id', $request->maquina_id)->first('referencia')->toArray();
                $query->where('maquina_id', $request->maquina_id);
            }

            if(isset($request->zona_id) && $request->zona_id != null) $query->where('zona_id', $request->zona_id);

            if(isset($request->linea_id) && $request->linea_id != null) $query->where('linea_id', $request->linea_id);

            if (!isset($request->cerrado) || empty($request->cerrado) || $request->cerrado == 'false') $query->where('estado', '!=', 'cerrado');

            $registro = $query->select('id', 'linea_id', 'zona_id', 'maquina_id', 'procesocalidad_id', 'empleados_id', 'resumen', 'valor', 'frecuencia', 'evaluador', 'fechaInicio', 'fechaFinal', 'estado', 'created_at', 'updated_at')
                ->with('linea', 'zona')->get();

            if($registro->isEmpty()) return response($registro);
            foreach ($registro as $r) {
                $c = Carbon::createFromFormat('Y-m-d H:i:s', $r->created_at)->format('H:i:s');
                $proceso = Procesoscalidad::where('id', $r->procesocalidad_id)->first('resumen');
                if(isset($maquina['referencia'])) {
                    $referencia = $maquina['referencia'];
                } elseif(!isset($maquina['referencia']) && isset($r->maquina_id)) {
                    $referencia = Maquina::where('id', $r->maquina_id)->first()->referencia;
                } else {
                    $referencia = '';
                }

                $r->hora = $c;
                $r->referencia_maquina = $referencia;
                $r->nombre_proceso_calidad = $proceso->resumen.' '.$r->valor;
                $r->nombre_proceso = $proceso->resumen;
            }

            DB::commit();
            return response ($registro);
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            
			if(!isset($request->auto)) {
                if (isset($request->maquina)) {
                    $this->validate($request, [
                        'maquina' => ['exists:maquinas,id'],
                    ]);
                }

                $this->validate($request, [
                    'tipo' => ['required'],
                    'linea' => ['required', 'exists:lineas,id'],
                    'zona' => ['required', 'exists:zonasdelinea,id']
                ]);
			}
			
            $array_reg = array();
            foreach (Procesoscalidad::where('linea_id', $request->linea)->where('zona_id', $request->zona)->where('activo',1)->get() as $procesocalidad) {

                $evaluador = auth()->user()->empleados_id;

//                $array = [
//                    'proceso' => $procesocalidad,
//                    'empleado' => $evaluador,
//                    'linea' => $request->linea,
//                    'zona' => $request->zona,
//                    'valor' => $request->valor,
//                    'tipo' => $request->tipo,
//                    'storage' => $this->storage.'/registros_calidad',
//                    'app' => 'registrocalidad'
//                ];

                $json = (new ExcelCalidadController())->getJson($procesocalidad, $evaluador, $request);

                if(isset($json['message'])) {
                	if(!isset($request->auto)) return response(['message' => $json['message']], 404);
                	else continue;
                }

                if(isset($json['entidad']['registros']) && empty($json['entidad']['registros'])) continue;

                $date = Carbon::now();
                $ev = '';
                foreach ($json['entidad']['registros'] as $registro) {
                    if($registro['evaluador'] == '') continue;
                    $ev.= $registro['evaluador'];
                }
                $eval = substr(implode(',',array_unique(explode(',',$ev))),0,-1);

				$array_aux = [];
                foreach ($json['entidad']['registros'] as $registro) {
                    if($registro['fase'] !== 'FINICIO' && $registro['fase'] !== 'FFIN') {
                        $array_aux[] = $registro;
                    }
                }
                if(empty($array_aux)) {
                	if(!isset($request->auto)) return response(['message' => 'El registro de calidad no puede crearse si la plantilla solo tiene fases de Inicio y Fin'], 422);
                	else continue;
                }
                //EVALUADOR ES EL QUE LA CREA, EL EMPLEADO_ID ES EL RESPONSABLE
                $registrocalidad = Registrocalidad::create([
                    'linea_id' => $request->linea,
                    'zona_id' => $request->zona,
                    'maquina_id' => $request->maquina,
                    'procesocalidad_id' => $procesocalidad->id,
                    'empleados_id' => $eval,
                    'resumen' => $procesocalidad->resumen,
                    'frecuencia' => $request->tipo,
                    'valor' => isset($request->valor) && $request->valor != '' ? $request->valor : $this->translateWeek($date->format('l')),
                    'evaluador' => $evaluador,
                    'fechaInicio' => $date,
                    'fechaFinal' => $json['entidad']['registros'][0]['fechafin'], //TODO TEMPORAL
                    'totalMal' => 0,
                    'totalBien' => 0,
                    'porcentajeBien' => 0,
                    'totalPreguntas' => 0,
                ]);

                $totalPreguntas = 0;
                $indice = 0;
                foreach ($json['entidad']['registros'] as $registro) {
                    if (isset($registro)) {
                        unset($registro['fechafin']);

                        if($indice == 0){
                            $indice = $registro['indice'];
                        } else {
                            $indice++;
                        }

                        $registro['indice'] = $indice;

                        $checkcalidad = new ChecklistRegistrosdecalidadRegistros();
                        $checkcalidad->registrocalidad_id = $registrocalidad->id;
                        foreach ($registro as $key => $value) {
                            $nombre = $key;
                            if ($nombre == 'tipo' && strpos($value, '!') === false) {
                                $totalPreguntas++;
                            }
                            if (is_array($value)) {
                                if (!empty($value)) {
                                    $opciones = '';
                                    foreach ($value as $key => $v) {
                                        $opciones .= $key . ':' . $v . ';';
                                    }
                                    $checkcalidad->$nombre = $opciones;
                                    $checkcalidad->save();
                                }
                            } else {
                                if($nombre == 'evaluador') $value = substr($value,0,-1);
                                $checkcalidad->$nombre = $value;
                                $checkcalidad->save();

                            }
                        }
                    }
                }
                $registrocalidad->totalPreguntas = $totalPreguntas;
                $registrocalidad->save();

                $reg_calidad = Registrocalidad::where('id', $registrocalidad->id)->first();

                $empleado = Empleados::where('id', $evaluador)->first();
                $reg_calidad->iniciales = $empleado->iniciales;
                $reg_calidad->empleado = $empleado;

                $array_reg[] = $reg_calidad;
            }

            if(isset($array_reg) && empty($array_reg) && !isset($request->auto)) return response(['message' => 'No se han encontrado registros que coincidan con los datos introducidos.'], 404);

            DB::commit();
            return response($array_reg);
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function show($id)
    {
        try {
            DB::beginTransaction();
            $reg_calidad = Registrocalidad::find($id);

            if (!$reg_calidad) return response(['message' => 'No existe la confirmación de proceso solicitada'], 500);

            DB::commit();
            return $reg_calidad;
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $registrocalidad = Registrocalidad::where('id', $id)->first();

            $data = json_decode($request->json, true);

            if (isset($data['entidad']) && isset($data['entidad']['id']) && isset($data['entidad']['registros']) && $data['entidad']['fases']) {
                if (isset($data["entidad"]["finalizacion"])) {
                    $registrocalidad->estado = 'cerrado';
                    $registrocalidad->save();
                }
                $registrocalidad->update($request->all());

                DB::commit();
                return response($registrocalidad);
            } else {
                DB::commit();
                return response(['message' => 'La confirmación del proceso está incompleta'], 422);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $reg = Registrocalidad::where()->first();

            $noconformidades = Noconformidades::where('aplicacion_id', $id)
                ->where('opl','registroscalidad')->get();
            if(isset($reg) && $reg->estado == 'cerrado') {
                if(isset($noconformidades) && !$noconformidades->isEmpty()) {
                    $noconformidades->delete();
                    ChecklistRegistrosdecalidadRegistros::where('registrocalidad_id',$id)->delete();
                }
                $reg->delete();
                DB::commit();
                return response(['message' => 'El registro ha sido eliminado']);
            }

            $noconformidades = Noconformidades::where('aplicacion_id', $id)
                ->where('opl','registroscalidad')->get()->toArray();

            if(isset($noconformidades) && !empty($noconformidades)) {
                return response(['message' => 'No puede eliminarse el registro de calidad, tiene No Conformidades asociadas'], 422);
            }
            ChecklistRegistrosdecalidadRegistros::where('registrocalidad_id',$id)->delete();
            Registrocalidad::where('id', $id)->delete();
            DB::commit();
            return response(['message' => 'ok']);
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }
}
