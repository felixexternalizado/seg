<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use PHPExcel;
use PHPExcel_IOFactory;

class ExcelController extends AppController
{
    public $objPHPExcel;

    public function getJson(\App\Procesos $fichero,$empleado, $linea, $storage){

        $carpeta = explode('.',$fichero->resumen)[0];
        $file = strtolower(str_replace(' ', '_', explode('.',$fichero->resumen)[1]) . '.xlsx');

        $dir = '';

        for ($x = 2; $x < count(scandir($storage.'/confirmacion_procesos')); $x++) {
            $scandir = scandir($storage.'/confirmacion_procesos');
            $dir = $storage . '/confirmacion_procesos/'.$scandir[$x].'/'.$carpeta.'/'.$file;
        }
        if($dir == '' && file_exists($dir)) {
            return (['message' => 'Error al encontrar la plantilla del proceso.']);
        }

        $this->getExcel($dir);

        $fichero->resumen = explode('.',$fichero->resumen)[1];

        $empleado = \App\Empleados::where('id', $empleado)->first();
        $linea = \App\Line::where('id', $linea)->first();

        $fases = array();
        $datos = array();
        $date = Carbon::now();
        $date = $date->format('Ymdhis');
        $argumento = str_replace(' ','',$fichero->resumen).'.'.$date;
        $materiales = '';

        for($i=12;$i<500;$i++){
            $result = $this->getRow($i, $argumento, $empleado, $linea, $materiales);

            if ($result) { $datos[] = $result; }

            $fase = $result['fase'];
            if(isset($fases[$fase])){
                $fases[$fase]++;
            }else{
                $fases[$fase]=1;
            }
        }
        $fases = array_keys($fases);
        return ["entidad"=>["id"=>	$fichero->resumen,"fases"=>$fases,"registros"=>$datos]];
    }

    public function getExcel($fichero){
        $inputFileType = PHPExcel_IOFactory::identify($fichero);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $this->objPHPExcel = $objReader->load($fichero); // Empty Sheet
        $this->objPHPExcel->setActiveSheetIndex(0);
    }

    public function getRow($i, $argumento, $empleado, $linea, $materiales=[]){
        $fase = $this->getCelda('B'.$i);
        if(strlen($fase)>0){
            $descripcion = mb_substr($this->getCelda('C'.$i), 0, 254);
            $operacion = mb_substr($this->getCelda('D'.$i), 0, 254);
            $instrucciones = str_replace(chr(10),'<br>',mb_substr($this->getCelda('E'.$i), 0, 254));
            $instrucciones = str_replace('\n','<br>',$instrucciones);
            $condicional = $this->condicionalExcel($this->getCelda('G'.$i));
            $noconformidad = strval($this->getCelda('H'.$i));
            $limite1 = $this->getCelda('I'.$i);
            $limite2 = $this->getCelda('J'.$i);
            $fotoInstrucciones = $this->getCelda('K'.$i) == '' ? $this->getCelda('K'.$i) : $_SERVER['HTTP_HOST'].'/_storage/fotos_plantillas/'.$this->getCelda('K'.$i);
            $comentario = '';

//            $variable = $this->getCelda('I'.$i);
            if($this->getCelda('F'.$i)!=""){ //respuesta
                $respuesta = $this->getCelda('F'.$i);

            }else{
                $respuesta = "ETIQUETA";
            }
            $valor="";

            switch(strtoupper($respuesta)){
                case "ID":								//OK
                    $tipo ="!"; //checkeado
                    $validador="";
                    $opciones ="";
                    $requerido = true;
                    $valor=$argumento;
                    break;
                case "AHORA":								//OK
                    $tipo ="!A"; //checkeado
                    $validador="";
                    $opciones ="";
                    $requerido = true;
                    $valor = Carbon::now()->format('Y-m-d H:i:s');
                    break;
                case "HORA":								//OK
                    $tipo ="HORA"; //checkeado
                    $validador="";
                    $opciones ="";
                    $requerido = true;
                    $valor = Carbon::now()->format('H:i:s');
                    break;
                case "LINEA":								//OK
                    $tipo ="!L"; //checkeado
                    $validador="";
                    $opciones ="";
                    $requerido = true;
                    $valor=$linea->resumen;
                    break;
                case "USER":								//OK
                    $tipo ="!U"; //checkeado
                    $validador="";
                    $opciones ="";
                    $requerido = true;
                    $valor=$empleado->nombre;
                    break;
                case "OK":								//OK
                    $tipo ="O"; //checkeado
                    $validador="OK";
                    $opciones =[];
                    $requerido = true;
                    break;
                case "SI":								//SI
                    $tipo="B"; //booleano
                    $validador="SI";
                    $opciones =[];
                    $requerido = true;
                    break;
                case "TEXTO":							//TEXTO
                    $tipo = "T"; //texto
                    $validador="";
                    $opciones =[];
                    $requerido = false;
                    break;
                case "TEXTO OBLIGATORIO":				//NUMERO OBLIGATORIO
                    $tipo = "N"; //numero
                    $validador="";
                    $opciones =[];
                    $requerido = true;
                    break;
                case "NUMERO":							//NUMERO
                    $tipo = "N"; //numero
                    $validador="";
                    $opciones =[];
                    $requerido = false;
                    break;
                case "NUMERO OBLIGATORIO":				//NUMERO OBLIGATORIO
                    $tipo = "N"; //numero
                    $validador="";
                    $opciones =[];
                    $requerido = true;
                    break;
                case "FOTO":
                    $tipo = "F"; //foto
                    $validador="";
                    $opciones =[];
                    $requerido = true;
                    break;
                case "IMAGEN":
                    $tipo = "I"; //imagen
                    $validador="";
                    $opciones =[];
                    $requerido = true;
                    break;
                case "ETIQUETA":
                    $tipo = "E"; //etiqueta
                    $validador="";
                    $opciones =[];
                    $requerido = false;
                    break;
                case "VALORACION5":
                    $tipo = "S"; //etiqueta
                    $validador="";
                    $opciones =[];
                    $requerido = false;
                    break;
                case "QR":
                    $tipo = "QR"; //qr
                    $validador="";
                    $opciones =[];
                    $requerido = false;
                    break;
                default:
                    if (preg_match('/BARCODE:/',$respuesta) >= 1){
                        $tipo = "barcode"; //barcode
                        $validador="";
                        $opciones =$this->getBarcode($respuesta);
                        $requerido = false;
                    }
                    elseif(substr($respuesta,0,1)=="@"){	//*cargar...
                        $tipo ="A"; //ACCION
                        $validador = "";
                        $comando = explode(":",trim(substr($respuesta,1)));
                        @$opciones=["titulo"=>$comando[0],"accion"=>$comando[1]];
                        $requerido = false;

                    }elseif(substr($respuesta,0,1)=="*"){	//*060.0  // *A3

                        $opciones[]="*";
                        $tipo ="H"; //LISTA
                        $validador = "UNICO";
                        $opciones=[];
                        if(isnumeric(substr($respuesta,1,1))){	//*060.0
                            foreach($materiales as $material){
                                if(substr($material["id"],0,4)==substr($respuesta,1,4)){
                                    $opciones[]=$material["ref"];
                                }

                            }
                        }else{// *A3
                            $opciones = explode(",", $instrucciones[substr($respuesta,1)]);
                        }
                        $opciones[]="*";
                        $requerido = true;

                    }else{
                        if(strpos($respuesta,";")>0){
                           //F00M148602;F00M146634;NO
                                $tipo ="select"; //lista
                                $validador = "LISTA";
                                $respuesta = str_replace('?','',$respuesta);
                                $respuesta = explode(';', $respuesta);
                                $item = array();
                                foreach ($respuesta as $res) {
                                    $a = explode('#', $res);
                                    $item[$a[0]] = $a[1];
                                }
                                $opciones = $item;
                                $requerido = true;
                        }else{							//F00M990180
                            $tipo ="T"; //lista
                            $validador = "UNICO";
                            $opciones = [trim($respuesta)];
                            $requerido = true;
                        }


                    }
            }
            return ["indice"=>$i-11,"valor"=>$valor,"fase"=>"F".$fase,"descripcion"=>$descripcion,"operacion"=>$operacion,
                "condicional"=>$condicional,"comentario"=>$comentario,
                "noconformidad"=>$noconformidad,"limite1"=>$limite1,"limite2"=>$limite2,"tipo"=>$tipo,'instrucciones' =>$instrucciones,
                "validador"=>$validador,"opciones"=>$opciones, "requerido"=>$requerido, 'fotoInstrucciones' => $fotoInstrucciones/*, "variable"=>$variable*/];
        }


        return false;

    }

    public function getCelda($celda){
        $code = $this->objPHPExcel->getActiveSheet()->getCell($celda)->getCalculatedValue();
        if(strstr($code,'=')==true)    $code = $this->objPHPExcel->getActiveSheet()->getCell($celda)->getCalculatedValue();
        return $code;

    }
}
