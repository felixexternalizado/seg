<?php

namespace App\Http\Controllers;

use App\BancoPaso;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AppController extends Controller
{
    protected $aplicaciones = [];
    protected $plantilla = [];

    public function __construct()
    {
        $this->aplicaciones = [
            'confirmacionesproceso' => [
                'opl' => 'App\Confirmacionprocesos',
                'proceso' => 'App\Procesos',
                'foreanea_proceso' => 'procesos_id',
                'checklist' => 'App\ChecklistConfirmacionesprocesoRegistros',
                'campolinea' => 'linea',
                'campotitulo' => 'resumen',
                'foranea_noconformidad' => 'confirmacionProcesos_id',
                'foranea_checklist' => 'confirmacionproceso_id',
                'tabla' => 'confirmacionprocesos',
                'zona' => false,
                'txt' => 'Confirmacion de procesos',
                'carpeta_procesos' => $this->storage.'/confirmacion_procesos/procesos',
                'nombre' => ['confirmacionesproceso', 'confirmacionproceso', 'confirmacionprocesos']
            ],
            'registroscalidad' => [
                'opl' => 'App\Registrocalidad',
                'proceso' => 'App\Procesoscalidad',
                'foreanea_proceso' => 'procesocalidad_id',
                'checklist' => 'App\ChecklistRegistrosdecalidadRegistros',
                'campolinea' => 'linea_id',
                'campotitulo' => 'resumen',
                'foranea_noconformidad' => 'registrocalidad_id',
                'foranea_checklist' => 'registrocalidad_id',
                'tabla' => 'registrosdecalidad',
                'zona' => true,
                'txt' => 'Registros de calidad',
                'carpeta_procesos' => $this->storage.'/registros_calidad/procesos',
                'nombre' => ['registroscalidad', 'registrocalidad']
            ],
            'cambiocatalogo' => [
                'opl' => 'App\CambioCatalogo',
                'checklist' => 'App\ChecklistCambiocatalogoRegistros',
                'campolinea' => 'linea_id',
                'campotitulo' => 'alternador',
                'foranea_checklist' => 'cambiocatalogo_id',
                'tabla' => 'cambiocatalogo',
                'zona' => false,
                'txt' => 'Cambio de catalogo',
                'carpeta_procesos' => $this->storage.'/cambiocatalogo/plantillas',
                'nombre' => ['cambiocatalogo']
            ]
        ];

        $this->aplicaciones['confirmacionprocesos'] = $this->aplicaciones['confirmacionesproceso'];

        $this->aplicaciones['confirmacionproceso'] = $this->aplicaciones['confirmacionesproceso'];

        $this->aplicaciones['registrocalidad'] = $this->aplicaciones['registroscalidad'];

        $this->plantilla = [
            'fase' => 'B',
            'descripcion' => 'C',
            'operacion' => 'D',
            'instrucciones' => 'E',
            'respuesta' => 'F',
            'condicional' => 'G',
            'noconformidad' => 'H',
            'limite1' => 'I',
            'limite2' => 'J',
            'fotoInstrucciones' => 'K'
        ];

    }

    //<editor-fold desc="NoCRUD">
    public function getProcesos($ruta)
    {
        $scandir = scandir($ruta);
        $procesos = array();
        for ($i = 2; $i < count($scandir); $i++) {
            $hijo = $scandir[$i];
            $hijo_scandir = scandir($ruta . '/' . $hijo);
            for ($j = 2; $j < count($hijo_scandir); $j++) {
                $nieto = $hijo_scandir[$j];
                $procesos[] = explode('.', $nieto)[1];
            }
        }
        sort($procesos);
        return $procesos;
    }

    public function condicionalExcel($celda) {
        $condicional = '';
        if($celda === null) {
            $condicional = '';

        } elseif (strpos($celda,':')){
            $explode = explode(':', $celda);
            if(!is_numeric($explode[0]) && isset($explode[1]) && is_numeric($explode[1])) {
                $explode[1] = trim($explode[1]) - 11;
                $condicional = implode(':', $explode);
            } elseif(is_numeric($explode[0])) {
                for($i = 0; $i < count($explode); $i++) {
                    if($i == 0) $explode[$i] = trim($explode[$i]) - 11;
                    $explode[$i] = trim($explode[$i]);
                }
                $condicional = implode(':', $explode);
            }
        } else {
            if (strpos($celda,'-')){
	        	$explode = explode('-',trim($celda));
	            $explode[0] = trim($explode[0]) - 11;
	            if (isset($explode[1])) $explode[1] = trim($explode[1]);
	            $condicional = implode('-', $explode);	
        	} else {
                $condicional = $celda;
            }
        }

        return $condicional;
    }

    public function translateWeek($dia)
    {
        switch ($dia) {
            case 'Monday' :
                return 'Lunes';
                break;
            case 'Tuesday' :
                return 'Martes';
                break;
            case 'Wednesday' :
                return 'Miercoles';
                break;
            case 'Thursday' :
                return 'Jueves';
                break;
            case 'Friday' :
                return 'Viernes';
                break;
            case 'Saturday' :
                return 'Sabado';
                break;
            case 'Sunday' :
                return 'Domingo';
                break;
        }
    }

    public function reReadXml()
    {
        try {
            $storage = $this->storage . '/bancos/datos/';
            //BUSCA TODAS LAS CARPETAS BAJO DATOS
            $this->buscarCarpetas($storage);

            return response("Datos importados de los excel con éxito");
        } catch (\Exception $e) {
            throw($e);
        }
    }

    private function buscarCarpetas($path, $banco = null)
    {
        $stop = false;

        foreach (scandir($path) as $elemento) { //HASTA QUE NO ENCUENTRA UN FICHERO, SIGUE BUSCANDO EN SUBCARPETAS
            if ($stop) continue;

            if ($elemento == "." || $elemento == "..") continue;

            if (is_numeric(strpos($elemento, 'banco')) || is_numeric(strpos($elemento, 'Banco'))) $banco = $elemento;

            if (is_dir($path . '/' . $elemento)) {
                //var_dump($path . '/' . $elemento.' Es una carpeta');
                $this->buscarCarpetas($path . '/' . $elemento, $banco);
            } else {
                $stop = true;
                //var_dump($path . '/' . $elemento.' Es un archivo');
                //CUANDO ENCUENTRA UN ARCHIVO XML
                $this->listarArchivos($path, $banco);
            }
        }
    }

    private function listarArchivos($path, $banco)
    {
        $totalOk = 0;
        $totalnOk = 0;
        $fecha = null;
        $inicio = null;
        $fin = null;
        $catalogo = 'error';
        $minValor = null;
        $maxValor = null;
        $pruebas = [];

        $dir = opendir($path);
        //ABRE EL ARCHIVO XML

        $folder = explode('/', $path)[count(explode('/', $path)) - 1];
        $banco = explode('/', $path)[count(explode('/', $path)) - 2];
        $carp = explode('#', $folder);

        while ($elemento = readdir($dir)) {

            if (is_dir($path . '/' . $elemento)) continue;

            if ($elemento == '.' || $elemento == '..') continue;

            if (substr($elemento, -2) === 'ok' || substr($elemento, -2) === 'ko') continue;

            $datosArchivo = $this->leerArchivo($path, $elemento, $pruebas);
            if ($datosArchivo) {
                if ($inicio == null || $inicio > $datosArchivo['hora']) $inicio = $datosArchivo['hora'];
                if ($fin == null || $fin < $datosArchivo['hora']) $fin = $datosArchivo['hora'];
                if ($fecha == null || $fecha < $datosArchivo['fecha']) $fecha = $datosArchivo['fecha'];

                if ($datosArchivo['ok']) {
                    rename($path . '/' . $elemento, $path . '/' . $elemento . '.ok');
                    $totalOk++;
                } else {
                    rename($path . '/' . $elemento, $path . '/' . $elemento . '.ko');
                    $totalnOk++;
                }

            }

            $query = BancoPaso::select('id')
                ->where('carpeta', $carp[0])
                ->where('banco', $banco)
                ->where('fecha', Carbon::createFromFormat('d:m:Y', $datosArchivo['fecha'])->format('Y-m-d'))
                ->where('inicio', $datosArchivo['hora']);

            $arr_ps = [];
            $arr_dmc = [];

            foreach ($datosArchivo['arr_bancopaso'] as $p) {
                $p['banco'] = $banco;
                $p['carpeta'] = $carp[0];
                $p['nPruebas'] = $datosArchivo['nPruebas'];
                $arr_ps[] = $p;
                $arr_dmc[] = $p['dmc'];
            }

            $query->whereIn('dmc', $arr_dmc)->delete();

            BancoPaso::insert($arr_ps);
            unset($arr_ps);
        }

        if ($totalOk === 0 && $totalnOk === 0) return false;
    }

    private function leerArchivo($path, $elemento, &$carpeta)
    {

        $banco = explode('/', $path)[count(explode('/', $path)) - 2];

        $res = $this->xml2json($path . '/', $elemento);

        $arr_bancopaso = [];

        for ($i = 0; $i < count($res['PasoXML']); $i++) {

            $paso = $res['PasoXML'][$i];
            $dmc = isset($res['Id']) ? $res['Id'] : $res['Nombre'];
            if (is_array($dmc) && empty($dmc)) $dmc = '';
            $datosPaso = $this->leerPaso($path, $paso, $dmc, $res['Fecha'], $arr_bancopaso);

            $arr_bancopaso = $datosPaso['arr_bancopaso'];

            if (!isset($carpeta[$i])) {

                $carpeta[$i] = [
                    'banco' => $banco,
                    'minimo' => INF,
                    'maximo' => 0,
                    'media' => 0,
                    'total' => 0,
                    'nOk' => 0,
                    'paso' => $i + 1,
                    'etapa' => $datosPaso['etapa'],
                    'descripcion' => $datosPaso['descripcion']
                ];

            }

            if ($datosPaso['valor'] > $carpeta[$i]['maximo']) $carpeta[$i]['maximo'] = $datosPaso['valor'];

            if ($datosPaso['valor'] < $carpeta[$i]['minimo']) $carpeta[$i]['minimo'] = $datosPaso['valor'];

            if ($paso['S_EtapaProceso'] !== 'B') $carpeta[$i]['nOk']++;

            $carpeta[$i]['media'] += $datosPaso['valor'];

            $carpeta[$i]['total']++;

        }

        $objectFecha = explode('/', $res['Fecha']);

        if(!isset($res['NroPruebas'])) $res['NroPruebas'] = '';

        return [
            'fecha' => $objectFecha[0],
            'hora' => $objectFecha[1],
            'ok' => $res['Resultado'] == 'BUENO',
            'catalogo' => $res['CatalogoActivo'],
            'nPruebas' => $res['NroPruebas'],
            'arr_bancopaso' => $arr_bancopaso,
        ];

    }

    private function xml2json($path, $elemento)
    {

        $xmlfile = file_get_contents($path . '/' . $elemento);
        $xml = simplexml_load_string($xmlfile);

        $doc = (json_decode(json_encode($xml), true));

        return $doc;

    }

    private function leerPaso($path, $paso, $archivo, $fecha, $arr_bancopaso)
    {

        $carpetas = explode('/', $path);

        $folder = $carpetas[count($carpetas) - 1];

        $banco = $carpetas[count($carpetas) - 2];

        $carp = explode('#', $folder);

        for ($x = 1; $x <= 4; $x++) {
            if (is_array($paso["V_V" . $x])) $paso["V_V" . $x] = '';

            if (is_array($paso["S_V" . $x])) $paso["S_V" . $x] = '';

            if (is_array($paso["LI_V" . $x])) $paso["LI_V" . $x] = '';

            if (is_array($paso["LS_V" . $x])) $paso["LS_V" . $x] = '';
        }

        $objectFecha = explode('/', $fecha);

        $inicio = $objectFecha[1];

        $formatFecha = Carbon::createFromFormat('d:m:Y', $objectFecha[0])->format('Y-m-d');

        $arr_bancopaso[] = [
            'fecha' => $formatFecha,
            'inicio' => $inicio,
            'dmc' => $archivo,
            'paso' => $paso['@attributes']['index'],
            'descripcion' => is_array($paso['Proceso']) ? '' : $paso['Proceso'],
            'etapaProceso' => is_array($paso['S_EtapaProceso']) ? '' : $paso['S_EtapaProceso'],
            'etapa' => $paso['Etapa'],
            "vV1" => $paso["V_V1"],
            "sV1" => $paso["S_V1"],
            "liV1" => $paso["LI_V1"],
            "lsV1" => $paso["LS_V1"],

            "vV2" => $paso["V_V2"],
            "sV2" => $paso["S_V2"],
            "liV2" => $paso["LI_V2"],
            "lsV2" => $paso["LS_V2"],

            "vV3" => $paso["V_V3"],
            "sV3" => $paso["S_V3"],
            "liV3" => $paso["LI_V3"],
            "lsV3" => $paso["LS_V3"],

            "vV4" => $paso["V_V4"],
            "sV4" => $paso["S_V4"],
            "liV4" => $paso["LI_V4"],
            "lsV4" => $paso["LS_V4"],

            "carpeta" => $carp[0],
            'banco' => $banco,
        ];


        $valor = $paso['V_V1'];

        preg_match_all('!\d+!', $valor, $matches);

        if (count($matches[0]) > 1) {

            $valor = $matches[0][0] . '.' . $matches[0][1];

        } elseif ($valor === 'B' || $valor === 'Ok') {

            $valor = 1;

        } elseif ($valor !== 'B' || $valor !== 'Ok') {

            $valor = 0;

        } else {

            $valor = $matches[0][0];

        }

        return [
            'valor' => $valor,
            'etapa' => $paso['Etapa'],
            'descripcion' => $paso['Proceso'],
            'arr_bancopaso' => $arr_bancopaso,
        ];

    }

    public function absoluteFilter($request, $model, $query) {
        $table = $model->getTable();
        $columns = Schema::getColumnListing($table);

        if (isset($request->sSearch) && $request->sSearch != '') {
            $query->where('id');
            for ($i = 0; $i < count($columns); $i++) {
                $query->orWhere($columns[$i], $request->sSearch);
            }
        }

        if (isset($request->filters) && is_array($request->filters) && isset($request->filters[0])) {
            for ($i = 0; $i < count($request->filters); $i++) {

                $filter = json_decode($request->filters[$i], true);
                ksort($filter);

                $aux[$i] = [];
                $like = false;
                foreach ($filter as $key => $value) {

                    if($key=='campo' && ($value == 'linea_id' || $value == 'zona_id') && $table == 'empleados') $value = 'default_'.$value;

                    if($key=='campo' &&
                        ($value == 'linea_id' || $value == 'cargo_id' || $value == 'empleado_id') &&
                        $table == 'cargoslineasempleados') {
                        $explode = explode('_',$value);
                        $explode[0] = $explode[0].'s';
                        $value = implode('_',$explode);
                    }

                    if($key == 'campo' && !in_array($value, $columns)) abort(422, "El campo por el que quiere filtrar no existe");

                    if($key=='campo' && $value == 'carpeta' && ($table == 'procesoscalidad' || $table == 'procesos')) $value = DB::raw('SUBSTRING_INDEX(resumen,".",1)');

                    if($key=='campo' && $value == 'resumen' && ($table == 'procesoscalidad' || $table == 'procesos')) $value = DB::raw('SUBSTRING_INDEX(resumen,".",-1)');



                    if(isset($like) && $like === true) $value = '%'.$value.'%';

                    $aux[$i][] = $value;

                    if($key == 'comparacion' && $value == 'like') $like = true;
                }
            }
            $query->where($aux);
        }

        if (isset($request->sortBy) && isset($request->sortDesc) && $request->sortBy != '') {

            if($request->sortBy == 'area' && $table == 'lineas') $request->sortBy = 'resumen';

            if($table == 'empleados' && ($request->sortBy  == 'linea_id' || $request->sortBy == 'zona_id')) $request->sortBy = 'default_'.$request->sortBy;

            if($table == 'cargoslineasempleados') {
            	$explode = explode('_',$request->sortBy);
            	if(isset($explode[1])) {
            		$explode[0] = $explode[0] . 's';
            		$request->sortBy = implode('_',$explode);
            	}
            }

            if (!in_array($request->sortBy, $columns)) abort(422,"El campo por el que quiere ordenar no existe");

            if($request->sortBy == 'carpeta' && ($table == 'procesos' || $table == 'procesoscalidad')) $request->sortBy = DB::raw('SUBSTRING_INDEX(resumen,".",1)');

            if($request->sortBy == 'resumen' && ($table == 'procesos' || $table == 'procesoscalidad')) $request->sortBy = DB::raw('SUBSTRING_INDEX(resumen,".",-1)');

            $order = $request->sortDesc == "true" ? 'DESC' : 'ASC';
            $query->orderBy($request->sortBy, $order);
        }

        return $query;
    }

    public function getBarcode($barcode)
    {
        $arr_codes = [];
        $code = explode(':', $barcode)[1];
        $code = strtolower($code) . '_reader';
        $arr_codes[] = $code;

        return $arr_codes;
    }
    //</editor-fold>
}
