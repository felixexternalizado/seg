<?php

namespace App\Http\Controllers;

use App\Manodeobra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ManodeobraController extends Controller
{
    public function index(Request $request)
    {
        $query = Manodeobra::query();
        if (isset($request->linea_id) && $request->linea_id != null) {
            $query->where('linea_id', $request->linea_id);
        }
        if (isset($request->zona_id) && $request->zona_id != null) {
            $query->where('zona_id', $request->zona_id);
        }
        if (isset($request->empleado_id) && $request->empleado_id != null) {
            $query->where('empleado_id', $request->empleado_id);
        }
        if (isset($request->fecha) && $request->fecha != null) {
            $query->where('fecha', 'like', '%' . $request->fecha . '%');
        }
        if (isset($request->turno) && $request->turno != null) {
            $query->where('turno', $request->turno);
        }

        if (isset($request->sort) && isset($request->order) && $request->sort != null && $request->order != null) {
            $query->orderBy($request->sort, $request->order);
        } else {
            $query->orderBy('fecha', 'ASC');
        }
        $result = $query->with('empleado','zona','linea')->get();

        return response($result);
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $this->validate($request, [
                'linea_id' => ['required', 'exists:lineas,id'],
                'zona_id' => ['required', 'exists:zonasdelinea,id'],
                'empleado_id' => ['required', 'exists:empleados,id'],
                'fecha' => ['required'],
                'turno' => ['required', 'digits_between:1,3'],
            ]);

            $manodeobra = Manodeobra::create([
                'linea_id' => $request->linea_id,
                'zona_id' => $request->zona_id,
                'empleado_id' => $request->empleado_id,
                'fecha' => $request->fecha,
                'turno' => $request->turno,
            ]);
            $manodeobra->zona = $manodeobra->zona()->first();
            $manodeobra->linea = $manodeobra->linea()->first();
            $manodeobra->empleado = $manodeobra->empleado()->first();
            DB::commit();
            return response($manodeobra);

        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function show($id)
    {
        try {
            DB::beginTransaction();

            $manodeobra = Manodeobra::where('id', $id)->first();
            if (!$manodeobra) return response(['message' => 'No existe la linea solicitada'], 404);

            DB::commit();
            return $manodeobra;
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            $manodeobra = Manodeobra::where('id', $id)->first();
            if (!$manodeobra) return response(['message' => 'No existe el recurso solicitado'], 404);

            $this->validate($request, [
                'linea_id' => $request->linea_id,
                'zona_id' => $request->zona_id,
                'empleado_id' => $request->empleado_id,
                'fecha' => $request->fecha,
                'turno' => $request->turno,
            ]);

            $manodeobra->update($request->all());

            DB::commit();
            return response($manodeobra);

        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            if ($id <= 0 || $id == null) return response(['message' => 'No existe el recurso solicitado'], 404);

            Manodeobra::where('id', $id)->delete();
            DB::commit();
            return response(['message' => 'ok']);
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }
}
