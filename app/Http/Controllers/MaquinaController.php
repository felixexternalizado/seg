<?php

namespace App\Http\Controllers;

use App\Maquina;
use App\Registrocalidad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MaquinaController extends AppController
{
    public function index(Request $request)
    {
        try {
            DB::beginTransaction();
            $query = Maquina::query();

            $model = new Maquina();

            $query = $this->absoluteFilter($request,$model,$query);

            $results = $query->get();

            foreach ($results as $result) {
                $result->setAttributeLinea($result->linea_id);
                $result->setAttributeZona($result->zona_id);
            }
            DB::commit();
            return response($results);
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function store(Request $request) {
        try {
            DB::beginTransaction();

            $this->validate($request, [
                'linea_id' => ['required', 'exists:lineas,id'],
                'zona_id' => ['required', 'exists:zonasdelinea,id'],
                'fase' => ['required'],
                'descripcion' => ['required'],
            ]);

            $maquina = Maquina::create([
                'linea_id' => $request->linea_id,
                'zona_id' => $request->zona_id,
                'fase' => $request->fase,
                'descripcion' => $request->descripcion,
            ]);
            DB::commit();
            return response($maquina);

        } catch(\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function show($id)
    {
        try {
            DB::beginTransaction();

            $line = Maquina::where('id', $id)->first();
            if (!$line) return response(['message' => 'No existe la linea solicitada'], 404);

            DB::commit();
            return $line;
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function update(Request $request, $id) {
        try {
            DB::beginTransaction();

            $maquina = Maquina::where('id',$id)->first();
            if (!$maquina) return response(['message' => 'No existe el recurso solicitado'], 404);

            $this->validate($request, [
                'linea_id' => ['required', 'exists:lineas,id'],
                'zona_id' => ['required', 'exists:zonasdelinea,id'],
            ]);

            $maquina->update($request->all());

            DB::commit();
            return response($maquina);

        } catch(\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function destroy($id) {
        try {
            DB::beginTransaction();
            if($id <= 0 || $id == null) return response(['message' => 'No existe el recurso solicitado'], 404);

            $regcal = Registrocalidad::where('maquina_id', $id)->get();
            if(isset($regcal) && !$regcal->isEmpty()) {
                return response(['message' => 'No puede eliminarse la máquina, esta asociada a varios registros de calidad'], 422);
            }

            Maquina::where('id',$id)->delete();
            DB::commit();
            return response(['message' => 'ok']);
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }
}
