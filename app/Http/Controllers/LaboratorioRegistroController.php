<?php

namespace App\Http\Controllers;

use App\LaboratorioRegistro;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class LaboratorioRegistroController extends AppController
{
    public function index(Request $request)
    {
        $query = LaboratorioRegistro::query();

        $query = $this->readFilters($request, $query);

        $lab = $query->with('linea','empleado','zona')->get();


        return $lab;
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $this->validate($request, [
                'linea_id' => ['required', 'exists:lineas,id'],
                'zona_id' => ['required', 'exists:zonasdelinea,id'],
                'numero_pieza' => ['required'],
                'catalogo' => ['required'],
                'nombre_pieza' => ['required'],
            ]);

            $lab = LaboratorioRegistro::create([
                'linea_id' => $request->linea_id,
                'zona_id' => $request->zona_id,
                'empleado_id' => auth()->user()->empleado_id,
                'numero_pieza' => $request->numero_pieza,
                'catalogo' => $request->catalogo,
                'nombre_pieza' => $request->nombre_pieza,
                'fecha' => Carbon::now()->format('Y-m-d'),
            ]);

            DB::commit();

            return response($lab);

        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function show($id)
    {
        try {
            $lab = LaboratorioRegistro::findOrFail($id);

            if (!$lab) return response(['message' => 'No existe el registro solicitado'], 404);

            return response($lab);
        } catch (\Exception $e) {
            throw($e);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            $lab = LaboratorioRegistro::findOrFail($id);

            if (isset($request->linea_id)) {
                $this->validate($request, [
                    'linea_id' => ['required','exists:lineas,id'],
                ]);
            }
            if (isset($request->zona_id)) {
                $this->validate($request, [
                    'zona_id' => ['required','exists:zonasdelinea,id'],
                ]);
            }
            if (isset($request->fecha)) {
                $this->validate($request, [
                    'fecha' => ['date', 'date_format:d/m/Y'],
                ]);
                $request->merge(['fecha' => Carbon::createFromFormat('d/m/Y', $request->fecha)->format('Y-m-d H:i:s'),]);
            }
            if(isset($request->estado)) {
                $array_estados = ['pendiente','entregado','aceptado','rechazado','cerrado'];
                if(!in_array($request->estado,$array_estados)) return response('Ese estado no es válido para una pieza de laboratorio');
            }

            $lab->update($request->all());

            DB::commit();

            return response($lab);

        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function destroy($id)
    {
        try {
            $lab = LaboratorioRegistro::where('id',$id)->first();

            if (!$lab) return response(['message' => 'No existe el registro solicitado'], 404);

            $lab->delete();

            return response(['message' => 'El registro ha sido eliminado con éxito'], 200);
        } catch (\Exception $e) {
            throw($e);
        }
    }

    private function readFilters($request, $query) {

        $cols = ['numero_pieza', 'catalogo', 'fecha', 'nombre_pieza','estado', 'linea'];

        if (isset($request->linea_id)) $query->where('linea_id', $request->linea_id);
        if (isset($request->zona_id)) $query->where('zona_id', $request->zona_id);
        if (isset($request->numero_pieza)) $query->where('numero_pieza', $request->numero_pieza);
        if (isset($request->catalogo)) $query->where('catalogo', $request->catalogo);
        if (isset($request->nombre_pieza)) $query->where('nombre_pieza', $request->nombre_pieza);
        if (isset($request->fecha)) $query->where('fecha', $request->fecha);

        isset($request->estado) ? $query->where('estado', $request->estado) : $query->where('estado', 'pendiente');

        if(isset($request->sortBy) && !empty($request->sortBy) && isset($request->sortDesc) && $request->sortBy != 'linea' && $request->sortBy != 'zona_id') {
            $request->sortDesc == 'true' ? $query->orderBy($request->sortBy, 'ASC') : $query->orderBy($request->sortBy, 'DESC');
        }

        if (isset($request->filter) && $request->filter != '' && isset($cols)) {
            $query->where(function ($query) use ($request, $cols) {
                foreach ($cols as $col) {
                    $query->orWhere($col, 'like', '%' . $request->sSearch . '%');
                }
            }
            );
        }

        return $query;
    }

    public function piezasEnviadas(Request $request) {
        try {
            DB::beginTransaction();

            $this->validate($request, [
                'linea_id' => ['required', 'exists:lineas,id'],
                'zona_id' => ['required', 'exists:zonasdelinea,id'],
                'numero_piezas' => ['required', 'numeric'],
            ]);
            for ($i = 0; $i < $request->numero_piezas; $i++) {
                LaboratorioRegistro::create([
                    'linea_id' => $request->linea_id,
                    'zona_id' => $request->zona_id,
                    'empleado_id' => auth()->user()->empleado_id,
                    'numero_pieza' => '',
                    'catalogo' => '',
                    'nombre_pieza' => '',
                    'fecha' => Carbon::now()->format('Y-m-d H:i:s'),
                ]);
            }
            DB::commit();
            return response(['message' => 'Piezas enviadas'], 200);

        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }
}
