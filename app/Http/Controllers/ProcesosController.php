<?php

namespace App\Http\Controllers;

use App\Confirmacionprocesos;
use App\Procesos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProcesosController extends AppController
{
    public function index(Request $request)
    {
        $query = Procesos::query();

        $model = new Procesos();

        $query = $this->absoluteFilter($request,$model,$query);

        $result = $query->get();

        return response($result);
    }

    public function store(Request $request) {
        try {
            DB::beginTransaction();

            $this->validate($request, [
                'resumen' => ['required', 'string', 'max:255']
            ]);

            $aux = explode('.',$request->resumen);
            if(!isset($aux[1])) return response('El nombre del proceso debe tener el formato "carpeta.nombreproceso"', 422);

            $proceso = Procesos::create([
                'resumen' => $request->resumen,
            ]);
            DB::commit();
            return response($proceso);

        } catch(\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function show($id)
    {
        try {
            DB::beginTransaction();

            $proceso = Procesos::where('id', $id)->first();
            if (!$proceso) return response(['message' => 'No existe el recurso solicitado'], 404);

            DB::commit();
            return response($proceso);
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function update(Request $request, $id) {

        $proceso = Procesos::findOrFail($id);

        $aux = explode('.',$request->resumen);
        if(!isset($aux[1])) return response('El nombre del proceso debe tener el formato "carpeta.nombreproceso"', 422);

        $proceso->update([
            'resumen' => $request->resumen,
        ]);

        return response($proceso);
    }

    public function destroy($id) {
        try {
            DB::beginTransaction();
            if($id <= 0 || $id == null) return response(['message' => 'No existe el recurso solicitado'], 404);
            $confpro = Confirmacionprocesos::where('procesos_id', $id)->get();
            if(isset($confpro) && !$confpro->isEmpty()) {
                return response(['message' => 'No puede eliminarse el proceso, esta asociado a varias confirmaciones de proceso'], 422);
            }
            Procesos::where('id',$id)->delete();
            DB::commit();
            return response(['message' => 'ok']);
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }
}
