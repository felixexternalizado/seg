<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ConfigController extends Controller
{

    public function index(Request $request)
    {
        if(!isset($request->nombre)) {
            $arr_json = [];
            foreach (scandir($this->storage) as $json) {
                if(substr($json, -5) !== '.json') continue;

                $arr_json[] = [
                    'id' => $json,
                    'resumen' => substr($json,0,-5),
                    'contenido' => file_get_contents($this->storage . '/' . $json),
                ];
            }
            return response($arr_json);
        }
        $file = $this->storage . '/' . $request->nombre . '.json';
        if (file_exists($file)) {
            $contents = file_get_contents($file);
            return response($contents);
        } else {
            return response(['message' => 'No existe el archivo '.$request->nombre . '.json'], 404);
        }
    }

    public function store(Request $request)
    {
        $this->crearmodificar($request);

        if(!file_exists($this->storage . '/' . $request->resumen.'.json'))  return response(['message' => 'No ha podido crearse el archivo '.$request->resumen.'.json, ha ocurrido un error', 500]);

        return response(['message' => 'Se ha creado el archivo '.$request->resumen.'.json con éxito'], 200);
    }

    public function update(Request $request)
    {
        $this->crearmodificar($request);

        if(!file_exists($this->storage . '/' . $request->resumen.'.json'))  return response(['message' => 'No ha podido crearse el archivo '.$request->resumen.'.json, ha ocurrido un error', 500]);

        return response(['message' => 'Se ha modificado el archivo '.$request->resumen.'.json con éxito'], 200);

    }
    public function destroy($json)
    {
        if(!file_exists($this->storage . '/' . $json)) return response(['message' => 'No se ha encontrado el archivo'],200);

        unlink($this->storage . '/' . $json);
        return response(['message' => 'Archivo eliminado'],200);
    }

    private function crearmodificar($request) {
        $validator = Validator::make($request->all(), [
            'resumen' => ['required'],
            'contenido' => ['required'],
        ]);
        if ($validator->fails()) {
            return response($validator->errors());
        }
        file_put_contents($this->storage . '/' . $request->resumen.'.json',$request->contenido);
    }
}
