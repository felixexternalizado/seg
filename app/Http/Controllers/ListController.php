<?php

namespace App\Http\Controllers;

use App\ChecklistConfirmacionesprocesoRegistros;
use App\ChecklistRegistrosdecalidadRegistros;
use App\Dias;
use App\Documento;
use App\Empleados;
use App\Confirmacionprocesos;
use App\Registrocalidad;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Symfony\Component\HttpKernel\EventListener\ValidateRequestListener;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;
use Illuminate\Support\Facades\Schema;

class ListController extends AppController
{
    public $objPHPExcel;

    public function trabajadores(\App\Line $line, $anio)
    {
        $procesos = \App\Procesos::all()->toArray();

        $array_listas = array();
        $array_listas['empleados'] = array();
        $array_listas['procesos'] = array();
        $array_listas['calculado'] = array();

        $array_semanas = Dias::where('anno', $anio)->get()->toArray();
        $newarray = array();
        foreach ($array_semanas as $s) {
            if ($s['mes'] != 12 || $s['semana'] != 1) $newarray[$s['mes']][$s['semana']][] = $s['dia'];
        }

        $empleados = \App\Cargoslineasempleados::select('empleados_id')
            ->where('lineas_id', $line->id)
            ->get()->toArray();

        $ultimasemana = 52;

        $empleados = Empleados::whereIn('id', $empleados)->get()->toArray();
        foreach ($empleados as $empleado) {
            $array2 = [];
            $array2['data'] = $empleado;
            $listas = \App\Confirmacionprocesos::where('linea', $line->id)
                ->where('empleados_id', $empleado['id'])
                ->where(DB::raw('YEAR(fechaInicio)'), $anio)
                ->orderBy('fechaInicio', 'ASC')
                ->get()->toArray();

            for ($i = 0; $i <= $ultimasemana; $i++) {
                $array2['semanas'][$i] = array();
                $count = 0;
                $hechos = 0;
                foreach ($listas as $lista) {
                    $mes = substr($lista['fechaInicio'], 5, 2);
                    $dia = substr($lista['fechaInicio'], 8, 2);
                    $o = $newarray[$mes];
                    foreach ($o as $index => $value) {
                        if (intval($index) - 1 == $i && in_array($dia, $value)) {
                            $count++;
                            if ($lista['estado'] == 'cerrado') $hechos++;

                            $array2['semanas'][$i] = $hechos . '/' . $count;
                        }
                    }
                }
            }
            array_push($array_listas['empleados'], $array2);
        }
        foreach ($procesos as $proceso) {
            $array2 = [];
            $array2['data'] = $proceso;
            $listas = \App\Confirmacionprocesos::select('id', 'procesos_id', 'empleados_id', 'resumen', 'evaluador', 'fechaInicio', 'fechaFinal', 'totalMal', 'totalBien', 'porcentajeBien', 'totalPreguntas', 'linea', 'estado', 'created_at', 'updated_at')
                ->where('linea', $line->id)
                ->where('procesos_id', $proceso['id'])
                ->where(DB::raw('YEAR(fechaInicio)'), $anio)
                ->orderBy('fechaInicio', 'ASC')
                ->get();

            for ($i = 0; $i <= $ultimasemana; $i++) {
                $array2['semanas'][$i] = array();
                foreach ($listas as $lista) {
                    $mes = substr($lista['fechaInicio'], 5, 2);
                    $dia = substr($lista['fechaInicio'], 8, 2);
                    $o = $newarray[$mes];
                    $lista['empleado'] = Empleados::findOrFail($lista->empleados_id)->toArray();
                    foreach ($o as $index => $value) {
                        if (intval($index) - 1 == $i && in_array($dia, $value)) {
                            $lista->setEliminarAttribute($lista);
                            $array2['semanas'][$i][] = $lista->toArray();
                        }
                    }
                }
            }
            array_push($array_listas['procesos'], $array2);
        }
        $arraytxt = [0 => 'Total planificado', 1 => 'Total realizado', 2 => 'Porcentaje'];
        for ($j = 0; $j < count($arraytxt); $j++) {
            $array2 = [];
            $array2['data'] = $arraytxt[$j];
            $listas = \App\Confirmacionprocesos::select('*')
                ->where('linea', $line->id)
                ->where(DB::raw('YEAR(fechaInicio)'), $anio)
                ->orderBy('fechaInicio', 'ASC')
                ->get()->toArray();

            for ($i = 0; $i <= $ultimasemana; $i++) {
                $count = 0;
                $cerrado = 0;
                $array2['semanas'][$i] = array();
                foreach ($listas as $lista) {
                    $mes = substr($lista['fechaInicio'], 5, 2);
                    $dia = substr($lista['fechaInicio'], 8, 2);
                    $o = $newarray[$mes];
                    foreach ($o as $index => $value) {
                        if (intval($index) - 1 == $i && in_array($dia, $value)) {
                            $count++;
                            if ($lista['estado'] == 'cerrado') {
                                $cerrado++;
                            }
                        }
                    }
                }
                if ($count > 0) {
                    if ($j == 0) $array2['semanas'][$i] = $count;

                    if ($j == 1) $array2['semanas'][$i] = $cerrado;

                    if ($j == 2) {
                        $porcentaje = number_format((intval($cerrado) / intval($count)) * 100, 1);
                        if ($porcentaje == 0) {
                            $array2['semanas'][$i] = '0%';
                        } else {
                            $array2['semanas'][$i] = $porcentaje . '%';
                        }

                    }
                }
            }
            array_push($array_listas['calculado'], $array2);
        }

        $array_listas['meses'] = $this->meses();

        return response($array_listas);
    }


    public function procesos()
    {
        $lineaprocesos = \App\Procesos::get()->toArray();
        return response($lineaprocesos);
    }

    public function listaPorUsuario($empleado, $anio)
    {
        $array_listas['lineas'] = array();
        $lineas = \App\Line::all();

        $empleado = Empleados::where('id', $empleado)->first();

        $array_semanas = Dias::where('anno', $anio)->get()->toArray();
        $newarray = array();
        foreach ($array_semanas as $s) {
            if ($s['mes'] == 12 && $s['semana'] == 1) {
            } else {
                $newarray[$s['mes']][$s['semana']][] = $s['dia'];
            }
        }

        $ultimasemana = 52;

        foreach ($lineas as $linea) {
            $array2 = [];
            $array2['data'] = $linea->toArray();
            $confprocesos = \App\Confirmacionprocesos::select('id', 'procesos_id', 'empleados_id', 'resumen', 'evaluador', 'fechaInicio', 'fechaFinal', 'totalMal', 'totalBien', 'porcentajeBien', 'totalPreguntas', 'linea', 'estado', 'created_at', 'updated_at')
                ->where('linea', $linea->id)
                ->where('empleados_id', $empleado->id)
                ->where(DB::raw('YEAR(fechaInicio)'), 'like', $anio . '%')
                ->orderBy('fechaInicio', 'ASC')
                ->get()->toArray();
            for ($i = 0; $i <= $ultimasemana; $i++) {
                $array2['semanas'][$i] = array();
                $count = 0;
                foreach ($confprocesos as $confproceso) {
                    $mes = substr($confproceso['fechaInicio'], 5, 2);
                    $dia = substr($confproceso['fechaInicio'], 8, 2);
                    $o = $newarray[$mes];
                    foreach ($o as $index => $value) {
                        if (intval($index) - 1 == $i && in_array($dia, $value)) {
                            $count++;
                            $array2['semanas'][$i] = $count;
                        }
                    }
                }
            }
            array_push($array_listas['lineas'], $array2);
        }
        $array_listas['meses'] = $this->meses();
        $array_listas['empleado'] = $empleado;

        return response($array_listas);
    }

    public function getJson($tipo, $id)
    {
        try {
            DB::beginTransaction();
            $array_fase = array();
            $model = $this->aplicaciones[$tipo]['opl'];
            $model = $model::query();

            $aplicacion = $model->findOrFail($id);
            if($aplicacion->estado == 'cerrado') return response(['message' => 'Este checklist esta cerrado.'], 403);
            $new_json = array();
            isset($aplicacion->resumen) ? $new_json['id'] = $aplicacion->resumen : $new_json['id'] = $aplicacion->alternador;
            isset($aplicacion->fechaFinal) ? $new_json['fechaFinal'] = $aplicacion->fechaFinal : $new_json['fechaFinal'] = null;
            $fases = $aplicacion->registros()->select('fase')->distinct('fase')->get()->toArray();
            foreach ($fases as $fase) {
                $array_fase[] = $fase['fase'];
            }
            $registros = array();
            $reg = $aplicacion->registros()->get()->toArray();
            $type_array = ['H','B','O','A','barcode'];

            //Leemos la 2a página de la plantilla de excel.
            $frecuencia = isset($aplicacion->frecuencia) ? $aplicacion->frecuencia : null;
            $arraytabla = [];
            $array_tipo_tabla = [];
            if($tipo != 'cambiocatalogo') {
                $arraytabla = $this->getTabla($tipo,$aplicacion,$frecuencia);
                $array_tipo_tabla = ['Espiras','Resistencia'];
            }

            foreach ($reg as $registro) {
                $reg_op = $registro['opciones'];

                if(strpos($registro['noconformidad'],':')) {

                    if($registro['tipo'] == 'ejecutar') $nc = $registro['noconformidad'];

                    $registro['crearNC'] = $this->crearNc($registro);
                    $registro['noconformidad'] = '';
                }
                if(strpos($registro['condicional'],':')) {
                    $tipo_tabla = explode(':',$registro['condicional']);
                    if(in_array($tipo_tabla[0],$array_tipo_tabla)) {
                        if(isset($tipo_tabla[1])) {
                            $registro['tabla'] = [
                                'nombre' => strtolower($tipo_tabla[0]),
                                'indice' => (int)$tipo_tabla[1]
                            ];
                            $registro['condicional'];
                        }
                    } else {
                        $registro['saltarPregunta'] = $this->saltarPregunta($registro);
                    }
                    $registro['condicional'] = '';
                } elseif(in_array($registro['condicional'],$array_tipo_tabla) && !empty($arraytabla)) {
                	foreach ($arraytabla[$registro['condicional']] as $item) {
                		if($item['referencia'] !== $aplicacion->valor) continue;
						
						$registro['crearNC'] = $item['crearNC'];
                	}
                }

                if ($registro['tipo'] == 'select') {
                    $opciones = [];
                    foreach (explode(';', $registro['opciones']) as $op) {
                        if ($op === "") continue;
                        $trozos = explode(':', $op);
                        $opciones[] = [
                            'value' => $trozos[0],
                            'text' => $trozos[1]
                        ];
                    }
                    $registro['opciones'] = $opciones;
                } elseif(in_array($registro['tipo'],$type_array)) {
                    $opciones = [];
                    foreach (explode(';', $registro['opciones']) as $op) {
                        if ($op === "") continue;
                        $trozos = explode(':', $op);
                        $opciones[] = $trozos[1];
                    }
                    $registro['opciones'] = $opciones;
                } elseif($registro['tipo'] == 'ejecutar') {
                    $opc = [];

                    foreach (explode(';', $reg_op) as $op) {
                        if ($op === "") continue;
                        $trozos = explode(':', $op);
                        $opc[] = $trozos[1];
                    }
                    if(isset($nc)) {
                        unset($registro['crearNC']);
                        $arr = $this->ejecutar($aplicacion,$registro,$reg_op,$nc);
                    } else {
                        $arr = $this->ejecutar($aplicacion,$registro,$reg_op);
                    }


                    $registro['opciones'] = $arr;

                    $arr = [];

                }

                if($registro['indice'] != '') $registro['indice'] = (int)$registro['indice'];
                if($registro['limite1'] != '') $registro['limite1'] = (int)$registro['limite1'];
                if($registro['limite2'] != '') $registro['limite2'] = (int)$registro['limite2'];
                if(isset($registro['formato']) && $registro['formato'] != '') {
                    if(is_numeric($registro['formato'])) {
                        $registro['decimales'] = (int)$registro['formato'];
                        unset($registro['formato']);
                    } else {
                        $registro['formato'] = $registro['formato'];
                    }
                }
                

                if (isset($registro['evaluador'])) {
                    $registro['oplData'] = [
                        'empleados_id' => $registro['evaluador'],
                    ];
                }

                $registros[] = $registro;
            }
            $new_json['fases'] = $array_fase;
            $new_json['registros'] = $registros;
            $new_json['adjuntos'] = $aplicacion->documentos();

            if(!empty($arraytabla)) $new_json['tablas'] = $arraytabla;
            
            $aplicacion->json = json_encode($new_json);
            if (!isset($aplicacion->json) || $aplicacion->json == null) {
                return response(['message' => 'La confirmación del proceso no tiene JSON'], 422);
            } else {
                DB::commit();
                return response($aplicacion->json);
            }

        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function calendario(Request $request, $tipo)
    {
        $array_fase = array();
        $model = $this->aplicaciones[$tipo]['opl'];
        $jsons = $model::query();

        if(isset($request->estado)) $jsons->where('estado', $request->estado);

        $array_json = array();
        foreach ($jsons->get() as $json) {
            $new_json = array();
            $new_json['alternador'] = isset($json->resumen) ? $json->resumen : $json->alternador;
            $new_json['totalMal'] = $json->totalMal;
            $new_json['totalBien'] = $json->totalBien;
            $new_json['porcentajeBien'] = $json->porcentajeBien;
            $new_json['totalPreguntas'] = $json->totalPreguntas;
            $new_json['finalizacion'] = $json->updated_at;
            $fases = $json->registros()->select('fase')->distinct('fase')->get()->toArray();
            foreach ($fases as $fase) {
                $array_fase[] = $fase['fase'];
            }
            $new_json['fases'] = $array_fase;
            $new_json['registros'] = $json->registros()->get()->toArray();
            $new_json['adjuntos'] = $json->documentos();
            $json->json = json_encode(['catalogo' => $new_json]);
            $array_json[] = json_decode($json->json);
        }
        return response(['code' => 0, 'data' => $array_json, 'message' => 'ok']);
    }

    public function guardarRegistro(Request $request, $tipo, $id)
    {
        try {
            DB::beginTransaction();

            $checklist = $this->aplicaciones[$tipo]['checklist'];
            $checklist = $checklist::findOrFail($id);

            $checklist->update($request->all());

            DB::commit();
            return response($checklist, 200);

        } catch (\Exception $e) {
            DB::rollBack();
            throw ($e);
        }
    }

    public function borrarChecklist($tipo, $id)
    {
        try {
            DB::beginTransaction();

            $aplicacion = $this->aplicaciones[$tipo]['opl'];
            $checklist = $this->aplicaciones[$tipo]['checklist'];
            $checklist::where($this->aplicaciones[$tipo]['foranea_checklist'], $id)->delete();
            if($tipo == 'cambiocatalogo') {
                Documento::where($this->aplicaciones[$tipo]['foranea_checklist'], $id)->delete();
            }
            $app = $aplicacion::findOrFail($id);
            $app->delete();

            DB::commit();
            return response("El registro se ha eliminado", 200);

        } catch (\Exception $e) {
            DB::rollBack();
            throw ($e);
        }
    }

    public function finalizarChecklist($tipo, $id)
    {
        try {
            DB::beginTransaction();
            $model = $this->aplicaciones[$tipo]['opl'];
            $model = $model::findOrFail($id);
            $checklist = $this->aplicaciones[$tipo]['checklist'];
            $checklist = $checklist::where($this->aplicaciones[$tipo]['foranea_checklist'], $id)->where('tipo', 'not like', '%!%')->get();

            $total = $model->totalPreguntas;
            $bien = $checklist->where('ncFlag', 0)->count();
            $porcentajeBien = ($bien / $total) * 100;
            $mal = $checklist->where('ncFlag', 1)->count();

            $model->update([
                'fechaFinal' => Carbon::now()->format('Y-m-d'),
                'totalMal' => $mal,
                'totalBien' => $bien,
                'porcentajeBien' => $porcentajeBien,
                'estado' => 'cerrado'
            ]);

            if($tipo == 'cambiocatalogo') {
                $checklist = $this->aplicaciones[$tipo]['checklist'];
                $piezasFinal = $checklist::where($this->aplicaciones[$tipo]['foranea_checklist'], $id)->orderBy('id','DESC')->first();
                $piezasTotales = $checklist::where($this->aplicaciones[$tipo]['foranea_checklist'], $id)
                    ->where('descripcion', 'like', '%Numero de Piezas%')
                    ->where('fase', 'like', '%FINICIO%')
                    ->first();
                $ritmoTrabajo = $checklist::where($this->aplicaciones[$tipo]['foranea_checklist'], $id)
                    ->where('descripcion', 'like', '%Ritmo de Producción%')
                    ->where('fase', 'like', '%FINICIO%')
                    ->first();
                $piezasRealizadas = intval($piezasFinal->valor) - intval($piezasTotales->valor);

                $fechaInicio = Carbon::parse($model->fechaInicio);
                $fechaFinal= Carbon::parse($model->fechaFinal);
                $tiempoConsumido = (Carbon::parse($fechaInicio)->diffInSeconds($fechaFinal));

                $piezasEsperadas = $tiempoConsumido / floatval($ritmoTrabajo->valor);
                $piezasPerdidas = $piezasEsperadas - $piezasRealizadas;
                $model->update([
                    'piezasRealizadas' => $piezasRealizadas,
                    'piezasEsperadas' => $piezasEsperadas,
                    'piezasPerdidas' => $piezasPerdidas,
                    'tiempoConsumido' => $tiempoConsumido,
                ]);

                $ritmoReal = round($model->ritmoReal * 100) / 100;
            }
            DB::commit();
            return response($model, 200);

        } catch (\Exception $e) {
            DB::rollBack();
            throw ($e);
        }
    }

    public function sendFoto(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            if ($request->has('data')) {
                $data = $request->data;
                list($type, $data) = explode(';', $data);
                list(, $data) = explode(',', $data);
                $data = base64_decode($data);
                $name = $request->n;
                $path = $this->storage.'/'.$request->a.'/documentos/'.$id;
//                $path = public_path('fotos' . DIRECTORY_SEPARATOR . $request->a . DIRECTORY_SEPARATOR . $id);
                if (!file_exists($path)) {
                    File::makeDirectory($path, $mode = 0777, true, true);
                }
                file_put_contents($path . DIRECTORY_SEPARATOR . $name, $data);
                DB::commit();
                return response(['message' => 'La foto se ha adjuntado correctamente'], 200);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            throw ($e);
        }
    }

    public function sendFase(Request $request, $tipo) {
        $fase = $request->fase;
        if(isset($request->alternador)) {
            $alternador = $request->alternador;
            $model = $this->aplicaciones[$tipo]['opl'];
            $alternador = substr($alternador, 0, 4);
            $model = $model::where(DB::raw('SUBSTRING(alternador, 1, 4)'), $alternador)->get('id')->toArray();
        } elseif(isset($request->resumen)) {
            $resumen = $request->alternador;
            $model = $this->aplicaciones[$tipo]['opl'];
            $resumen = substr($resumen, 0, 4);
            $model = $model::where(DB::raw('SUBSTRING(resumen, 1, 4)'), $resumen)->get('id')->toArray();
        }
        $checklist = $this->aplicaciones[$tipo]['checklist'];
        $registros = $checklist::whereIN($this->aplicaciones[$tipo]['foranea_checklist'], $model)
            ->where('fase', 'like', '%'.$fase.'%')
            ->where('tipo', 'not like', '%!%')
            ->orderBy('updated_at', 'DESC')
            ->get();

        $array_registros = [];
        $array_indice = [];
        $count = 0;
        foreach ($registros as $registro) {
            if($count == 0 || !in_array($registro->indice, array_unique($array_indice))) {
                $array_indice[] = $registro->indice;
                $array_registros[] = $registro;
            }
           $count = 1;
        }
        return response($array_registros, 200);
    }

    public function getRegistrosAplicacion(Request $request, $tipo) {
        $model = $this->aplicaciones[$tipo]['opl'];

		$obj = new $model();
		$table = $obj->getTable();
        $columns = Schema::getColumnListing($table);

        $query = $model::select('id',$this->aplicaciones[$tipo]['campotitulo'],'estado','fechaInicio as fecha');

        if($request->todos == 'false') {
        	$query->where('estado', 'cerrado');
        }
        if(isset($request->start) && isset($request->end)) {
            $query->whereBetween('fechaInicio',[$request->start,$request->end]);
        } elseif(isset($request->start) && !isset($request->end)) {
            $query->where('fechaInicio', '>=', $request->start);
        } elseif(!isset($request->start) && isset($request->end)) {
            $query->where('fechaInicio', '<=', $request->end);
        }
        if(isset($request->linea_id) && in_array($this->aplicaciones[$tipo]['campolinea'],$columns) && $request->linea_id != '') $query->where($this->aplicaciones[$tipo]['campolinea'], $request->linea_id);
        if(isset($request->zona_id) && in_array($request->zona_id,$columns) && $request->zona_id != '') $query->where('zona_id', $request->zona_id);

        $aplicaciones = $query->get();

        foreach ($aplicaciones as $registro) {
            $registro->titulo = isset($registro->alternador) ? $registro->alternador : $registro->resumen;
            $registro->_color = $registro->estado === 'cerrado' ? '#28a745' : '#dc3545'; //verde : rojo
            if($registro->estado === 'cerrado') {
                $incluido = true;
                $nc = 0;
                foreach ($registro->registros as $reg) {

                    if($reg->ncFlag == 1) $nc = 1;

                    if(in_array($reg->tipo, ['H','L']) && $reg->valormanual === $reg->valor) {

                        $incluido = false;
                        foreach (explode(';', $reg->opciones) as $item) {
                            if (isset(explode(':', $item)[1]) && explode(':', $item)[1] === $reg->valor) $incluido = true;
                        }
                    } else {
                        continue;
                    }
                }
                if (!$incluido) $registro->_color = '#33BEFF'; //Azul si tiene valor manual

                if($nc == 1) $registro->_color = '#FFCA33'; // naranja Si tiene no conformidad

            }
            if(isset($registro->alternador)) unset($registro->alternador);
            if(isset($registro->resumen)) unset($registro->resumen);
            if(isset($registro->estado)) unset($registro->estado);
            if(isset($registro->registros)) unset($registro->registros);
        }

        return $aplicaciones;
    }

    public function getRegistroAplicacion($tipo, $id) {
        if(!isset($this->aplicaciones[$tipo])) return response(['message' => 'El tipo de aplicacion enviado no es correcto'], 422);
        $model = $this->aplicaciones[$tipo]['opl'];
        $model = $model::query();

        $aplicacion = $model->findOrFail($id);

        $aplicacion->registros;
        $type_array = ['H','B','O','A','barcode'];
        foreach ($aplicacion->registros as $registro) {
            if ($registro->tipo == 'select') {
                $opciones = [];
                foreach (explode(';', $registro->opciones) as $op) {
                    if ($op === "") continue;
                    $trozos = explode(':', $op);
                    $opciones[] = [
                        'value' => $trozos[0],
                        'text' => $trozos[1]
                    ];
                }
                $registro->opciones = $opciones;
            } elseif(in_array($registro->tipo,$type_array)) {
                $opciones = [];
                foreach (explode(';', $registro->opciones) as $op) {
                    if ($op === "") continue;
                    $trozos = explode(':', $op);
                    $opciones[] = $trozos[1];
                }
                $registro->opciones = $opciones;
            }
        }        

        if(isset($aplicacion->alternador)) {
            $aplicacion->resumen = $aplicacion->alternador;
            unset($aplicacion->alternador);
        }

        return response($aplicacion);
    }

    public function guardarMultiplesRegistros(Request $request, $tipo)
    {
        //Compara que no se recibe un array asociativo
        if (array_keys($request->all()) !== range(0, count($request->all()) - 1)) abort(422, 'Los registros han de ir como array en el cuerpo');

        try {
            DB::beginTransaction();

            $checklist = $this->aplicaciones[$tipo]['checklist'];

            $arr_checklist = [];

            foreach ($request->all() as $registro) {

                if (!is_array($registro)) return response('Los datos enviados no son formato json', 422);

                if (!isset($registro['id']) || empty($registro['id'])) return response( 'No se han encontrado algunos de los registros', 422);

                $checklist = $checklist::findOrFail($registro['id']);
                $checklist->update([
                    'valor' => $registro['valor'], 'foto' => $registro['foto'], 'fotoInstrucciones' => $registro['fotoInstrucciones'],
                    'comentario' => $registro['comentario'], 'instrucciones' => $registro['instrucciones'],
                    'validador' => $registro['validador'], 'valormanual' => $registro['valormanual'], 'ncFlag' => $registro['ncFlag'], 'horaEntrada' => $registro['horaEntrada']
                ]);
                if(isset($registro['frecuencia'])) {
                    $checklist->frecuencia = $registro['frecuencia'];
                    $checklist->save();
                }
                $arr_checklist[] = $checklist;
            }

            DB::commit();
            return response($arr_checklist, 200);

        } catch (\Exception $e) {
            DB::rollBack();
            throw ($e);
        }
    }

    public function meses()
    {

        return [
            [
                "color" => "yellow",
                "weeks" => 5
            ],
            [
                "color" => "purple",
                "weeks" => 4
            ],
            [
                "color" => "green",
                "weeks" => 4
            ],
            [
                "color" => "pink",
                "weeks" => 5
            ],
            [
                "color" => "lightblue",
                "weeks" => 4
            ],
            [
                "color" => "lightgreen",
                "weeks" => 4
            ],
            [
                "color" => "red",
                "weeks" => 5
            ],
            [
                "color" => "gray",
                "weeks" => 5
            ],
            [
                "color" => "darkpink",
                "weeks" => 4
            ],
            [
                "color" => "blue",
                "weeks" => 4
            ],
            [
                "color" => "darkblue",
                "weeks" => 4
            ],
            [
                "color" => "brown",
                "weeks" => 4
            ]

        ];


    }

    private function ejecutar($aplicacion,$registro,$reg_op, $nc = null) {

       $arr = array();
       switch (explode(':',explode(';', $reg_op)[1])[1]) {
           case 'laboratorio':                                                              //SI AL EJECUTAR, SE QUIEREN ENVIAR PIEZAS AL LABORATORIO
               $arr[] = [
                   'tipo' => 'N',
                   'nombre' => 'linea_id',
                   'valor' => $aplicacion->linea_id,
                   'visible' =>  false,
               ];
               $arr[] = [
                   'tipo' => 'N',
                   'nombre' => 'zona_id',
                   'valor' => $aplicacion->zona_id,
                   'visible' =>  false,
               ];

               if(isset($nc) && strpos($nc,':')) {
                   if(strpos($nc,';')) {
                       $arr_explode = explode(';', $nc);
                       for($i = 0; $i < count($arr_explode); $i++) {
                           if($arr_explode[$i] == '') continue;
                           $key = explode(':',$arr_explode[$i])[0];
                           $value = explode(':',$arr_explode[$i])[1];

                           $array_nc[] = [
                               'comparacion' => $key,
                                'valor' => $value,
                           ];
                       }
                   }
                    if(isset($array_nc[0])) {
                        $arr[] = [
                            'tipo' => 'N',
                            'nombre' => 'numero_piezas',
                            'valor' => $registro['valor'],
                            'visible' =>  true,
                            'crearNC' => $array_nc[0],
                        ];
                    }
               } else {
                   $arr[] = [
                       'tipo' => 'N',
                       'nombre' => 'numero_piezas',
                       'valor' => $registro['valor'],
                       'visible' =>  true,
                   ];
               }

               $back = ($_SERVER['SERVER_NAME'] == 'localhost') ? 'evaluacionProcesos' : '_backend';

               $arr[] = [
                   'tipo' => 'configuracionEjecutar',
                   'metodo' => 'get',
                   'url' =>  'http://'.$_SERVER['SERVER_NAME'].'/'.$back.'/public/index.php/api/ejecutar/'.explode(':',explode(';', $reg_op)[1])[1],
                   'boton' => 'Mandar piezas',
                   'visible' =>  true,
               ];
               break;
           default:
               break;
       }

       return $arr;
    }

    private function crearNC($registro) {


        $aux_explode = explode(':', $registro['noconformidad']);

        $arr_aux = [];
        for($i = 1; $i<count($aux_explode);$i++) {
            $arr_aux[] = (int)$aux_explode[$i];
        }

        $crearNC = [
            'comparacion' => $aux_explode[0],
            'valor' => $arr_aux,
        ];

        return $crearNC;
    }

    private function saltarPregunta($registro) {
        $aux_explode = explode(':', $registro['condicional']);

        $arr_aux = [];
        for($i = 2; $i<count($aux_explode);$i++) {
            $arr_aux[] = (int)$aux_explode[$i];
        }

        $saltarPregunta = [
            'indice' => $aux_explode[0],
            'comparacion' => $aux_explode[1],
            'valor' => $arr_aux,
        ];

        return $saltarPregunta;
    }

    private function getTabla($tipo,$aplicacion, $frecuencia) {
        $array = [];
        $resumen = str_replace(' ','_',$aplicacion->resumen);
        $proceso = $this->aplicaciones[$tipo]['proceso'];
        $query = $proceso::query();
        $foreanea = $this->aplicaciones[$tipo]['foreanea_proceso'];
        $query->where('id',$aplicacion->$foreanea);
        $process = $query->first();
        $nombre_excel = explode('.', $process->resumen)[1];
        $nombre_excel = str_replace(' ','_', $nombre_excel);
        $carpeta = explode('.', $process->resumen)[0];
        $ruta = $carpeta . '/' . strtolower($nombre_excel);
        $dir = $this->aplicaciones[$tipo]['carpeta_procesos'] . '/' . $ruta  . '.xlsx';
        if(!is_file($dir)) return $array;

        $inputFileType = PHPExcel_IOFactory::identify($dir);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $this->objPHPExcel = $objReader->load($dir); // Empty Sheet

        $nhojas = $this->objPHPExcel->getSheetCount();
        $ultimahoja = $nhojas - 1;
        if($ultimahoja < 1) return $array;

        $this->objPHPExcel->setActiveSheetIndex(1);
        $high = $this->objPHPExcel->getActiveSheet(1)->getHighestDataRow();

        for($i=3;$i<$high;$i++) { //SUSTITUIR POR WHILE
            $respuesta = strtolower($this->getCelda('B'.$i));
            $referencia = $this->getCelda('C'.$i);
            $limite1 = $this->getCelda('D'.$i);
            $limite2 = $this->getCelda('E'.$i);
            $decimales = $this->getCelda('F'.$i);


            $arr = [
                'referencia' => $referencia,
                'crearNC' => [
                    'comparacion' => '!entre',
                    'valor' => [floatval(str_replace(',','.',$limite1)),$limite2],
                ],
                'decimales' => $decimales,
            ];
            $array[$respuesta][] = $arr;
        }
        return $array;
    }

    private function getCelda($celda){
        $code = $this->objPHPExcel->getActiveSheet()->getCell($celda)->getCalculatedValue();
        if(strstr($code,'=')==true)    $code = $this->objPHPExcel->getActiveSheet()->getCell($celda)->getCalculatedValue();
        return $code;

    }
}
