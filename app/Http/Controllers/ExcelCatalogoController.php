<?php

namespace App\Http\Controllers;

use App\CambioCatalogo;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use PHPExcel_IOFactory;
//incluir la libreria de manejo de SAP
use App\Library\SAP;
//incluir la libreria de manejo de EXCEL
use App\Library\officeClass;
use function GuzzleHttp\Psr7\str;


class ExcelCatalogoController extends AppController
{
    public $objPHPExcel;


    public function getJson($alternador, $linea)
    {
        foreach (scandir($this->storage.'/cambiocatalogo/plantillas') as $archivo) {

            if(!strpos($archivo,$linea->resumen)) continue;

            if(strpos(str_replace('_','',$archivo),substr($alternador,0,4))) {
                $dir = $this->storage.'/cambiocatalogo/plantillas/'.$archivo;
            }
        }
        if(!isset($dir)) {return json_encode(["message" => "No existe una plantilla para ese alternador", 422]);}
        $this->getExcel($dir);


        $fases = array();
        $datos = array();
        $date = Carbon::now();
        $date = $date->format('Ymdhis');
        $argumento = str_replace(' ', '', $alternador) . '.' . $date;

        $materiales = $this->getListaMateriales($alternador);
        $instrucciones = $this->getListaInstrucciones($alternador);
//        $instrucciones = mb_convert_encoding($instrucciones,'ISO-8859-1');

        if($materiales == null) {
            return json_encode(["message" => "No existe la lista de materiales", 500]);
        }
        if($instrucciones == null) {
            return json_encode(["message" => "No existe la lista de instrucciones", 500]);
        }

        for ($i = 12; $i < 500; $i++) {
            $result = $this->getRow($i, $argumento, $materiales,$instrucciones);

            if ($result) {
                $datos[] = $result;
            }

            $fase = $result['fase'];
            if (isset($fases[$fase])) {
                $fases[$fase]++;
            } else {
                $fases[$fase] = 1;
            }
        }
        $fases = array_keys($fases);
        return ["entidad" => ["id" => explode('.',$alternador)[0], "fases" => $fases, "registros" => $datos]];
    }

    public function getExcel($alternador)
    {
        $inputFileType = PHPExcel_IOFactory::identify($alternador);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $this->objPHPExcel = $objReader->load($alternador); // Empty Sheet
        $this->objPHPExcel->setActiveSheetIndex(0);
    }

    public function getRow($i, $argumento, $materiales,$instrucciones)
    {
        $fase = $this->getCelda('B' . $i);
        if (strlen($fase) > 0) {
            $descripcion = mb_substr($this->getCelda('C' . $i), 0, 254);
            $operacion = mb_substr($this->getCelda('D' . $i), 0, 254);

            $condicional = $this->condicionalExcel($this->getCelda('G'.$i));

            $fotoInstrucciones = $this->getCelda('H'.$i) == '' ? $this->getCelda('H'.$i) : $_SERVER['HTTP_HOST'].'/_storage/fotos_plantillas/'.$this->getCelda('H'.$i);

            $comentario = '';

            if ($this->getCelda('F' . $i) != "") { //respuesta
                $respuesta = $this->getCelda('F' . $i);

            } else {
                $respuesta = "ETIQUETA";
                file_put_contents("respuestastipo.txt",$i,FILE_APPEND);
            }
            $valor = "";

            switch (strtoupper($respuesta)) {
                case "ID":                                //OK
                    $tipo = "!"; //checkeado
                    $validador = "";
                    $opciones = [];
                    $requerido = true;
                    $valor = $argumento;
                    break;
                case "LINEA":
                    $tipo = "!LI"; //checkeado
                    $validador = "";
                    $opciones = [];
                    $requerido = true;
                    $valor = '';
                    break;
                case "AHORA":								//OK
                    $tipo ="!A"; //checkeado
                    $validador="";
                    $opciones ="";
                    $requerido = true;
                    $valor = Carbon::now()->format('Y-m-d H:i:s');
                    break;
                case "HORA":								//OK
                    $tipo ="HORA"; //checkeado
                    $validador="";
                    $opciones ="";
                    $requerido = true;
                    $valor = Carbon::now()->format('H:i:s');
                    break;
                case "OK": //OK
                    $tipo ="O"; //checkeado
                    $validador="OK";
                    $opciones =[];
                    $requerido = true;
                    break;
                case "SI": //SI
                    $tipo="B"; //booleano
                    $validador="SI";
                    $opciones =[];
                    $requerido = true;
                    break;
                case "TEXTO":                            //TEXTO
                    $tipo = "T"; //texto
                    $validador = "";
                    $opciones = [];
                    $requerido = false;
                    break;
                case "TEXTO OBLIGATORIO":                //NUMERO OBLIGATORIO
                    $tipo = "N"; //numero
                    $validador = "";
                    $opciones = [];
                    $requerido = true;
                    break;
                case "NUMERO":                            //NUMERO
                    $tipo = "N"; //numero
                    $validador = "";
                    $opciones = [];
                    $requerido = false;
                    break;
                case "NUMERO OBLIGATORIO":                //NUMERO OBLIGATORIO
                    $tipo = "N"; //numero
                    $validador = "";
                    $opciones = [];
                    $requerido = true;
                    break;
                case "FOTO":
                    $tipo = "F"; //foto
                    $validador = "";
                    $opciones = [];
                    $requerido = true;
                    break;
                case "IMAGEN":
                    $tipo = "I"; //imagen
                    $validador = "";
                    $opciones = [];
                    $requerido = true;
                    break;
                case "ETIQUETA":
//                    $tipo = "E"; //etiqueta
                    $tipo = "T"; //etiqueta
                    $validador = "";
                    $opciones = [];
                    $requerido = false;
                    break;
                case "VALORACION5":
                    $tipo = "S"; //etiqueta
                    $validador = "";
                    $opciones = [];
                    $requerido = false;
                    break;
                case "QR":
                    $tipo = "QR"; //qr
                    $validador="";
                    $opciones =[];
                    $requerido = false;
                    break;
                default:
                    if (preg_match('/BARCODE:/',$respuesta) >= 1){
                        $tipo = "barcode"; //barcode
                        $validador="";
                        $opciones =$this->getBarcode($respuesta);
                        $requerido = false;
                    }
                    elseif (substr($respuesta, 0, 1) == "@") {    //*cargar...
                        $tipo = "A"; //ACCION
                        $validador = "";
                        $comando = explode(":", trim(substr($respuesta, 1)));
                        $opciones = [$comando[0],$comando[1]];
                        $requerido = false;

                    } elseif (substr($respuesta, 0, 1) == "*") {    //*060.0  // *A3
                        $tipo = "H"; //LISTA
                        $datovalor =  substr($respuesta, 1);
                        if (substr($respuesta, 0, 2) == "**") { //**060.0  // **A3
                            $tipo = "O";
                            $datovalor =  substr($respuesta, 2);
                        }

                        $validador = "UNICO";
                        $opciones = [];
                        if (is_numeric($datovalor)) {    //*060.0
                            foreach ($materiales as $material) {
                                if (substr($material["id"], 0, 4) == substr($respuesta, 1, 4)) {
                                    $opciones[] = $material['ref'] == '--' ? 'NO' : $material["ref"];
                                }
                            }
                        }else{// *A3
                            $datovalor=strtoupper($datovalor);
                            if(isset($instrucciones[$datovalor])) {
                                if($instrucciones[$datovalor] == '') {
                                    $opciones = ['-'];
                                } else {
                                    if($instrucciones[$datovalor] == '--') {
                                        $opciones[] = 'NO';
                                    } else {
                                        $opciones = explode(",", $instrucciones[$datovalor]);
                                    }
                                }
                            } else {
                                $opciones = ['-'];
                            }
                        }
                        $opciones[] = "*";
                        $requerido = true;
                    } else {
                        if (strpos($respuesta, ";") > 0) {
                            $tipo = "select"; //lista
                            $validador = "LISTA";
                            $respuesta = str_replace('?', '', $respuesta);
                            $respuesta = explode(';', $respuesta);
                            $item = array();
                            foreach ($respuesta as $res) {
                                if (strpos($res, '#')) {
                                    $a = explode('#', $res);
                                    $item[$a[0]] = $a[1];
                                } else {
                                    $item[] = $res;
                                }

                            }
                            $opciones = $item;
                            $requerido = true;
                        } else {                            //F00M990180
                            $tipo = "T"; //lista
                            $validador = "UNICO";
                            $opciones = [trim($respuesta)];
                            $requerido = true;
                        }


                    }
            }
            file_put_contents("ultimojson.txt", print_r(["indice" => $i - 11, "valor" => $valor, "fase" => "F" . $fase, "descripcion" => $descripcion, "operacion" => $operacion,
                "comentario" => $comentario, "tipo" => $tipo,
                "validador" => $validador, "opciones" => $opciones, "requerido" => $requerido],1),FILE_APPEND);
            return ["indice" => $i - 11, "valor" => $valor, "fase" => "F" . $fase, "descripcion" => $descripcion, "operacion" => $operacion,
                "comentario" => $comentario, "tipo" => $tipo,"condicional"=>$condicional,
                "validador" => $validador, "opciones" => $opciones, "requerido" => $requerido,/* "variable"=>$variable*/
                'fotoInstrucciones' => $fotoInstrucciones];
        }


        return false;

    }

    public function getCelda($celda)
    {
        $code = $this->objPHPExcel->getActiveSheet()->getCell($celda)->getCalculatedValue();
        if (strstr($code, '=') == true) $code = $this->objPHPExcel->getActiveSheet()->getCell($celda)->getCalculatedValue();
        return $code;

    }

    public function getListaMateriales($alternador){
        $data=[];
        foreach (scandir($this->storage.'/cambiocatalogo/documentos/') as $fichero) {
            if($fichero == $alternador) {
                $materiales = $this->storage.'/cambiocatalogo/documentos/'.$alternador.'/'.explode('.',$alternador)[0].'_LISTA.TXT';
            }
        }

        if(isset($materiales)) {
            if (file_exists($materiales)) {
                $content = file($materiales);
                $cabecera = false;
                $resultados = false;
                foreach ($content as $linea) {
                    if ($resultados) {
                        $linea = str_replace(chr(9), "*", $linea);
                        $linea = str_replace("****", "*", $linea);
                        $linea = str_replace("***", "*", $linea);
                        $linea = str_replace("**", "*", $linea);
                        $linea = str_replace("*", "*", $linea);

                        //$data[] = ["id"=>trim(substr($linea,0,5)),"cantidad"=>trim(substr($linea,5,10)),"cantidad"=>trim(substr($linea,5,10))];
                        $datos = explode("*", $linea);
                        if (substr($datos[0], 3, 1) == ".") {
                            if (substr($datos[0], 0, 1) < 9) {
                                $data[] = ["id" => $datos[0], "ref" => $datos[4]];
                            } else {
                                $data[] = ["id" => $datos[0], "ref" => $datos[1]];
                            }
                        }
                    }
                    if ($cabecera == true) {
                        $resultados = true;
                    }
                    if (substr($linea, 0, 5) == "Pos.F") {
                        $cabecera = true;
                    };
                }
                return $data;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getListaInstrucciones($alternador){
        file_put_contents("ultimalistainstrucciones.txt",getcwd().$this->storage.'/cambiocatalogo/documentos/'.$alternador.'/');

        if(file_exists($this->storage.'/cambiocatalogo/documentos/'.$alternador.'/'.explode('.',$alternador)[0].'_INSTRUCCIONES.PHP')){
            $data = $this->storage.'/cambiocatalogo/documentos/'.$alternador.'/'.explode('.',$alternador)[0].'_INSTRUCCIONES.PHP';
            $data = require($data);
            file_put_contents($this->storage.'/cambiocatalogo/documentos/'."ultimalistainstrucciones.txt",print_r($data,1));
            return $data;
        }else{
            file_put_contents("ultimalistainstrucciones.txt","no existe ".getcwd().$this->storage.'/cambiocatalogo/documentos/'.$alternador.'/'.explode('.',$alternador)[0].'_INSTRUCCIONES.php');
            //echo $dirDoc.$catalogoLote.DIRECTORY_SEPARATOR.$alternador."_INSTRUCCIONES.PHP";
            return false;
        }
    }
}
