<?php

namespace App\Http\Controllers;

use App\Permiso;
use Illuminate\Http\Request;

class PermisoController extends AppController
{
    public function index(Request $request) {

        $query = Permiso::query();
        $model = new Permiso();

        $query = $this->absoluteFilter($request,$model,$query);

        $return = $query->get();

        return response($return, 200);
    }

    public function store(Request $request)
    {
        return response(['message' => 'No se pueden crear permisos'], 403);
    }

    public function show($id) {
        $query = Permiso::findOrFail($id);

        return response($query,200);
    }

    public function update(Request $request, $id) {
        return response(['message' => 'No se pueden modificar permisos'], 403);
    }

    public function destroy($id) {
        return response(['message' => 'No se pueden eliminar permisos'], 403);
    }
}
