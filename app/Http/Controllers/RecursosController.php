<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

use App\Line;
use App\Empleados;
use App\Cargos;
use App\Procesos;
use App\Procesoscalidad;
use App\User;
use App\Maquina;
use App\Manodeobra;
use App\Zona;

class RecursosController extends Controller
{
    public function enviarRecursos(Request $request) {
            $recursos = $request->recursos;
            $array_recursos = array();
            foreach ($recursos as $recurso) {
                $recurso = json_decode($recurso);
                $model = 'App\\'.ucwords($recurso->recurso);
                $allmodels = ($this->getModels());
                if(array_search($model,$allmodels) === false) {
                    $model .= 's';
                    if(array_search($model,$allmodels) === false) {
                        return response(['message' => 'El modelo '.$model.' no existe'], 422);
                    }
                }
                $query = $model::query();
                $columns = Schema::getColumnListing((new $model())->getTable());
                if(isset($recurso->filtros)) {
                    foreach ($recurso->filtros as $key=>$value) {
                        if(array_search($key, $columns)) {
                            $query = $query->where($key,$value);
                        }
                    }
                }
                $array_recursos[strtolower(substr($model,4))] = $query->get()->toArray();
            }
            return response($array_recursos);
    }

    public function getModels(){
        $path = app_path();
        $out = [];
        $results = scandir($path);
        foreach ($results as $result) {
            if ($result === '.' or $result === '..') continue;
            $filename = $path . '/' . $result;
            if (!is_dir($filename)) {
                $filemodel = substr($filename,0,-4);
                $filemodel = explode('app/',$filemodel);
                $out[] = 'App\\'.$filemodel[1];
            }
        }
        return $out;
    }
}
