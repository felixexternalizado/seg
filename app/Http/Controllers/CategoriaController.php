<?php

namespace App\Http\Controllers;

use App\Aplicacion;
use App\Categoria;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Categoria::query();

        if(isset($request->sortBy) && !empty($request->sortBy)) {

            if($request->sortDesc) $query->orderBy($request->sortBy,'ASC');
            else $query->orderBy($request->sortBy,'DESC');
        } else {
            $query->orderBy('orden','ASC');
        }

        return response($query->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => ['required','string'],
            'icon' => ['string'],
            'color' => ['string'],
            'orden' => ['numeric'],
        ]);

        $color = $request->color === '' ? '#005286' : $request->color;

        $categoria = Categoria::create([
            'title' => $request->title,
            'icon' => $request->icon,
            'color' => $color,
            'orden' => $request->orden,
        ]);

        return response ($categoria);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response (Categoria::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categoria = Categoria::findOrFail($id);

        $categoria->update($request->all());

        return response ($categoria);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $aplicacion = Aplicacion::where('categoria_id', $id)->get();
        if(isset($aplicacion) && !$aplicacion->isEmpty()) {
            return response(['message' => 'No puede eliminarse la categoria, hay aplicaciones que dependen de ella.'], 422);
        }

        Categoria::findOrFail($id)->delete();

        return response(['message' => 'La categoría ha sido eliminada'], 200);
    }
}
