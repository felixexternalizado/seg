<?php

namespace App\Http\Controllers;

use App\Cargos;
use App\Cargoslineasempleados;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;

class CargosController extends AppController
{
    public function index(Request $request) {

        $query = Cargos::query();
        $model = new Cargos();

        $query = $this->absoluteFilter($request,$model,$query);

        $return = $query->get();

        return response($return, 200);
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $this->validate($request, [
                'resumen' => ['required'],
            ]);

            $cargo = Cargos::create([
                'resumen' => $request->resumen,
            ]);
            DB::commit();
            return response($cargo);

        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function show($id)
    {
        try {
            DB::beginTransaction();

            $cargos = Cargos::where('id', $id)->first();
            if (!$cargos) return response(['message' => 'No existe la linea solicitada'], 404);

            DB::commit();
            return $cargos;
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            $cargos = Cargos::where('id', $id)->first();
            if (!$cargos) return response(['message' => 'No existe el recurso solicitado'], 404);

            $validator = Validator::make($request->all(), [
                'resumen' => ['required'],
            ]);
            if ($validator->fails()) {
                return response($validator->errors());
            }

            $cargos->resumen = $request->resumen;
            $cargos->save();

            DB::commit();
            return response($cargos);

        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            if ($id <= 0 || $id == null) return response(['message' => 'No existe el recurso solicitado'], 404);
            $cargoslineasempl = Cargoslineasempleados::where('cargos_id', $id)->get();
            if(isset($cargoslineasempl) && !$cargoslineasempl->isEmpty()) {
                return response(['message' => 'No puede eliminarse el cargo, pertenece a una o varias lineas y tiene un empleados asociados'], 422);
            }
            Cargos::where('id', $id)->delete();
            DB::commit();
            return response(['message' => 'El registro se ha eliminado.']);
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }
}
