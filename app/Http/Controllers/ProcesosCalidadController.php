<?php

namespace App\Http\Controllers;

use App\Procesoscalidad;
use App\Registrocalidad;
use App\Line;
use App\Zona;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProcesosCalidadController extends AppController
{
    public function index(Request $request)
    {
        $query = Procesoscalidad::query();

        $model = new Procesoscalidad();

        $query = $this->absoluteFilter($request,$model,$query);

        if(isset($request->desactivados) && $request->desactivados == 'false') {
            $query->where('activo', 1);
        }

        $result = $query->with('linea','zona')->get()->toArray();
        
        $respuesta = [];
        foreach($result as $r) {
        	if( $r['linea'] === null) {
        		$r['linea'] = new Line();
        		$r['linea']->resumen = '';
        	}
        	if($r['zona'] === null)  {
        		$r['zona'] = new Zona();
        		$r['zona']->resumen = '';
        	}
        	$respuesta[] = $r;
        }
        return response($respuesta);
    }

    public function store(Request $request) {
        try {
            DB::beginTransaction();

            $this->validate($request, [
                'resumen' => ['required', 'string', 'max:255'],
                'linea_id' => ['required', 'exists:lineas,id'],
                'zona_id' => ['required', 'exists:zonasdelinea,id'],
            ]);

            $aux = explode('.',$request->resumen);
            if(!isset($aux[1])) return response(['message' =>'El nombre del proceso debe tener el formato "carpeta.nombreproceso"'], 422);

            $proceso = Procesoscalidad::create([
                'resumen' => $request->resumen,
                'linea_id' => $request->linea_id,
                'zona_id' => $request->zona_id,
            ]);
            DB::commit();
            return response($proceso);

        } catch(\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function show($id)
    {
        try {
            DB::beginTransaction();

            $proceso = Procesoscalidad::where('id', $id)->first();
            if (!$proceso) return response(['message' => 'No existe el recurso solicitado'], 404);

            DB::commit();
            return response($proceso);
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function update(Request $request, $id) {

        $proceso = Procesoscalidad::findOrFail($id);

        $proceso->update([
            'resumen' => $request->resumen,
            'linea_id' => $request->linea_id,
            'zona_id' => $request->zona_id,
        ]);

        $aux = explode('.',$request->resumen);
        if(!isset($aux[1])) return response('El nombre del proceso debe tener el formato "carpeta.nombreproceso"', 422);

        return response($proceso);

    }

    public function destroy($id) {
        try {
            DB::beginTransaction();
            if($id <= 0 || $id == null) return response(['message' => 'No existe el recurso solicitado'], 404);
            $regcal = Registrocalidad::where('procesocalidad_id', $id)->where('estado', '!=', 'cerrado')->get();

            if(isset($regcal) && !$regcal->isEmpty()) {
                return response(['message' => 'No puede eliminarse el proceso de calidad, esta asociado a varios registros de calidad que no han sido cerrados'], 422);
            }

            $procesocal = Procesoscalidad::findOrFail($id);
            $procesocal->activo == 1 ? $procesocal->activo = 0 : $procesocal->activo = 1;
            $procesocal->save();

            DB::commit();
            return response(['message' => 'El proceso de calidad ha sido desactivado'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }
}
