<?php

namespace App\Http\Controllers;

use App\Aplicacion;
use App\AplicacionUser;
use App\Categoria;
use App\Empleados;
use App\Permiso;
use App\PermisoUser;
use App\Zona;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $query = User::query();
        if(isset($request->nombre)) {
            $query = $query->where('name', 'like', '%'.$request->nombre.'%');
        }
        if(isset($request->email)) {
            $query = $query->where('email', '%'.$request->email.'%');
        }
        if(isset($request->empleados_id)) {
            $query = $query->where('empleados_id', $request->empleados_id);
        }

        //Para los administradores de Llood
        if(!strpos(auth()->user()->email,'@externalizado.com')) $query->where('email', 'not like', '%@externalizado.com');

        return response($query->with('empleado')->orderBy('name', 'ASC')->get());

    }

    public function store(Request $request)
    {
        try {

            DB::beginTransaction();

            $validate = [
                'name' => ['required', 'string', 'max:255'],
                'password' => ['required', 'min:8', 'confirmed'],
                'email' => ['required', 'email'],
                'empleados_id' => ['required', 'exists:empleados,id'],
            ];
            $respuestas = [
                'name.required' => 'El campo Nombre es requerido.',
                'name.string' => 'El campo Nombre debe ser una cadena de texto.',
                'name.max' => 'El campo Nombre no puede superar los 255 carcteres.',
                'password.required' => 'El campo Contraseña es requerido.',
                'password.min' => 'El campo Contraseña debe tener 8 carcteres como mnimo.',
                'password.confirmed' => 'La contraseña introducida no coincide.',
                'email.required' => 'El campo email es requerido.',
                'email.unique' => 'Ya existe un usuario con ese Email.',
                'email.email' => 'El campo email no tiene el formato correcto.',
                'empleados_id.required' => 'El campo Empleado es requerido.',
                'empleados_id.exists' => 'El empleado seleccionado no existe.',
            ];
            $this->validate($request, $validate, $respuestas);
            if (isset($request['admin'])) {
                $admin = $request['admin'];
            } else {
                $admin = 0;
            }

            $password = Hash::make($request->password);

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => $password,
                'admin' => $admin,
                'empleados_id' => $request->empleados_id,
                'creator_id' => auth()->user()->id,
            ]);

            if(isset($request->zona_id)) {
                $empleado = Empleados::findOrFail($request->empleados_id);
                $empleado->default_zona_id = $request->zona_id;
                $empleado->save();
            }

            if (!$user) return response(['message' => 'Error'], 500);

            $client = new Client([
                'base_uri' => config('app.dms_url'),
                'header' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ]);
            $result = $client->post('appusers', [
                'form_params' => [
                    'name' => $request->name,
                    'password' => $password,
                    'password_confirmation' => $password,
                    'email' => $request->email,
                    'apptoken' => config('app.dms_token'),
                ]
            ]);

            DB::commit();

            return response($user);

        } catch (\Exception $e) {
            DB::rollback();
            throw ($e);
        }
    }

    public function show($id)
    {
        $user = User::find($id);

        if(!$user) return response(['message' => 'No existe el usuario solicitado'], 404);

        return $user;

    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'nombre' => ['string', 'max:255'],
            'empleados_id' => ['exists:empleados,id'],
        ]);

        $user = User::findOrFail($id);
        $olduser = json_encode($user->toArray());

        if(isset($request['admin'])) {
            $admin = $request['admin'];
        } else {
            $admin = $user->admin;
        }

        $request->admin = $admin;

        if(!$user) return response(['message' => 'No existe el usuario solicitado'], 404);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->empleados_id = $request->empleados_id;
        $user->admin = $admin;
        $user->aplicaciones = $request->aplicaciones;
        $user->save();

//        $user->update($request->all());

        $client = new Client([
            'base_uri' => config('app.dms_url'),
            'header' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ]
        ]);
        $result = $client->post('appusers/'.$user->id, [
            'form_params' => [
                'olduser' => $olduser,
                'name' => $request->name,
                'email' => $request->email,
                'apptoken' => config('app.dms_token'),
            ]
        ]);

        return response($user);

    }

    public function destroy($id)
    {
        $user = User::where('creator_id', $id)->get()->toArray();
        if(isset($user) && !empty($user)) {
            return response(['message' => 'No puede eliminarse el usuario, es creador de otro usuario'], 422);
        }

        $user_id = User::findOrFail($id);
        
		$olduser = json_encode($user_id->toArray());
		
        $client = new Client([
            'base_uri' => config('app.dms_url'),
            'header' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ]
        ]);
        $result = $client->delete('appusers/'.$user_id->id, [
            'form_params' => [
                'olduser' => $olduser,
                'apptoken' => config('app.dms_token'),
            ]
        ]);

        $user_id->delete();

        return response(['message' => 'ok']);

    }

    public function auth() {
        $user = User::findOrFail(auth()->user()->id);

        $empl = Empleados::findOrFail(auth()->user()->empleados_id);

        if(isset($empl->default_zona_id) && !empty($empl->default_zona_id)) {
            $linea = Zona::findOrFail($empl->default_zona_id)->linea_id;
            $empl->linea_id = $linea;
        }

        $user->empleado = $empl;

        if(strpos($user->email, '@externalizado.com')) $user->superadmin = true;

        $cats = [];
        $apps = [];

        foreach ($user->aplicaciones()->get() as $key => $app) {

            if (!isset($cats[$app->categoria->title])) $cats[$app->categoria->title] = ['categoria' => $app->categoria, 'buttons' => []];

            $cats[$app->categoria->title]['buttons'][] = $app;

        }

        foreach ($cats as $cat) {

            $apps[] = [
                'title' => $cat['categoria']->title,
                'color' => $cat['categoria']->color,
                'icono' => $cat['categoria']->icon,
                'buttons' => $cat['buttons']
            ];

        }

        $user->aplicaciones = $apps;

        return response($user);

        $permisosuser = PermisoUser::where('user_id', auth()->user()->id)->get('permiso_id')->toArray();
        $permisos = Permiso::whereIn('id', $permisosuser)->get()->toArray();

        $user->permisos = $permisos;

        return response($user);
    }

    public function changePassword(Request $request)
    {
        if (isset($request->user_id)) {
            $this->validate($request, [
                'user_id' => ['exists:users,id'],
                'password' => ['required', 'min:8', 'confirmed'],
            ]);

            $user = User::findOrFail($request->user_id);
        } else {
            $this->validate($request, [
                'password_old' => ['string', 'min:8'],
                'password' => ['string', 'min:8', 'confirmed', 'different:password_old'],
            ]);
            $user = User::findOrFail(auth()->user()->id);

            if (!Hash::check($request->password_old, $user->password)) {
                $arr_pass[] = 'La antigua contraseña no coincide';
                $arr['errors']['password'] = $arr_pass;
                $arr['message'] = 'The given data was invalid.';
                return response($arr, 422);
            }
        }

        $olduser = json_encode($user->toArray());

        $user->password = Hash::make($request->password);
        $user->save();

        try {
            $client = new Client([
                'base_uri' => config('app.dms_url'),
                'header' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ]);
            $result = $client->post('appusers/' . $user->id, [
                'form_params' => [
                    'olduser' => $olduser,
                    'name' => $user->name,
                    'email' => $user->email,
                    'password' => Hash::make($request->password),
                    'apptoken' => config('app.dms_token'),
                ]
            ]);
        } catch (ClientErrorResponseException $e) {
            return response($e->getResponse()->getBody(),500);
        }

        return response(['message' => 'La contraseña ha sido sustituida'], 200);
    }

    public function insertUsersDMS() {
        $client = new Client([
            'base_uri' => config('app.dms_url'),
            'header' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ]
        ]);

        foreach (User::get() as $user) {

            try {
                $result = $client->post('appusers', [
                    'form_params' => [
                        'name' => $user->name,
                        'password' => $user->password,
                        'password_confirmation' => $user->password,
                        'email' => $user->email,
                        'apptoken' => config('app.dms_token'),
                    ]
                ]);
            } catch (\Exception $e) {
            }

        }
    }

    public function cambiarAplicacion(Request $request, $id) {
        try {

            $this->validate($request, [
                'aplicacion_id' => ['required', 'exists:aplicaciones,id'],
                'permiso' => ['required'],
            ]);
            $app_user = AplicacionUser::where('user_id', $id)->where('aplicacion_id', $request->aplicacion_id)->first();

            if ($request->permiso == 'true') {
                if (!isset($app_user) || empty($app_user)) AplicacionUser::create(['aplicacion_id' => $request->aplicacion_id, 'user_id' => $id]);
            } else {
                if (isset($app_user) && !empty($app_user)) $app_user->delete();
            }

            return response(['message' => 'El permiso ha sido modificado'], 200);
        } catch (\Exception $e) {
            throw($e);
        }
    }
}
