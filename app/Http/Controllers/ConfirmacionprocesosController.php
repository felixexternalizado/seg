<?php

namespace App\Http\Controllers;

use App\Confirmacionprocesos;
use App\Dias;
use App\Empleados;
use App\ChecklistConfirmacionesprocesoRegistros;
use App\Noconformidades;
use App\Procesos;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class ConfirmacionprocesosController extends AppController
{
    public function index(Request $request)
    {
        $confprocesos = new Collection();
        $empleado_id = auth()->user()->empleados_id;
        $date = Carbon::now();

        if (isset($request->todos)) {
            //TODOS LOS MIOS QUE NO ESTAN CERRADOS
            $confprocesos = Confirmacionprocesos::where('estado', '!=', 'cerrado')
                ->where('empleados_id', $empleado_id)
                ->orderBy('resumen', 'ASC')
                ->limit(100)
                ->get();
            foreach ($confprocesos as $confirmp) {
                $confirmp->setSemanaAttribute($confirmp->fechaInicio);
            }
            return response($confprocesos);
        }

        $semana = Dias::where('dia', $date->day)->where('mes', $date->month)->where('anno', $date->year)->first();
        $week = $semana->semana;
        foreach (Confirmacionprocesos::where('estado', '!=', 'cerrado')
                     ->where('empleados_id', $empleado_id)
                     ->where(DB::raw('MONTH(fechaInicio)'), $date->month)
                     ->orderBy('resumen', 'ASC')
                     ->get() as $confirmp) {
            $confirmp->setSemanaAttribute($confirmp->fechaInicio);
            $confirmp->semana == $week ? $confprocesos->push($confirmp) : '';
        }
        return response($confprocesos);
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $this->validate($request, [
                'fecha' => ['required'], //En el planificador es el nº de la semana
                'empleado' => ['required', 'exists:empleados,id'],
                'linea' => ['required', 'exists:lineas,id'],
                'proceso' => ['required', 'exists:procesos,id']
            ]);

            $nombre_proceso = Procesos::where('id', $request->proceso)->first();

//            $array = [
//                'proceso' => $nombre_proceso,
//                'empleado' => $request->empleado,
//                'linea' => $request->linea,
//                'storage' => $this->storage.'/confirmacion_procesos',
//                'app' => 'confirmacionproceso'
//            ];

            $json = (new ExcelController())->getJson($nombre_proceso, $request->empleado, $request->linea, $this->storage);
            if (isset($json['message'])) {
                return response($json, 404);
            }

            $empleado = Empleados::where('id', $request->empleado)->first();
            //Si lo que envía es el nº de la semana
            if (strlen($request->fecha) > 0 && strlen($request->fecha) < 3) {
                if (isset($request->anno) && $request->anno >= 0) {
                    $semana = Dias::where('semana', $request->fecha)
                        ->where('anno', $request->anno)
                        ->first();
                    $actual = Carbon::now();
                    $semana_actual = Dias::where('mes', $actual->month)
                        ->where('anno', $actual->year)
                        ->where('dia', $actual->day)
                        ->first();
                    if($semana->semana == $semana_actual->semana) {
                        $date = $semana_actual->anno.'-'.$semana_actual->mes.'-'.$semana_actual->dia;
                    } else {
                        $date = $semana->anno.'-'.$semana->mes.'-'.$semana->dia;
                    }
                } else {
                    return response(['message' => 'No ha especificado un año'], 422);
                }
            } else {
                $date = explode('T', $request->fecha)[0];
            }
            $confproceso = Confirmacionprocesos::create([
                'procesos_id' => $request->proceso,
                'empleados_id' => $request->empleado,
                'resumen' => $nombre_proceso->resumen,
                'evaluador' => '',
                'fechaInicio' => $date,
                'fechaFinal' => '',
                'plantilla' => '',
                'version' => '',
                'porcentajefallopermitido' => '',
                'porcentajefalloreal' => '',
                'totalMal' => 0,
                'totalBien' => 0,
                'porcentajeBien' => 0,
                'totalPreguntas' => 0,
                'linea' => $request->linea,
                'iniciales' => $empleado->iniciales,
            ]);

            $totalPreguntas = 0;
            foreach ($json['entidad']['registros'] as $registro) {
                if (isset($registro)) {
                    $checkconf = new ChecklistConfirmacionesprocesoRegistros();
                    $checkconf->confirmacionproceso_id = $confproceso->id;
                    foreach ($registro as $key => $value) {
                        $nombre = $key;
                        if ($nombre == 'tipo' && strpos($value, '!') === false) {
                            $totalPreguntas++;
                        }
                        if (is_array($value)) {
                            if (!empty($value)) {
                                $opciones = '';
                                foreach ($value as $key => $v) {
                                    $opciones .= $key . ':' . $v . ';';
                                }
                                $checkconf->$nombre = $opciones;
                                $checkconf->save();
                            }
                        } else {
                            if($value === null) $value = '';

                            $checkconf->$nombre = $value;
                            $checkconf->save();
                        }
                    }
                }
            }
            $confproceso->totalPreguntas = $totalPreguntas;
            $confproceso->save();

            DB::commit();
            $conf_proceso = Confirmacionprocesos::where('id', $confproceso->id)->first();

            $conf_proceso->iniciales = $empleado->iniciales;
            $conf_proceso->empleado = $empleado;
            $conf_proceso->setEliminarAttribute($conf_proceso);

            return response($conf_proceso);

        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }

    }

    public function show($id)
    {
        try {
            DB::beginTransaction();

            $confirmacionproceso = Confirmacionprocesos::find($id);

            if (!$confirmacionproceso) return response(['message' => 'No existe la confirmación de proceso solicitada'], 500);

            DB::commit();
            return $confirmacionproceso;
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $confirmacionproceso = Confirmacionprocesos::where('id', $id)->first();

            $data = json_decode($request->json, true);

            if (isset($data['entidad']) && isset($data['entidad']['id'])
                && isset($data['entidad']['registros']) && $data['entidad']['fases']) {
                if (isset($data["entidad"]["finalizacion"])) {
                    $confirmacionproceso->estado = 'cerrado';
                    $confirmacionproceso->save();
                    if (isset($data['entidad']['totalMal']) && $data['entidad']['totalMal'] >= 0) {
                        $confirmacionproceso->totalMal = $data['entidad']['totalMal'];
                    }
                    if (isset($data['entidad']['totalBien']) && $data['entidad']['totalBien'] >= 0) {
                        $confirmacionproceso->totalBien = $data['entidad']['totalBien'];
                    }
                    if (isset($data['entidad']['porcentajeBien']) && $data['entidad']['porcentajeBien'] >= 0) {
                        $confirmacionproceso->porcentajeBien = $data['entidad']['porcentajeBien'];
                    }
                    if (isset($data['entidad']['totalPreguntas']) && $data['entidad']['totalPreguntas'] >= 0) {
                        $confirmacionproceso->totalPreguntas = $data['entidad']['totalPreguntas'];
                    }
                } else {
                    $confirmacionproceso->estado = 'en curso';
                    $confirmacionproceso->save();
                }
                $confirmacionproceso->update($request->all());
                DB::commit();
                return response($confirmacionproceso);
            } else {
                return response(['message' => 'La confirmación del proceso está incompleta'], 422);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }


    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $noconformidades = Noconformidades::where('aplicacion_id', $id)
                ->where(function ($query) {
                    $query->where('opl', 'confirmacionprocesos')
                        ->orWhere('opl', 'confirmacionesproceso');
                })->get()->toArray();
            if (isset($noconformidades) && !empty($noconformidades)) {
                return response(['message' => 'No puede eliminarse la confirmación del proceso, tiene No Conformidades asociadas'], 422);
            }

            $orig = Confirmacionprocesos::where('original_id', $id)->count();
            if($orig > 0) return response(['message' => 'No puede eliminarse la confirmación del proceso, otras confirmaciones de proceso se han creado a partir de esta'], 422);

            ChecklistConfirmacionesprocesoRegistros::where('confirmacionproceso_id',$id)->delete();
            Confirmacionprocesos::where('id', $id)->delete();
            DB::commit();
            return response(['message' => 'ok']);
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function repetirPlanificacion(Request $request, $confpr)
    {
        try {
            DB::beginTransaction();

            $conf_original = Confirmacionprocesos::findOrFail($confpr);
            $registros = $conf_original->registros;

            $fechaFinal = Carbon::createFromFormat('Y-m-d', $request->fechaFin); //YYYY-mm-dd
            $tipo = $request->tipo; //diariamente, semanalmente y mensualmente
            $frecuencia = $request->frecuencia; //1

            $fechaIni = Carbon::createFromFormat('Y-m-d', $conf_original->fechaInicio);

            if ($tipo == 'diariamente') {
                $dif = Carbon::parse($fechaIni)->diffInDays($fechaFinal);
                $sum = $frecuencia;
            }
            if ($tipo == 'semanalmente') {
                $dif = Carbon::parse($fechaIni)->diffInWeeks($fechaFinal);
                $sum = $frecuencia * 7;
            }
            if ($tipo == 'mensualmente') {
                $dif = Carbon::parse($fechaIni)->diffInMonths($fechaFinal);
            }

            $count = 0;

            if ($tipo == 'diariamente' || $tipo == 'semanalmente') $fecha = $fechaIni->addDays($sum);
            if ($tipo == 'mensualmente') $fecha = $fechaIni->addMonths($frecuencia);

            while ($fechaIni->lte($fechaFinal)) {

                $confproceso = Confirmacionprocesos::create([
                    'original_id' => $conf_original->id,
                    'procesos_id' => $conf_original->procesos_id,
                    'empleados_id' => $conf_original->empleados_id,
                    'resumen' => $conf_original->resumen,
                    'evaluador' => $conf_original->evaluador,
                    'fechaInicio' => $fecha->format('Y-m-d'),
                    'fechaFinal' => '',
                    'plantilla' => '',
                    'version' => '',
                    'porcentajefallopermitido' => '',
                    'porcentajefalloreal' => '',
                    'totalMal' => 0,
                    'totalBien' => 0,
                    'porcentajeBien' => 0,
                    'totalPreguntas' => $conf_original->totalPreguntas,
                    'linea' => $conf_original->linea,
                ]);

                foreach ($registros as $registro) {
                    ChecklistConfirmacionesprocesoRegistros::create([
                        'confirmacionproceso_id' => $confproceso->id,
                        'indice' => $registro->indice,
                        'valor' => $registro->valor,
                        'fase' => $registro->fase,
                        'foto' => $registro->foto,
                        'fotoInstrucciones' => $registro->fotoInstrucciones,
                        'descripcion' => $registro->descripcion,
                        'operacion' => $registro->operacion,
                        'comentario' => $registro->comentario,
                        'noconformidad' => $registro->noconformidad,
                        'limite1' => $registro->limite1,
                        'limite2' => $registro->limite2,
                        'tipo' => $registro->tipo,
                        'instrucciones' => $registro->instrucciones,
                        'validador' => $registro->validador,
                        'opciones' => $registro->opciones,
                        'condicional' => $registro->condicional,
                        'requerido' => $registro->requerido,
                        'valormanual' => $registro->valormanual,
                        'ncFlag' => $registro->ncFlag,
                        'horaEntrada' => $registro->horaEntrada,
                    ]);
                }
                $count++;

                if ($tipo == 'diariamente' || $tipo == 'semanalmente') $fecha = $fechaIni->addDays($sum);
                if ($tipo == 'mensualmente') $fecha = $fechaIni->addMonths($frecuencia);
            }

            DB::commit();

            return response(['message' => 'Se han creado ' . $count . ' planificaciones.'], 200);

        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }
}
