<?php

namespace App\Http\Controllers;

use App\LaboratorioRegistro;
use App\Noconformidades;
use App\OptimizacionCos;

class GenerateController extends Controller
{
    public function generateOPL() {
        factory(Noconformidades::class, 50)->create();

        return response('Generadas OPLs con éxito.');
    }
    public function generateLab() {
        factory(LaboratorioRegistro::class, 50)->create();

        return response('Generados LabReg con éxito.');
    }

    public function generateCOS() {
        factory(OptimizacionCos::class, 50)->create();

        return response('Generados COS con éxito.');
    }
}
