<?php

namespace App\Http\Controllers;

use App\Cargoslineasempleados;
use App\Confirmacionprocesos;
use App\Dias;
use App\Line;
use App\Manodeobra;
use App\Noconformidades;
use App\Registrocalidad;
use App\User;
use App\Zona;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Empleados;
use Illuminate\Support\Facades\DB;

class EmpleadosController extends AppController
{
    public function index(Request $request)
    {
        try {
            DB::beginTransaction();
            $query = Empleados::query();
            $model = new Empleados();

            $query = $this->absoluteFilter($request,$model,$query);

            $query->where('email', 'not like', '%@externalizado.com%');

            $result = $query->with('responsable','linea','zona')->get();

            $salida = [];
            foreach ($result->toArray() as $item) {

                $objeto = $item;

                if (!$objeto['zona']) $objeto['zona'] = [];

                if (!$objeto['linea']) $objeto['linea'] = [];

                $salida[] = $objeto;
            }

            DB::commit();
            return response($salida);
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $this->validate($request, [
                'nombre' => ['required', 'string'],
                'email' => ['required', 'email']
            ]);

            $iniciales = '';
            $txt = explode(' ', $request->nombre);
            foreach ($txt as $iniciale) {
                $iniciales .= substr($iniciale, 0, 1);
            }

            if($request->zona_id == '') {
                $request->zona_id = null;
            }
            if($request->linea_id == '') {
                $request->linea_id = null;
            }

            DB::commit();
            return response(Empleados::create([
                'iniciales' => strtoupper($iniciales),
                'nombre' => $request->nombre,
                'resumen' => $request->nombre,
                'email' => $request->email,
                'default_zona_id' => $request->zona_id,
                'default_linea_id' => $request->linea_id,
                'responsable_id' => $request->responsable_id,
            ]));
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function show($id)
    {
        try {
            DB::beginTransaction();
            $empl = Empleados::where('id', $id)->first();

            if (!$empl) return response(['message' => 'No existe la linea solicitada'], 404);
            DB::commit();
            return response($empl);
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            $empl = Empleados::where('id', $id)->first();
            if (!$empl) return response(['message' => 'No existe el recurso solicitado'], 404);

            $this->validate($request, [
                'nombre' => ['required', 'string']
            ]);
            if ($empl->nombre != $request->nombre) {
                $iniciales = '';
                $txt = explode(' ', $request->nombre);
                foreach ($txt as $iniciale) {
                    $iniciales .= substr($iniciale, 0, 1);
                }
                $empl->nombre = $request->nombre;
                $empl->iniciales = strtoupper($iniciales);
            }

            if(isset($request->responsable_id) && $request->responsable_id != '') {
                $this->validate($request, [
                    'responsable_id' => ['exists:empleados,id'],
                ]);

                $empl->responsable_id = $request->responsable_id;
            }

            $empl->email = $request->email;

            if(isset($request->zona_id) && $request->zona_id != '') $empl->default_zona_id = $request->zona_id;
            
            if(isset($request->linea_id) && $request->linea_id != '') $empl->default_linea_id = $request->linea_id;

            $empl->save();

            DB::commit();
            return response($empl);

        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            if ($id <= 0 || $id == null) return response(['id' => $id,'message' => 'No existe el recurso solicitado'], 404);
            $users = User::where('empleados_id', $id)->get();
            if(isset($users) && !$users->isEmpty()) {
                return response(['message' => 'No puede eliminarse el empleado, tiene usuarios asociados'], 422);
            }
            $cargoslineasempl = Cargoslineasempleados::where('empleados_id', $id)->get();
            if(isset($cargoslineasempl) && !$cargoslineasempl->isEmpty()) {
                return response(['message' => 'No puede eliminarse el empleado, pertenece a una o varias lineas y tiene un cargos asociados'], 422);
            }
            $query = Noconformidades::query();
            $noconf = $query->where(function ($query) use ($id) {
                $query->where('empleados_id', $id)
                    ->orWhere('evaluador', $id);
            })->get();
            if(isset($noconf) && !$noconf->isEmpty()) {
                return response(['message' => 'No puede eliminarse el empleado, pertenece a una o varias no conformidades'], 422);
            }
            $query = Confirmacionprocesos::query();
            $confpr = $query->where(function ($query) use ($id) {
                $query->where('empleados_id', $id)
                    ->orWhere('evaluador', $id);
            })->get();
            if(isset($confpr) && !$confpr->isEmpty()) {
                return response(['message' => 'No puede eliminarse el empleado, pertenece a una o varias confirmaciones de proceso'], 422);
            }
            $query = Registrocalidad::query();
            $regcal = $query->where(function ($query) use ($id) {
                $query->where('empleados_id', $id)
                    ->orWhere('evaluador', $id);
            })->get();
            if(isset($regcal) && !$regcal->isEmpty()) {
                return response(['message' => 'No puede eliminarse el empleado, pertenece a uno o varios registros de calidad'], 422);
            }
//            $manodeobra = Manodeobra::where('empleado_id', $id)
//                ->where(DB::raw('YEAR(fecha)'),Carbon::now()->year)
//                ->where(DB::raw('MONTH(fecha)'),Carbon::now()->month)
//                ->get();
            $manodeobra = Manodeobra::where('empleado_id', $id)->get();
            if(isset($manodeobra) && !$manodeobra->isEmpty()) {
                return response(['message' => 'No puede eliminarse el empleado, tiene turnos asignados'], 422);
//                $semana_actual = Dias::where('dia', Carbon::now()->day)
//                    ->where('mes', Carbon::now()->month)
//                    ->where('anno', Carbon::now()->year)
//                    ->first('semana');
//                foreach ($manodeobra as $item) {
//                    $semana = Dias::where('dia', explode('-',$item->fecha)[2])
//                        ->where('mes', explode('-',$item->fecha)[1])
//                        ->where('anno', explode('-',$item->fecha)[0])
//                        ->first('semana');
//                    if($semana->semana == $semana_actual->semana) {
//                        return response(['message' => 'No puede eliminarse el empleado, tiene turnos asignados esta semana'], 422);
//                    }
//                }
            }
            Empleados::where('id', $id)->delete();
            DB::commit();
            return response(['message' => 'El empleado ha sido eliminado'], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function empleados(\App\Line $line)
    {
        try {
            DB::beginTransaction();
            $cargolineaempleado = $line->cargosLineas;

            $arrayempleados = [];

            foreach ($cargolineaempleado as $empleado) {
                $arrayempleados[] = $empleado->empleado;
            }
            DB::commit();
            return response($arrayempleados);
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }
}
