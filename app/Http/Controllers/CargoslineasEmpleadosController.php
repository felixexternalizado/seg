<?php

namespace App\Http\Controllers;

use App\Cargoslineasempleados;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CargoslineasEmpleadosController extends AppController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $query = Cargoslineasempleados::query()->with(['empleado', 'linea', 'cargo']);


        $query->join('lineas', 'lineas.id', 'cargoslineasempleados.lineas_id')
        ->join('empleados', 'empleados.id', 'cargoslineasempleados.empleados_id')
        ->join('cargos', 'cargos.id', 'cargoslineasempleados.cargos_id')
        ->select('cargoslineasempleados.*', 'lineas.resumen as linea_resumen', 'empleados.nombre as empleado_nombre', 'cargos.resumen as cargo_resumen');

        $model = new Cargoslineasempleados();

        $query = $this->absoluteFilter($request,$model,$query);

        $result = $query->get();

        return response($result);

    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'linea_id' => ['required', 'exists:lineas,id'],
            'empleado_id' => ['required', 'exists:empleados,id'],
            'cargo_id' => ['required', 'exists:cargos,id'],
        ]);
        if ($validator->fails()) {
            return response($validator->errors());
        }

        $seguridadcargo = new Cargoslineasempleados;
        $seguridadcargo->lineas_id = $request->linea_id;
        $seguridadcargo->empleados_id = $request->empleado_id;
        $seguridadcargo->cargos_id = $request->cargo_id;
        $seguridadcargo->save();

        return response($seguridadcargo->with(['empleado', 'linea', 'cargo'])->findOrFail($seguridadcargo->id));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cargoslineasempleados  $seguridadcargo
     * @return \Illuminate\Http\Response
     */
    public function show(Cargoslineasempleados $seguridadcargo)
    {

        return response($seguridadcargo->with(['empleados_id', 'lineas_id', 'cargos_id'])->find($seguridadcargo->id));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cargoslineasempleados  $seguridadcargo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cargoslineasempleados $seguridadcargo)
    {

        $validator = Validator::make($request->all(), [
            'linea_id' => ['exists:lineas,id'],
            'empleado_id' => ['exists:empleados,id'],
            'cargo_id' => ['exists:cargos,id'],
        ]);
        if ($validator->fails()) {
            return response($validator->errors());
        }

        if(isset($request->linea_id)) $seguridadcargo->lineas_id = $request->linea_id;
        if(isset($request->empleado_id)) $seguridadcargo->empleados_id = $request->empleado_id;
        if(isset($request->cargo_id)) $seguridadcargo->cargos_id = $request->cargo_id;
        $seguridadcargo->save();

        return response($seguridadcargo);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cargoslineasempleados  $seguridadcargo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cargoslineasempleados $seguridadcargo)
    {

        $seguridadcargo->delete();

        return response(['message' => 'ok']);

    }
}
