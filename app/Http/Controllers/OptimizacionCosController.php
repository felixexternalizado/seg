<?php

namespace App\Http\Controllers;

use App\OptimizacionCos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class OptimizacionCosController extends AppController
{
    public function index(Request $request)
    {
        $query = OptimizacionCos::query();
        $model = new OptimizacionCos();

        $query = $this->absoluteFilter($request,$model,$query);

        $result = $query->get();

        return response($result, 200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'numero_orden' => ['required'],
        ]);

        $cos = OptimizacionCos::create($request->all());

        return response($cos, 200);
    }

    public function show($id)
    {
        $cos = OptimizacionCos::findOrFail($id);

        return response($cos);
    }

    public function update(Request $request, $id)
    {
        $cos = OptimizacionCos::findOrFail($id);

        $this->validate($request, [
            'numero_orden' => ['required'],
        ]);

        $cos->update($request->all());

        return response($cos, 200);
    }

    public function destroy($id)
    {
        OptimizacionCos::findOrFail($id)->delete();

        return response(['message' => 'Eliminado'], 200);
    }
}
