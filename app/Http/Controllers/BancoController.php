<?php

namespace App\Http\Controllers;

use App\Banco;
use App\BancoPaso;
use App\BancoPrueba;
use App\BancoPruebaDescripcion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class BancoController extends Controller
{
    protected $lang = 'es';

    public function index(Request $request, $tabla, $banco = null, $carpeta = null, $paso = null, $fecha = null)
    {
        try {
            if ($tabla === 'bancos') {
                $model = 'App\\Banco';
                $cols = ['fecha', 'banco', 'catalogo', 'inicio', 'fin', 'nPruebas', 'ok', 'okPer', 'ko', 'nOkPer'];
            } elseif ($tabla === 'pruebas') {
                $model = 'App\\BancoPrueba';
                $campo = 'banco';
                $cols = ['paso', 'fecha', 'descripcion', 'etapa', 'minimo', 'maximo', 'media', 'total', 'nOk', 'nOkPer'];
            } elseif ($tabla === 'pasos') {
                $model = 'App\\BancoPaso';
                $campo = 'banco';
                $cols = ['dmc', 'inicio', 'vV1', 'vV2', 'vV3', 'vV4'];
            } elseif ($tabla === 'paso') {
                $model = 'App\\BancoPaso';
                $campo = 'dmc';
                $cols = ['paso', 'descripcion', 'etapaProceso', 'etapa', 'nPruebas',
                    'vV1', 'sV1', 'liV1', 'lsV1',
                    'vV2', 'sV2', 'liV2', 'lsV2',
                    'vV3', 'sV3', 'liV3', 'lsV3',
                    'vV4', 'sV4', 'liV4', 'lsV4'];
            } elseif ($tabla === 'dmc') {
                $model = 'App\\BancoPruebaDescripcion';
                $campo = 'dmc';
                $cols = ['dmc', 'fecha', 'banco', 'catalogo','nPruebas',
                    'inicio', 'ok', 'ko'];
            }
            $query = $model::query();

            if($tabla === 'pasos' || $tabla === 'paso' || $tabla === 'dmc') {
                if (isset($banco) && $tabla === 'dmc') {

                    $arr_bancos = explode(' ', $banco);
                    $query->whereIN('dmc', $arr_bancos);

                    if (isset($request->filters)) {
                        $fechas = [];
                        foreach ($request->filters as $filter) {
                            $filter = json_decode($filter);

                            if ($filter->field !== 'fecha') continue;

                            if ($filter->operator === '=') $query->where('fecha', Carbon::createFromFormat('d/m/Y', $filter->value[0])->format('Y-m-d H:i:s'));

                            if ($filter->operator === 'between') {
                                $fechas[] = Carbon::createFromFormat('d/m/Y', $filter->value[0])->format('Y-m-d');
                                $fechas[] = Carbon::createFromFormat('d/m/Y', $filter->value[1])->format('Y-m-d');
                                $query->whereBetween('fecha', $fechas);
                            }
                        }
                    }

                    $recordsFiltered = $query->count();

                    $query = $this->skipOrder($request, $query);

                    $data = $query->get();
                    foreach($data as $d) {
                        $d->inicio = Carbon::createFromFormat('Y-m-d H:i:s', $d->fecha)->format('H:i:s');
                        $d->fecha = Carbon::createFromFormat('Y-m-d H:i:s', $d->fecha)->format('d/m/Y');
                        if($d->resultado === 'BUENO') {
                            $d->ok = 1;
                            $d->ko = 0;
                        } else {
                            $d->ok = 0;
                            $d->ko = 1;
                        }
                    }


                    $output = [
                        'data' => $data,
                        'recordsFiltered' => $recordsFiltered,
                    ];

                } else {
                    if (isset($paso)) $query->where('paso', $paso);
                    if (isset($fecha)) {
                        $date = Carbon::createFromFormat('Ymd', $fecha)->format('Y-m-d');
                        $query->where('fecha', $date)->where('banco', $banco)->where('carpeta', $carpeta);
                    } else {
                        $query->where('dmc', $banco);
                    }

                    $query = $this->readFilters($query, $request, $cols);

                    $query->where('descripcion', '!=', '');

                    $recordsFiltered = $query->count();

                    if($tabla !== 'paso') $query = $this->skipOrder($request, $query);

                    $data = $query->get();

                    if ($tabla === 'paso') {
                        if(isset($request->select_prueba) && $request->select_prueba != '') {
                            $cabecera = BancoPruebaDescripcion::where('dmc', $request->banco)->where('nPruebas',$request->select_prueba)->first();
                        } elseif ($request->select_prueba === '') {
                            $query = BancoPruebaDescripcion::where('dmc', $request->banco);
                            $count = $query->count();
                            if($count == 1) $cabecera = $query->first();
                        }

                        for($i = 7; $i >=0; $i--){

                            if(!isset($request['iSortCol_'.$i])) continue;

                            if($request['sSortDir_'.$i] == 'asc') {
                                $data = $data->sortBy($request['mDataProp_'.$request['iSortCol_'.$i]]);
                            } else {
                                $data = $data->sortByDesc($request['mDataProp_'.$request['iSortCol_'.$i]]);
                            }
                        }
                    }
                    if($tabla == 'pasos') {
                        foreach ($data as $d) {
                            $d->etapaProceso = $d->etapaProceso == 'B' ? $d->etapaProceso = 'OK' : 'NOK';
                        }
                    }

                    $output = [
                        'data' => $data,
                        'recordsFiltered' => $recordsFiltered,
                    ];
                    if(isset($cabecera)) {
                        $output['cabecera'] = $cabecera;
                    }
                }
            } else {
                if($tabla === 'pruebas') {
                    $query->where($campo,$banco)->where('carpeta', $carpeta);
                    $query->where('descripcion', '!=', '');
                }

                $query = $this->readFilters($query, $request, $cols);

                $recordsFiltered = $query->count();

                $query = $this->skipOrder($request,$query);

                $data = $query->get();
                if($tabla === 'pruebas') {
                    foreach ($data as $item) {
                        $item->formatedfecha = Carbon::createFromFormat('d/m/Y', $item->fecha)->format('Ymd');
                    }
                }

                $output = [
                    'data' => $data,
                    'recordsFiltered' => $recordsFiltered,
                ];
            }

            return response($output,200);
        } catch (\Exception $e) {
            throw($e);
        }
    }

    public function store()
    {
        try {
            $storage = $this->storage . '/bancos/datos/';
            //BUSCA TODAS LAS CARPETAS BAJO DATOS
            $this->buscarCarpetas($storage);

            return response("Datos importados de los excel con Ã©xito");
        } catch (\Exception $e) {
            throw($e);
        }
    }

    public function show($id)
    {

    }

    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            DB::commit();
            return response(['message' => 'ok']);
        } catch (\Exception $e) {
            DB::rollBack();
            throw($e);
        }
    }

    public function config(Request $request)
    {
        if (isset($request->lang)) {
            if ($request->lang == 'en') {
                App::setLocale($request->lang); //Se cambia el idioma al inglÃƒÂ©s
            }
        }
        $output = [];
        if ($request->tabla == 'resumen') {
            $output['dataUrl'] = url('/api/bancos?');
            $output['nextUrl'] = "/pruebas/@@banco@@/@@carpeta@@/";
            $output['cabecera'] = [
                "Tabla" => trans('diccionario.Resumen')
            ];

            $output['order'] = [
                 [
                    'col' => 0,
                    'dir' => 'desc'
                ],
                [
                    'col' => 1,
                    'dir' => 'asc'
                ],
                [
                    'col' => 3,
                    'dir' => 'desc'
                ],
            ];

            $output['columns'] = [
                array('data' => 'fecha', 'title' => trans('diccionario.Fecha'), 'width' => '100px', 'sortable' => true),
                array('data' => 'banco', 'title' => trans('diccionario.Banco'), 'min-width' => '150px','sortable' => true),
                array('data' => 'catalogo', 'title' => trans('diccionario.Catalogo'), 'width' => '150px', 'sortable' => true),
                array('data' => 'inicio', 'title' => trans('diccionario.Inicio'), 'width' => '75px', 'sortable' => true),
                array('data' => 'fin', 'title' => trans('diccionario.Fin'), 'width' => '75px', 'sortable' => true),
                array('data' => 'nPruebas', 'title' => trans('diccionario.Total de pruebas'), 'headerClass' => 'text-right', 'htmlClass' => 'text-right', 'width' => '175px', 'sortable' => true),
                array('data' => 'ok', 'title' => 'OK', 'headerClass' => 'text-right', 'htmlClass' => 'text-right', 'width' => '125px', 'sortable' => true),
                array('data' => 'okPer', 'title' => '%OK', 'headerClass' => 'text-right', 'format' => '2Decimales', 'htmlClass' => 'text-right', 'width' => '100px', 'sortable' => true),
                array('data' => 'ko', 'title' => 'NOK', 'headerClass' => 'text-right', 'htmlClass' => 'text-right', 'width' => '125px', 'sortable' => true),
                array('data' => 'nOkPer', 'title' => '%NOK', 'headerClass' => 'text-right', 'format' => '2Decimales', 'htmlClass' => 'text-right', 'width' => '100px', 'sortable' => true)
            ];

            $output['filters'] = [
                ["field" => "fecha", "name" => trans('diccionario.Fecha').' (dd/mm/YYYY)', "operators" => [
                    ["text" => trans('diccionario.entre'), "value" => 'between'],["text" => trans('diccionario.igual'), "value" => '=']
                ]],
                ["field" => "banco", "name" => trans('diccionario.Banco'), "operators" => [
                    ["text" => trans('diccionario.igual'), "value" => "="], ["text" => trans('diccionario.contiene'), "value" => "%"]
                ]],
                ["field" => "catalogo", "name" => trans('diccionario.Catalogo'), "operators" => [
                    ["text" => trans('diccionario.igual'), "value" => "="], ["text" => trans('diccionario.contiene'), "value" => "%"]
                ]],
                ["field" => "nPruebas", "name" => trans('diccionario.Total de pruebas'), "operators" => [
                    ["text" => trans('diccionario.entre'), "value" => 'between'],["text" => trans('diccionario.igual'), "value" => '='],
                    ["text" => trans('diccionario.mayor'), "value" => '>'],["text" => trans('diccionario.menor'), "value" => '<']
                ]]
            ];
        } elseif ($request->tabla == 'pruebas') {
            $output['dataUrl'] = url('/api/pruebas/' . $request->banco . '/' . $request->carpeta . '?');
            $output['nextUrl'] = "/pasos/@@banco@@/@@carpeta@@/@@paso@@/@@formatedfecha@@/";
            $output['cabecera'] = [
                "Tabla" => trans('diccionario.Pruebas'),
                'Banco' => $request->banco,
                'Carpeta' => $request->carpeta,
            ];

            $pruebas = BancoPrueba::select('fecha')->where('banco', $request->banco)->where('carpeta', $request->carpeta)->groupBy('fecha')->get();
            $aux_array[] = [
                'text' => trans('diccionario.select_fecha'),
                'value' => '',
            ];
            foreach($pruebas as $prueba) {
                $aux_array[] = [
                    'text' => $prueba->fecha,
                    'value' => Carbon::createFromFormat('d/m/Y',$prueba->fecha)->format('Y-m-d'),
                ];
            }

            $output['selects'] = [
                [
                    'field' => 'fecha',
                    'options' => $aux_array,
                ],
            ];

            $output['order'] = [
                [
                    'col' => 0,
                    'dir' => 'asc'
                ],
            ];

            $output['columns'] = [
                array('data' => 'paso', 'title' => trans('diccionario.Paso'), 'width' => '50px', 'sortable' => true),
                array('data' => 'fecha', 'title' => trans('diccionario.Fecha'), 'width' => '100px', 'sortable' => true),
                array('data' => 'descripcion', 'title' => trans('diccionario.Descripcion'), 'min-width' => '425px'),
                array('data' => 'etapa', 'title' => trans('diccionario.Etapa'), 'width' => '100px'),
                array('data' => 'minimo', 'title' => trans('diccionario.Minimo'), 'format' => '2Decimales', 'headerClass' => 'text-right', 'htmlClass' => 'text-right', 'width' => '100px'),
                array('data' => 'media', 'title' => trans('diccionario.Media'), 'format' => '2Decimales', 'headerClass' => 'text-right', 'htmlClass' => 'text-right', 'width' => '100px'),
                array('data' => 'maximo', 'title' => trans('diccionario.Maximo'), 'format' => '2Decimales', 'headerClass' => 'text-right', 'htmlClass' => 'text-right', 'width' => '100px'),
                array('data' => 'total', 'title' => trans('diccionario.Total de pruebas'), 'headerClass' => 'text-right', 'htmlClass' => 'text-right', 'width' => '150px'),
                array('data' => 'nOk', 'title' => 'NOK', 'headerClass' => 'text-right', 'htmlClass' => 'text-right', 'width' => '50px'),
                array('data' => 'nOkPer', 'title' => '%NOK', 'format' => '2Decimales', 'headerClass' => 'text-right', 'htmlClass' => 'text-right', 'width' => '75px'),
            ];

            $output['filters'] = [
                ["field" => "paso", "name" => trans('diccionario.Paso'), "operators" => [
                    ["text" => trans('diccionario.igual'), "value" => "="], ["text" => trans('diccionario.contiene'), "value" => "%"]
                ]],
                ["field" => "etapa", "name" => trans('diccionario.Etapa'), "operators" => [
                    ["text" => trans('diccionario.igual'), "value" => "="], ["text" => trans('diccionario.contiene'), "value" => "%"]
                ]],
            ];
        } elseif ($request->tabla == 'pasos') {
            $output['dataUrl'] = url('/api/pasos/' . $request->banco . '/' . $request->carpeta . '/' . $request->paso . '/' . $request->fecha . '?');
            $output['nextUrl'] = "/paso/@@dmc@@/";
            $output['cabecera'] = [
                "Tabla" => trans('diccionario.Pasos'),
                'Pasos' => $request->paso,
                'Carpeta' => $request->carpeta
            ];

            $output['order'] = [
                [
                    'col' => 1,
                    'dir' => 'desc'
                ],
            ];

            $output['columns'] = [
                array('data' => 'dmc', 'title' => 'DMC', 'width' => '225px', 'sortable' => true),
                array('data' => 'inicio', 'title' => trans('diccionario.Inicio'), 'width' => '75px', 'sortable' => true),
                array('data' => 'etapaProceso', 'title' => trans('diccionario.estado'), 'width' => '75px'),
                array('data' => 'vV1', 'title' => trans('diccionario.Valor 1'), 'min-width' => '125px', 'width' => 'calc((100vw - 482px - 2rem)/4)', 'sortable' => true),
                array('data' => 'vV2', 'title' => trans('diccionario.Valor 2'), 'min-width' => '125px', 'width' => 'calc((100vw - 482px - 2rem)/4)', 'sortable' => true),
                array('data' => 'vV3', 'title' => trans('diccionario.Valor 3'), 'min-width' => '125px', 'width' => 'calc((100vw - 482px - 2rem)/4)', 'sortable' => true),
                array('data' => 'vV4', 'title' => trans('diccionario.Valor 4'), 'min-width' => '125px', 'width' => 'calc((100vw - 482px - 2rem)/4)', 'sortable' => true),
            ];

            $output['filters'] = [
                ["field" => "dmc", "name" => 'DMC', "operators" => [
                    ["text" => trans('diccionario.igual'), "value" => "="], ["text" => trans('diccionario.contiene'), "value" => "%"]
                ]],
            ];
        } elseif ($request->tabla == 'paso') {
            if(isset($request->carpeta)) $output['dataUrl'] = url('/api/paso/' . $request->banco . '/'. $request->carpeta . '?'); //$request->banco en este caso es el dmc y $carpeta en este caso es la fecha
            $output['dataUrl'] = url('/api/paso/' . $request->banco . '/'. $request->carpeta . '?'); //$request->banco en este caso es el dmc
            $output['nextUrl'] = null;
            $output['cabecera'] = [
                "Tabla" => trans('diccionario.Paso'),
                'DMC' => $request->banco, //$request->banco en este caso es el dmc
            ];

            $output['order'] = [
                [
                    'col' => 0,
                    'dir' => 'asc'
                ],
                [
                    'col' => 3,
                    'dir' => 'asc'
                ],
            ];

            $bpds = BancoPruebaDescripcion::where('dmc',$request->banco)->get('nPruebas');
            if($bpds->count() > 0) {
                $aux_array[] = [
                    'text' => trans('diccionario.nprueba'),
                    'value' => '',
                ];
                foreach ($bpds as $bpd) {
                	$other_aux_array[] = $bpd->nPruebas;
                }
                $other_aux_array = array_unique($other_aux_array);
                foreach ($other_aux_array as $bpd) {
                    $aux_array[] = [
                        'text' => trans('diccionario.prueba').' '.$bpd,
                        'value' => $bpd
                    ];
                }

                $output['selects'] = [
                    [
                        'field' => 'prueba',
                        'options' => $aux_array,
                        'value' => '',
                    ],
                ];
            }

            $output['columns'] = [
                array('data' => 'paso', 'title' => trans('diccionario.Paso'), 'width' => '50px', 'sortable' => true),
                array('data' => 'descripcion', 'title' => trans('diccionario.Descripcion'), 'min-width' => '325px'),
                array('data' => 'etapa', 'title' => trans('diccionario.Etapa'), 'width' => '75px'),
                array('data' => 'nPruebas', 'title' => trans('diccionario.nprueba'), 'width' => '125px', 'sortable' => true),
                array('data' => 'vV1', 'title' => trans('diccionario.vv1'), 'width' => '200px', 'htmlTitle' => trans('diccionario.li').': @@liV1@@ '.trans('diccionario.ls').': @@lsV1@@'),
                array('data' => 'vV2', 'title' => trans('diccionario.vv2'), 'width' => '200px', 'htmlTitle' => trans('diccionario.li').': @@liV2@@ '.trans('diccionario.ls').': @@lsV2@@'),
                array('data' => 'vV3', 'title' => trans('diccionario.vv3'), 'width' => '200px', 'htmlTitle' => trans('diccionario.li').': @@liV3@@ '.trans('diccionario.ls').': @@lsV3@@'),
                array('data' => 'vV4', 'title' => trans('diccionario.vv4'), 'width' => '200px', 'htmlTitle' => trans('diccionario.li').': @@liV4@@ '.trans('diccionario.ls').': @@lsV4@@'),
            ];

            $output['filters'] = [];

        }elseif ($request->tabla == 'dmc') {
            $output['dataUrl'] = url('/api/dmc/' . $request->banco . '?'); //$request->banco en este caso es el dmc
            $output['nextUrl'] = "/paso/@@dmc@@/";
            $output['cabecera'] = [
                "Tabla" => trans('diccionario.resultados_dmc'),
            ];

            $output['order'] = [
                [
                    'col' => 0,
                    'dir' => 'desc'
                ],
                [
                    'col' => 1,
                    'dir' => 'desc'
                ],
            ];

            $output['columns'] = [
                array('data' => 'dmc', 'title' => 'DMC', 'width' => '225px','sortable' => true),
                array('data' => 'fecha', 'title' => trans('diccionario.Fecha'), 'width' => '100px','sortable' => true),
                array('data' => 'nPruebas', 'title' => trans('diccionario.nprueba'), 'width' => '125px', 'sortable' => true),
                array('data' => 'banco', 'title' => trans('diccionario.Banco'), 'sortable' => true),
                array('data' => 'catalogo', 'title' => trans('diccionario.Catalogo'), 'width' => '150px'),
                array('data' => 'inicio', 'title' => trans('diccionario.Inicio'), 'width' => '75px','sortable' => true),
                array('data' => 'ok', 'title' => 'OK', 'width' => '50px'),
                array('data' => 'ko', 'title' => 'NOK', 'width' => '50px'),
            ];

            $output['filters'] = [
                ["field" => "fecha", "name" => trans('diccionario.Fecha'), "operators" => [
                    ["text" => trans('diccionario.entre'), "value" => 'between'],["text" => trans('diccionario.igual'), "value" => '=']
                ]],
            ];
        }

        App::setLocale($this->lang); //Se deshace el cambio de idioma.

        return response($output, 200);

    }

    public function exportarExcel(Request $request)
    {
        $stmt = $this->getQuery($request);

        if(is_array($stmt) && isset($stmt['error'])) return response(['message' => 'Las fechas introducidas tienen un formato incorrecto. El formato correcto es d/m/Y'], 400);

        foreach ($stmt as $obj) {
            $array_aux[] = array_keys($obj->toArray());
        }
        $array_campos = $array_aux[0];

        foreach ($stmt as $obj) {
            $element = array();
            foreach ($array_campos as $campo) {
                $element[$campo] = $obj[$campo];
            }
            $array_elementos[] = $element;
        }

//Crear Excel
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
//Array con columnas
        $alphabet = array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
            "aa", "ab", "ac", "ad", "ae", "af", "ag", "ah", "ai", "aj", "ak", "al", "am", "an", "ao", "ap", "aq", "ar", "as", "at", "au", "av", "aw", "ax", "ay", "az",
            "ba", "bb", "bc", "bd", "be", "bf", "bg", "bh", "bi", "bj", "bk", "bl", "bm", "bn", "bo", "bp", "bq", "br", "bs", "bt", "bu", "bv", "bw", "bx", "by", "bz",
            "ca", "cb", "cc", "cd", "ce", "cf", "cg", "ch", "ci", "cj", "ck", "cl", "cm", "cn", "co", "cp", "cq", "cr", "cs", "ct", "cu", "cv", "cw", "cx", "cy", "cz",
            "da", "db", "dc", "dd", "de", "df", "dg", "dh", "di", "dj", "dk", "dl", "dm", "dn", "do", "dp", "dq", "dr", "ds", "dt", "du", "dv", "dw", "dx", "dy", "dz",
        );

//Cargamos la cabecera
        foreach ($array_campos as $key => $campo) {
            $cell = $alphabet[$key] . '1';
            $sheet->setCellValue($cell, $campo);
        }

//Cargamos los valores
        foreach ($array_elementos as $num_fila => $array_valores) {
            $num_columna = 0;
            //AÃ±adimos 2 a el numero de fila para no sobreescribir la cabecera
            $num_fila = $num_fila + 2;

            foreach ($array_valores as $nombre_campo => $valor) {

                $cell = $alphabet[$num_columna] . $num_fila;

                if ($nombre_campo === 'catalogo') $sheet->setCellValue($cell, "'" . $valor);
                else $sheet->setCellValue($cell, (string)$valor);

                $num_columna++;
            }

        }

        $writer = new Xlsx($spreadsheet);
        $path = $this->storage . '/bancos/files/export_' . date('YmdHis') . $request->tabla . '.xlsx';

        $writer->save($path);

        return response()->download($path);
    }

    public function leerXML() {
        try {
            $storage = $this->storage . '/bancos/datos/';
            //BUSCA TODAS LAS CARPETAS BAJO DATOS
            $this->buscarCarpetas($storage);

            return response("Datos importados de los excel con Éxito");
        } catch (\Exception $e) {
            throw($e);
        }
    }

    public function servicio() {

        $data = '';

        $estado_servicio = file_get_contents($this->storage . '/bancos/estado_servicio.txt');

        $estado_servicio = explode('=',$estado_servicio);

        if($estado_servicio[0] === 'estado' && $estado_servicio[1] === 'encendido') {
            $data = 'estado=detenido';
        } else {
            $data = 'estado=encendido';
        }

        file_put_contents($this->storage . '/bancos/estado_servicio.txt', $data);

        return response(['message' => 'Servicio cambiado'], 200);
    }

    //<editor-fold desc="PRIVATE">
    private function getQuery($request)
    {
        if (isset($request->lang)) {
            if ($request->lang == 'en') {
                App::setLocale($request->lang); //Se cambia el idioma al inglÃƒÂ©s
            }
        }

        $table = 'banco' . $request->tabla;

        if(!isset($request->cols)) {
            $cols = ['fecha as '.trans('diccionario.Fecha'),
             'banco as '.trans('diccionario.Banco'),
             'catalogo as ' .trans('diccionario.Catalogo'),
             'inicio as ' .trans('diccionario.Inicio'),
             'fin as ' .trans('diccionario.Fin'),
             'nPruebas as ' .trans('diccionario.Total de pruebas'),
             'ok as OK',
             'okPer as %OK',
             'ko as NOK',
             'nOkPer as %NOK'
            ];
        } else {
            $cols = explode(';', $request->cols);
        }

        $query = Banco::select($cols);

        if ($table === 'bancopruebas') {
            if(!isset($request->cols)) {
                $cols = ['fecha as '.trans('diccionario.Fecha'),
                    'paso as '.trans('diccionario.Paso'),
                    'descripcion as '.trans('diccionario.Descripcion'),
                    'etapa as ' .trans('diccionario.Etapa'),
                    'minimo as ' .trans('diccionario.Minimo'),
                    'maximo as ' .trans('diccionario.Maximo'),
                    'media as ' .trans('diccionario.Media'),
                    'total as ' .trans('diccionario.Total de pruebas'),
                    'nOk as NOK',
                    'nOkPer as %NOK'
                ];
            } else {
                $cols = explode(';', $request->cols);
            }

            $query = BancoPrueba::select($cols);
            if (isset($request->carpeta)) {
                $query->where('carpeta', $request->carpeta);
            }
            if (isset($request->banco)) {
                $query->where('banco', $request->banco);
            }
        }

        if ($table === 'bancopasos') {
            $cols = isset($request->cols) ? explode(';', $request->cols) : ['dmc', 'inicio as '.trans('diccionario.Inicio'), 'vV1', 'vV2', 'vV3', 'vV4'];

            $query = BancoPaso::select($cols);
            if (isset($request->carpeta)) {
                $query->where('carpeta', $request->carpeta);
            }
            if (isset($request->banco)) {
                if (is_numeric($request->banco)) {
                    $query->where('dmc', $request->banco);
                } else {
                    $query->where('banco', $request->banco);
                }
            }

            if(isset($request->fecha))  $query->where('fecha', Carbon::createFromFormat('Ymd',$request->fecha)->format('Y-m-d'));

            if (isset($request->paso)) $query->where('paso', $request->paso);

            if (isset($request->dmc)) $query->where('dmc', $request->dmc);
        }

        if ($table === 'bancopaso') {
            $cols = isset($request->cols) ? explode(';', $request->cols) : ['paso', 'descripcion', 'etapa', 'vV1', 'sV1', 'liV1', 'lsV1', 'vV2', 'sV2', 'liV2', 'lsV2', 'vV3', 'sV3', 'liV3', 'lsV3', 'vV4', 'sV4', 'liV4', 'lsV4'];
            $query = BancoPaso::select($cols);
            if (isset($request->banco)) {
                $query->where('dmc', $request->banco);
            }
            if (isset($request->paso)) {
                $query->where('paso', $request->paso);
            }
        }

        if ($table === 'bancodmc') {
            $cols = isset($request->cols) ? explode(';', $request->cols) : ['dmc', 'fecha', 'nPruebas', 'banco', 'catalogo','resultado'];
            if(!isset($request->cols)) {
                $cols = [
                    'dmc as DMC',
                    'fecha as '.trans('diccionario.Fecha'),
                    'nPruebas as '.trans('diccionario.Total de pruebas'),
                    'banco as '.trans('diccionario.Banco'),
                    'catalogo as '.trans('diccionario.Catalogo'),
                    'resultado as '.trans('diccionario.resultado'),
                ];
            }
            $query = BancoPruebaDescripcion::select($cols);
            if (isset($request->banco)) {
                $query->where('dmc', $request->banco);
            }
        }

        if(isset($request->select_fecha) && $request->select_fecha !== '') {
            $query->where('fecha', Carbon::createFromFormat('Y-m-d',$request->select_fecha)->format('d/m/Y'));
        }

        if($table === 'bancopruebas' || ($table !== 'bancodmc' && isset($request->banco) && is_numeric($request->banco))) {
            $query->where('descripcion', '!=', '');
        }

        $query = $this->readFilters($query, $request);

        if(is_array($query) && isset($query['error'])) return ['error' => 400];

        if (isset($request->iSortCol_0)) {
            $ordenCol = $request['mDataProp_' . $request->iSortCol_0];
            $ordenDir = $request->sSortDir_0;
            $query->orderBy($ordenCol, $ordenDir);
        }

        $query->skip(0)->take(1000);

        return $query->get();
    }

    private function buscarCarpetas($path, $banco = null)
    {
        $stop = false;

        foreach (scandir($path) as $elemento) { //HASTA QUE NO ENCUENTRA UN FICHERO, SIGUE BUSCANDO EN SUBCARPETAS
            $estado_servicio = file_get_contents($this->storage.'/bancos/estado_servicio.txt');

            $estado_servicio = explode('=',$estado_servicio);

            if($estado_servicio[0] === 'estado' && $estado_servicio[1] === 'detenido') {echo 'El servicio esta detenido';die;}

            if ($stop) continue;

            if ($elemento == "." || $elemento == "..") continue;

            if (is_numeric(strpos($elemento, 'banco')) || is_numeric(strpos($elemento, 'Banco'))) $banco = $elemento;

            if (is_dir($path . '/' . $elemento)) {
                //var_dump($path . '/' . $elemento.' Es una carpeta');
                $this->buscarCarpetas($path . '/' . $elemento, $banco);
            } else {
                $stop = true;
                //var_dump($path . '/' . $elemento.' Es un archivo');
                //CUANDO ENCUENTRA UN ARCHIVO XML
                $this->listarArchivos($path, $banco);
            }
        }
    }

    private function listarArchivos($path, $banco)
    {
        $totalOk = 0;
        $totalnOk = 0;
        $fecha = null;
        $inicio = null;
        $fin = null;
        $catalogo = 'error';
        $minValor = null;
        $maxValor = null;
        $pruebas = [];

        $objbanco = new Banco();

        $dir = opendir($path);
        //ABRE EL ARCHIVO XML

        $folder = explode('/', $path)[count(explode('/', $path)) - 1];
        $banco = explode('/', $path)[count(explode('/', $path)) - 2];
        $carp = explode('#', $folder);

        $count = 0;
        $arr_fechas = [];
        while ($elemento = readdir($dir)) {

            if (is_dir($path . '/' . $elemento)) continue;

            if ($elemento == '.' || $elemento == '..') continue;

            if (substr($elemento, -2) === 'ok' || substr($elemento, -2) === 'ko') continue;

            $datosArchivo = $this->leerArchivo($path, $elemento, $pruebas);
            if ($datosArchivo) {

                if ($inicio == null || $inicio > $datosArchivo['hora']) $inicio = $datosArchivo['hora'];
                if ($fin == null || $fin < $datosArchivo['hora']) $fin = $datosArchivo['hora'];
                if ($fecha == null || $fecha < $datosArchivo['fecha']) $fecha = $datosArchivo['fecha'];

                if ($datosArchivo['ok']) {
                    rename($path . '/' . $elemento, $path . '/' . $elemento . '.ok');
                    $totalOk++;
                } else {
                    rename($path . '/' . $elemento, $path . '/' . $elemento . '.ko');
                    $totalnOk++;
                }
                $catalogo = $datosArchivo['catalogo'];
            }

            $arr_bancopaso = $datosArchivo['arr_bancopaso'];
            $arr_cabecera = $datosArchivo['arr_cabecera'];
            
            if($count == 0) {
                $formatFecha = date('Y-m-d', strtotime(str_replace(':', '-', $fecha)));
                $objbanco->fecha = $formatFecha;
                $objbanco->banco = $banco;
                $objbanco->carpeta = $carp[0];
                $objbanco->catalogo = $catalogo;
                $objbanco->inicio = $inicio;
                $objbanco->fin = $fin;
                $objbanco->nPruebas = 0;
                $objbanco->ok = $totalOk;
                $objbanco->okPer = 0;
                $objbanco->ko = $totalnOk;
                $objbanco->nOkPer = 0;
                $objbanco->save();
                $count++;
            }

            if(is_array($arr_cabecera['sAislamiento'])) $arr_cabecera['sAislamiento'] = '';
            if(is_array($arr_cabecera['sAcoplamiento'])) $arr_cabecera['sAcoplamiento'] = '';
            if(is_array($arr_cabecera['vPicoTension'])) $arr_cabecera['vPicoTension'] = '';
            if(is_array($arr_cabecera['sPicoTension'])) $arr_cabecera['sPicoTension'] = '';
            if(is_array($arr_cabecera['pPicoTension'])) $arr_cabecera['pPicoTension'] = '';
            if(is_array($arr_cabecera['rPicoTension'])) $arr_cabecera['rPicoTension'] = '';

           $pruebadesc = BancoPruebaDescripcion::create([
                'dmc' => $arr_cabecera['dmc'],
                'resultado' => $arr_cabecera['resultado'],
                'nPruebas' => $arr_cabecera['nPruebas'],
                'nMq' => $arr_cabecera['nMq'],
                'fecha' => $arr_cabecera['fecha'],
                'catalogo' => $arr_cabecera['catalogo'],
                'sAislamiento' => $arr_cabecera['sAislamiento'],
                'sAcoplamiento' => $arr_cabecera['sAcoplamiento'],
                'vPicoTension' => $arr_cabecera['vPicoTension'],
                'sPicoTension' => $arr_cabecera['sPicoTension'],
                'pPicoTension' => $arr_cabecera['pPicoTension'],
                'rPicoTension' => $arr_cabecera['rPicoTension'],
                'carpeta' => $carp[0],
                'banco' => $banco,
            ]);
            $arr_fechas[] = $arr_cabecera['fecha'];
            
            $arr_ps = [];
            foreach ($arr_bancopaso as $paso) {
                $paso['banco'] = $banco;
                $paso['carpeta'] = $carp[0];
                $paso['nPruebas'] = $arr_cabecera['nPruebas'];
                $arr_ps[] = $paso;
            }
            BancoPaso::insert($arr_ps);
        }

        if ($totalOk === 0 && $totalnOk === 0) return false;

        foreach ($pruebas as $i => $d) {

            $pruebas[$i]['media'] = round($d['media'] / $d['total'], 2);

            $pruebas[$i]['nOkPer'] = round($d['nOk'] * 100 / $d['total'], 2);

            $pruebas[$i]['carpeta'] = $carp[0];

        }
        
        sort($arr_fechas);

        $totalPruebas = $totalOk + $totalnOk;

        $okPer = round(($totalOk / $totalPruebas * 100), 2);
        $nOkPer = round(($totalnOk / $totalPruebas * 100), 2);

    	$banco->inicio = substr($arr_fechas[0],11);
    	$banco->fin = substr(end($arr_fechas),11);
        $objbanco->nPruebas = $totalPruebas;
        $objbanco->okPer = $okPer;
        $objbanco->nOkPer = $nOkPer;
        $objbanco->ok = $totalOk;
        $objbanco->ko = $totalnOk;
        $objbanco->save();

        if ($pruebas !== false) {
            $arr_pr = [];
            foreach ($pruebas as $p) {
                if (is_array($p['descripcion'])) {
                    $p['descripcion'] = '';
                }
                $p['fecha'] = $objbanco->fecha;
                $p['paso'] = $p['paso']-1;
                $arr_pr[] = $p;
            }
            BancoPrueba::insert($arr_pr);
        }
    }

    private function leerArchivo($path, $elemento, &$carpeta)
    {

        $banco = explode('/', $path)[count(explode('/', $path)) - 2];

        $res = $this->xml2json($path . '/', $elemento);

        $arr_bancopaso = [];

        for ($i = 0; $i < count($res['PasoXML']); $i++) {

            $paso = $res['PasoXML'][$i];
            $dmc = isset($res['Id']) ?  $res['Id'] : $res['Nombre'];
            if(is_array($dmc) && empty($dmc)) $dmc = '';
            $datosPaso = $this->leerPaso($path, $paso, $dmc, $res['Fecha'], $arr_bancopaso);

            $arr_bancopaso = $datosPaso['arr_bancopaso'];

            if (!isset($carpeta[$i])) {

                $carpeta[$i] = [
                    'banco' => $banco,
                    'minimo' => INF,
                    'maximo' => 0,
                    'media' => 0,
                    'total' => 0,
                    'nOk' => 0,
                    'paso' => $i + 1,
                    'etapa' => $datosPaso['etapa'],
                    'descripcion' => $datosPaso['descripcion']
                ];

            }

            if ($datosPaso['valor'] > $carpeta[$i]['maximo']) $carpeta[$i]['maximo'] = $datosPaso['valor'];

            if ($datosPaso['valor'] < $carpeta[$i]['minimo']) $carpeta[$i]['minimo'] = $datosPaso['valor'];

            if ($paso['S_EtapaProceso'] !== 'B') $carpeta[$i]['nOk']++;

            $carpeta[$i]['media'] += $datosPaso['valor'];

            $carpeta[$i]['total']++;

        }

        $objectFecha = explode('/', $res['Fecha']);

        if(!isset($res['NroPruebas'])) $res['NroPruebas'] = null;
        if(!isset($res['NroMq'])) $res['NroMq'] = null;
        if(!isset($res['S_Aislamiento'])) $res['S_Aislamiento'] = null;
        if(!isset($res['V_PicoTension'])) $res['V_PicoTension'] = null;
        if(!isset($res['S_PicoTension'])) $res['S_PicoTension'] = null;
        if(!isset($res['P_PicoTension'])) $res['P_PicoTension'] = null;
        if(!isset($res['R_PicoTension'])) $res['R_PicoTension'] = null;

        $arr_cabecera = [
            'dmc' => $dmc,
            'resultado' => $res['Resultado'],
            'nPruebas' => $res['NroPruebas'],
            'nMq' => $res['NroMq'],
            'fecha' => Carbon::createFromFormat('d:m:Y/H:i:s',$res['Fecha'])->format('Y-m-d H:i:s'),
            'catalogo' => $res['CatalogoActivo'],
            'sAislamiento' => $res['S_Aislamiento'],
            'sAcoplamiento' => $res['S_Acoplamiento'],
            'vPicoTension' => $res['V_PicoTension'],
            'sPicoTension' => $res['S_PicoTension'],
            'pPicoTension' => $res['P_PicoTension'],
            'rPicoTension' => $res['R_PicoTension'],
        ];

        return [
            'fecha' => $objectFecha[0],
            'hora' => $objectFecha[1],
            'ok' => $res['Resultado'] == 'BUENO',
            'catalogo' => $res['CatalogoActivo'],
            'arr_bancopaso' => $arr_bancopaso,
            'arr_cabecera' => $arr_cabecera,
        ];

    }

    private function xml2json($path, $elemento)
    {

        $xmlfile = file_get_contents($path . '/' . $elemento);
        $xml = simplexml_load_string($xmlfile);

        $doc = (json_decode(json_encode($xml), true));

        return $doc;

    }

    private function leerPaso($path, $paso, $archivo, $fecha, $arr_bancopaso)
    {

        $carpetas = explode('/', $path);

        $folder = $carpetas[count($carpetas) - 1];

        $banco = $carpetas[count($carpetas) - 2];

        $carp = explode('#', $folder);

        for ($x = 1; $x <= 4; $x++) {
            if (is_array($paso["V_V" . $x])) $paso["V_V" . $x] = '';

            if (is_array($paso["S_V" . $x]))  $paso["S_V" . $x] = '';

            if (is_array($paso["LI_V" . $x])) $paso["LI_V" . $x] = '';

            if (is_array($paso["LS_V" . $x])) $paso["LS_V" . $x] = '';
        }

        $objectFecha = explode('/', $fecha);

        $inicio = $objectFecha[1];

        $formatFecha = Carbon::createFromFormat('d:m:Y',$objectFecha[0])->format('Y-m-d');

        $arr_bancopaso[] = [
            'fecha' => $formatFecha,
            'inicio' => $inicio,
            'dmc' => $archivo,
            'paso' => $paso['@attributes']['index'],
            'descripcion' => is_array($paso['Proceso']) ? '' : $paso['Proceso'],
            'etapaProceso' => is_array($paso['S_EtapaProceso']) ? '' : $paso['S_EtapaProceso'],
            'etapa' => $paso['Etapa'],
            "vV1" => $paso["V_V1"],
            "sV1" => $paso["S_V1"],
            "liV1" => $paso["LI_V1"],
            "lsV1" => $paso["LS_V1"],

            "vV2" => $paso["V_V2"],
            "sV2" => $paso["S_V2"],
            "liV2" => $paso["LI_V2"],
            "lsV2" => $paso["LS_V2"],

            "vV3" => $paso["V_V3"],
            "sV3" => $paso["S_V3"],
            "liV3" => $paso["LI_V3"],
            "lsV3" => $paso["LS_V3"],

            "vV4" => $paso["V_V4"],
            "sV4" => $paso["S_V4"],
            "liV4" => $paso["LI_V4"],
            "lsV4" => $paso["LS_V4"],

            "carpeta" => $carp[0],
            'banco' => $banco,
        ];


        $valor = $paso['V_V1'];

        preg_match_all('!\d+!', $valor, $matches);

        if (count($matches[0]) > 1) {

            $valor = $matches[0][0] . '.' . $matches[0][1];

        } elseif ($valor === 'B' || $valor === 'Ok') {

            $valor = 1;

        } elseif ($valor !== 'B' || $valor !== 'Ok') {

            $valor = 0;

        } else {

            $valor = $matches[0][0];

        }

        return [
            'valor' => $valor,
            'etapa' => $paso['Etapa'],
            'descripcion' => $paso['Proceso'],
            'arr_bancopaso' => $arr_bancopaso,
        ];

    }

    private function readFilters($query, $request, $cols = null)
    {
        if (isset($request->filters)) {
            if (!is_array($request->filters)) $request->filters = json_decode($request->filters);
            foreach ($request->filters as $filter) {
                if (is_string($filter)) $filter = json_decode($filter);

                if (!isset($filter->field) || !isset($filter->operator)) continue;

                if ($filter->field == 'fecha') {
                    if ($filter->operator == 'between') {

                        if ((!is_numeric($filter->value[0]) && is_string($filter->value[0]) && (strpos($filter->value[0], '-') >= 0 || strpos($filter->value[0], '/') >= 0))
                            && (!is_numeric($filter->value[1]) && is_string($filter->value[1]) && (strpos($filter->value[1], '-') >= 0 || strpos($filter->value[1], '/') >= 0))) {
                            $fechaDesde = $this->setDateAttribute($filter->value[0]);
                            $fechaHasta = $this->setDateAttribute($filter->value[1]);

                            if(is_array($fechaDesde) || is_array($fechaHasta)) return ['error' => 400];

                            $fechaDesde = Carbon::createFromFormat('Y-m-d', $fechaDesde);
                            $fechaHasta = Carbon::createFromFormat('Y-m-d', $fechaHasta);

                            if($fechaDesde->lt($fechaHasta)) {
                                $between = [$fechaDesde, $fechaHasta];
                            } else {
                                $between = [$fechaHasta, $fechaDesde];
                            }

                            $query->whereBetween($filter->field, $between);
                        } else {
                            return ['error' => 400];
                        }

                    } else {

                        if (!is_numeric($filter->value[0]) && is_string($filter->value[0]) && (strpos($filter->value[0], '-') >= 0 || strpos($filter->value[0], '/') >= 0)) {
                            $fechaDesde = $this->setDateAttribute($filter->value[0]);

                            if(is_array($fechaDesde)) return ['error' => 400];

                            $query->where($filter->field, $fechaDesde);
                        } else {
                            return ['error' => 400];
                        }

                    }
                } else {
                    if ($filter->operator === 'between') $query->whereBetween($filter->field, $filter->value);
                    elseif ($filter->operator === '%') $query->where($filter->field, 'like', '%' . $filter->value[0] . '%');
                    else $query->where($filter->field, $filter->operator, $filter->value[0]);
                }
            }
        }

        if (isset($request->select_fecha)) $query->where('fecha', Carbon::createFromFormat('Y-m-d',$request->select_fecha)->format('d/m/Y'));

        if (isset($request->select_prueba) && $request->select_prueba != '') {
//            $bpd = BancoPruebaDescripcion::where('dmc', $request->banco)->where('nPruebas', $request->select_prueba)->first();
//            $fecha = str_replace(' ', '', $bpd->fecha);
			$query->where('nPruebas', $request->select_prueba);
        }

        if (isset($request->sSearch) && $request->sSearch != '' && isset($cols)) {
            $query->where(function ($query) use ($request, $cols) {
                foreach ($cols as $col) {
                    $query->orWhere($col, 'like', '%' . $request->sSearch . '%');
                }
            }
            );
        }
        return $query;
    }

    private function setDateAttribute($date)
    {
        try {
            $approvedFormats = ['d/m/Y', 'Y-m-d', 'd-m-Y', 'y-m-d', 'd-m-y', 'd M Y'];
            foreach ($approvedFormats as $format) {
                if ($carbon = Carbon::createFromFormat($format, $date)) {
                    return $carbon->format('Y-m-d');
                } else {
                    continue;
                }
            }
        } catch (\Exception $e) {
            return ['message' => 'Las fechas introducidas tienen un formato incorrecto. El formato correcto es d/m/Y', 400];
        }
    }

    private function skipOrder($request, $query) {
        if (isset($request->iDisplayStart)) {
            $query->skip($request->iDisplayStart)->take($request->iDisplayLength);
        }

        $c = 0;
        foreach ($request->all() as $k => $v) {
            if (strpos($k, 'iSortCol_') === false) continue;

            $query->orderBy($request['mDataProp_' . $v], $request['sSortDir_' . $c]);
            $c++;

        }

        return $query;
    }
    //</editor-fold>
 /*   public function updateNPruebas() {
        foreach(Banco::get() as $banco) {
        	DB::enableQueryLog();
        	$fecha = Carbon::createFromFormat('d/m/Y',$banco->fecha)->format('Y-m-d');
        	$arr = [];
        	foreach(BancoPruebaDescripcion::where('banco',$banco->banco)->where('carpeta',$banco->carpeta)->where('fecha','like',$fecha.'%')->get() as $bpd) {
        		$arr[] = $bpd->fecha;
        	}
        	sort($arr);
        	$banco->inicio = substr($arr[0],11);
        	$banco->fin = substr(end($arr),11);
        	$banco->save();
        }
    }*/
}
