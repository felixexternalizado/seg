<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = 'categorias';

    protected $fillable = [
        'title','icon','color','orden',
    ];

    public function aplications() {
        return $this->hasMany('App\Aplicacion', 'categoria_id');
    }

}
