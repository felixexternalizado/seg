<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cargoslineasempleados extends Model
{
    protected $table = 'cargoslineasempleados';

    protected $fillable = ['empleado_id', 'linea_id', 'cargo_id'];

    public function empleado() {
        return $this->belongsTo('App\Empleados', 'empleados_id');
    }

    public function linea() {
        return $this->belongsTo('App\Line', 'lineas_id');
    }

    public function cargo() {
        return $this->belongsTo('App\Cargos', 'cargos_id');
    }
}
