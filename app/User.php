<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'empleados_id', 'admin', 'aplicaciones', 'creator_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'admin' => 'boolean',
    ];

    public function aplicaciones() {
        return $this->belongsToMany('App\Aplicacion', 'aplicaciones_users');
    }

    public function empleado () {
        return $this->belongsTo('App\Empleados', 'empleados_id');
    }

    public function permisos() {
        return $this->hasMany('App\PermisoUser', 'user_id');
    }
}
