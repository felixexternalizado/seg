<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class LaboratorioRegistro extends Model
{
    protected $table = 'registrosdelaboratorio';

    protected $fillable = [
        'linea_id','zona_id', 'empleado_id', 'numero_pieza','catalogo', 'nombre_pieza', 'fecha','estado'
    ];

    public function getFechaAttribute( $value ) {
        return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('Y-m-d');
    }

    public function linea () {
        return $this->belongsTo('App\Line', 'linea_id');
    }

    public function zona () {
        return $this->belongsTo('App\Zona', 'zona_id');
    }

    public function empleado() {
        return $this->belongsTo('App\Empleados', 'empleado_id');
    }
}
