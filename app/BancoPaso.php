<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BancoPaso extends Model
{
    protected $table = 'bancopasos';
    public $timestamps = false;
    protected $fillable = [
        'id',
        'fecha',
        'inicio',
        'dmc',
        'paso',
        'descripcion',
        'etapaProceso',
        'etapa',
        'minimo',
        'maximo',
        'media',
        'total',
        'nOk',
        'nOkPer',
        'vV1',
        'sV1',
        'liV1',
        'lsV1',
        'vV2',
        'sV2',
        'liV2',
        'lsV2',
        'vV3',
        'sV3',
        'liV3',
        'lsV3',
        'vV4',
        'sV4',
        'liV4',
        'lsV4',
        'carpeta',
        'banco',
        'nPruebas',
    ];
}
