<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OptimizacionCos extends Model
{
    protected $table = 'optimizacioncos';
    protected $fillable = [
        "id","numero_orden","estado","numero_pieza","orden_venta","cliente","tipo_plataforma","tipo","tipo_muestra","orden_cantidad_total","cantidad_enviada",
        "cantidad_pendiente_entrega","kit_termopar","observacion_orden","incoterm","detalles_envio","fecha_llegada_SGES_COS","kwt","fecha_envio_confirmada_SGES_COS",
        "fecha_envio_modificada","fecha_entrega_real_COST_CLP","fecha_envio_real_exTRETO","fecha_maxima_entrega","pps_ckd_montaje","ok_logistico","ok_tecnico",
        "internal_remarks_COS","revisar","mes_tomado_medir_cumplimentacion","otd","cumplimentacion_motivos_atraso","desviacion_tecnica","motivos_comentarios",
        "medida_correctiva","quien","actions","rotor","ok_logistico_rotor","ok_tecnico_rotor","comentarios_internos_rotor","stock_rotor","cantidad_necesaria_rotor",
        "fecha_maxima_entrega_rotor","rotor_disponible_notificado_trasladado","stator","ok_logistico_stator","ok_tecnico_stator","comentarios_internos_stator",
        "stock_stator", "cantidad_necesaria_stator","fecha_maxima_entrega_stator","stator_disponible_notificado_trasladado","rectifier","ok_logistico_rectifier",
        "ok_tecnico_rectifier","comentarios_internos_rectifier","stock_rectifier","cantidad_necesaria_rectifier","fecha_maxima_entrega_rectifier",
        "rectifier_disponible_notificado_trasladado","ecu","ok_logistico_ecu","ok_tecnico_ecu","comentarios_internos_ecu","stock_ecu","cantidad_necesaria_ecu",
        "fecha_maxima_entrega_ecu","ecu_disponible_notificado_trasladado","regulator_brush_holder","ok_logistico_regulator_brush_holder",
        "ok_tecnico_regulator_brush_holder","comentarios_internos_regulator_brush_holder","stock_regulator_brush_holder","cantidad_necesaria_regulator_brush_holder",
        "fecha_maxima_entrega_regulator_brush_holder","regulator_brush_holder_disponible_notificado_trasladado","drive_end_shield","ok_logistico_drive_end_shield",
        "ok_tecnico_drive_end_shield","comentarios_internos_drive_end_shield","stock_drive_end_shield","cantidad_necesaria_drive_end_shield",
        "fecha_maxima_entrega_drive_end_shield","drive_end_shield_disponible_notificado_trasladado","slip_ring_end_shield","ok_logistico_slip_ring_end_shield",
        "ok_tecnico_slip_ring_end_shield","comentarios_internos_slip_ring_end_shield","stock_slip_ring_end_shield","cantidad_necesaria_slip_ring_end_shield",
        "fecha_maxima_entrega_slip_ring_end_shield","slip_ring_end_shield_disponible_notificado_trasladado","protective_cap","ok_logistico_protective_cap",
        "ok_tecnico_protective_cap","comentarios_internos_protective_cap","stock_protective_cap","cantidad_necesaria_protective_cap",
        "fecha_maxima_entrega_protective_cap","protective_cap_disponible_notificado_trasladado","pulley","ok_logistico_pulley","ok_tecnico_pulley",
        "comentarios_internos_pulley","stock_pulley","cantidad_necesaria_pulley","fecha_maxima_entrega_pulley","pulley_disponible_notificado_trasladado",
        "pulley_cap","ok_logistico_pulley_cap","ok_tecnico_pulley_cap","comentarios_internos_pulley_cap","stock_pulley_cap","cantidad_necesaria_pulley_cap",
        "fecha_maxima_entrega_pulley_cap","pulley_cap_disponible_notificado_trasladado","bearing","ok_logistico_bearing","ok_tecnico_bearing",
        "comentarios_internos_bearing","stock_bearing","cantidad_necesaria_bearing","fecha_maxima_entrega_bearing","bearing_disponible_notificado_trasladado",
        "others","ok_logistico_others","ok_tecnico_others","comentarios_internos_others","stock_others","cantidad_necesaria_others","fecha_maxima_entrega_others",
        "others_disponible_notificado_trasladado","connection_plate_isolation_part","ok_logistico_connection_plate_isolation_part",
        "ok_tecnico_connection_plate_isolation_part","comentarios_internos_connection_plate_isolation_part","stock_connection_plate_isolation_part",
        "cantidad_necesaria_connection_plate_isolation_part","fecha_maxima_entrega_connection_plate_isolation_part",
        "connection_plate_isolation_part_disponible_notificado_trasladado"
    ];
}
