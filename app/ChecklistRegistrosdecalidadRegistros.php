<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChecklistRegistrosdecalidadRegistros extends Model
{
    protected $table = 'checklist_regcalidad_registros';

    protected $fillable = [
        'registrocalidad_id',
        'indice',
        'valor',
        'fase',
        'foto',
        'fotoInstrucciones',
        'descripcion',
        'frecuencia',
        'operacion',
        'comentario',
        'noconformidad',
        'limite1',
        'limite2',
        'formato',
        'tipo',
        'instrucciones',
        'validador',
//        'opciones',
        'valormanual',
        'ncFlag',
        'horaEntrada',
        'requerido'
    ];
}
