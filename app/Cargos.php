<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cargos extends Model
{
    protected $fillable = [
        'resumen',
    ];

    public function cargos () {

        return $this->hasMany('App\Cargoslineasempleados');

    }

}
