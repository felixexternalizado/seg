<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleados extends Model
{
    protected $fillable = [
        'id',
        'default_zona_id',
        'default_linea_id',
        'responsable_id',
        'nombre',
        'email',
        'iniciales',
        'resumen'
    ];

    public function confirmacionproceso () {
        return $this->hasMany('App\Confirmacionprocesos');
    }
    public function registrocalidad () {
        return $this->hasMany('App\Registrocalidad');
    }
    public function cambiocatalogo () {
        return $this->hasMany('App\CambioCatalogo');
    }
    public function cargos () {
        return $this->hasMany('App\Cargoslineasempleados');
    }
    public function zona() {
        return $this->belongsTo('App\Zona', 'default_zona_id');
    }
    public function linea() {
        return $this->belongsTo('App\Linea', 'default_linea_id');
    }
    public function responsable() {
        return $this->belongsTo('App\Empleados', 'responsable_id');
    }
}
