<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CambioCatalogo extends Model
{
    protected $table = 'cambiocatalogo';

    protected $fillable = [
        'empleado_id',
        'linea_id',
        'alternador',
        'fechaInicio',
        'fechaFinal',
        'totalMal',
        'totalBien',
        'porcentajeBien',
        'totalPreguntas',
        /*'piezasRealizadas',
        'piezasEsperadas',
        'piezasPerdidas',
        'tiempoConsumido',*/
        'estado',
    ];

    public function documentos() {
        $docs = Documento::where('cambiocatalogo_id', $this->id)->get();
        $lista = ["documentos" => [], "marcadores" => []];
        foreach ($docs as $doc) {
            $lista[$doc->tipo][] = $doc;
        }
        return $lista;
    }

    public function registros() {
        return $this->hasMany('App\ChecklistCambiocatalogoRegistros', 'cambiocatalogo_id');
    }
    public function empleado() {
        return $this->belongsTo('App\Empleados', 'empleado_id');
    }
    public function linea() {
        return $this->belongsTo('App\Line', 'linea_id');
    }
}
