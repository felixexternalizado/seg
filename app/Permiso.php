<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
    protected $table = 'permisos';

    protected $fillable = [
        'keyname',
        'nombre',
    ];

    public function permisos() {
        return $this->hasMany('App\PermisoUser', 'permiso_id');
    }
}
