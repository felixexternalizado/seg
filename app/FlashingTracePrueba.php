<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlashingTracePrueba extends Model
{
    protected $table = 'flashingtrace_pruebas';
    
    protected $fillable = [
        'fecha',
        'nombre',
        'minimo',
        'maximo',
        'media',
        'total',
        'nOk',
        'paso',
        'section',
        'description',
        'nOkPer',
        'carpeta',
    ];
}
