<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlashingTrace extends Model
{
    protected $table = 'flashingtrace';
    
    protected $fillable = [
        'fecha',
        'nombre',
        'carpeta',
        'catalogo',
        'inicio',
        'fin',
        'nPruebas',
        'ok',
        'okPer',
        'ko',
        'nOkPer',

    ];
}
