<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Registrocalidad extends Model
{
    protected $table = 'registrosdecalidad';
    protected $fillable = [
        'linea_id',
        'zona_id',
        'maquina_id',
        'procesocalidad_id',
        'empleados_id',
        'resumen',
        'frecuencia',
        'valor',
        'evaluador',
        'fechaInicio',
        'fechaFinal',
        'linea_id',
        'totalMal',
        'totalBien',
        'porcentajeBien',
        'totalPreguntas',
        'estado',
        'json'
    ];

    public function registros() {
        return $this->hasMany('App\ChecklistRegistrosdecalidadRegistros', 'registrocalidad_id');
    }

    public function documentos() {
        return [];
    }

    public function empleado() {
        return $this->belongsTo('App\Empleados', 'empleados_id');
    }

    public function linea() {
        return $this->belongsTo('App\Line', 'linea_id');
    }

    public function zona() {
        return $this->belongsTo('App\Zona', 'zona_id');
    }

    public function noconformidades () {
        return $this->hasMany('App\Noconformidades', 'aplicacion_id')->where('opl', 'registroscalidad');
    }

}
