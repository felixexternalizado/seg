<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Banco extends Model
{
    protected $table = 'bancoresumen';
    public $timestamps = false;
    protected $fillable = [
        'fecha',
        'banco',
        'carpeta',
        'catalogo',
        'inicio',
        'fin',
        'nPruebas',
        'ok',
        'okPer',
        'ko',
        'nOkPer',
    ];

    public function bancoprueba () {
        return BancoPrueba::where('banco', $this->banco)->where('carpeta', $this->carpeta)->get();
    }

    public function bancopaso () {
        return BancoPaso::where('banco', $this->banco)->where('carpeta', $this->carpeta)->get();
    }

    public function getFechaAttribute( $value ) {
        return Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y');
    }

    public function getDateAttribute( $value ) {
        return $this->getFechaAttribute($value);
    }
}
