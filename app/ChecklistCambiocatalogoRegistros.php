<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChecklistCambiocatalogoRegistros extends Model
{
    protected $table = 'checklist_cambiocatalogo_registros';

    protected $fillable = [
        'cambiocatalogo_id',
        'indice',
        'valor',
        'fase',
        'foto',
        'fotoInstrucciones',
        'descripcion',
        'operacion',
        'comentario',
        'noconformidad',
        'limite1',
        'limite2',
        'tipo',
        'instrucciones',
        'validador',
        'condicional',
        'requerido',
        'valormanual',
        'ncFlag',
        'horaEntrada',
    ];
}
