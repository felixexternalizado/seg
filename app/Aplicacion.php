<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aplicacion extends Model
{
    protected $table = 'aplicaciones';

    protected $fillable = [
        'categoria_id','text','link','orden'
    ];

    public function usuarios() {
        return $this->hasMany('App\AplicacionUser', 'user_id');
    }

    public function categoria() {
        return $this->belongsTo('App\Categoria', 'categoria_id');
    }

}
