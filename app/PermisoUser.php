<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermisoUser extends Model
{
    protected $table = 'permisos_users';

    protected $fillable = [
        'permiso_id',
        'user_id',
    ];
}
