<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Maquina extends Model
{
    protected $table = 'maquinas';

    protected $fillable = [
        'linea_id', 'zona_id', 'fase', 'descripcion'
    ];

    public function setAttributeLinea($o)
    {
        $l = Line::where('id',$o)->first();
        $this->attributes['linea'] = $l->resumen;
    }
    public function setAttributeZona($o)
    {
        $z = Zona::where('id',$o)->first();
        $this->attributes['zona'] = $z->resumen;
    }
}
