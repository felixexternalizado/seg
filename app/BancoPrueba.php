<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class BancoPrueba extends Model
{
    protected $table = 'bancopruebas';
    public $timestamps = false;
    protected $fillable = [
        'fecha',
        'paso',
        'descripcion',
        'etapa',
        'minimo',
        'maximo',
        'media',
        'total',
        'nOk',
        'nOkPer',
        'carpeta',
        'banco',
    ];

    public function getDateAttribute( $value ) {
        return $this->getFechaAttribute($value);
    }
}
