<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dias extends Model
{
    protected $fillable = [
        'dia','semana','week','mes','anno'
    ];

    protected $table = 'dias';

    public function returnSemana($dia, $mes, $anno) {
        $o = Dias::where('dia', $dia)->where('mes', $mes)->where('anno', $anno)->first();
        $semana = $o->semana;
        return $semana;
    }
}
