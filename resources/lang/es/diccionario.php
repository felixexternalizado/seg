<?php

return array(
    'Banco' => 'Banco',
    'Fecha' => 'Fecha',
    'Paso' => 'Paso',
    'Catalogo' => 'Catálogo',
    'Inicio' => 'Inicio',
    'Fin' => 'Fin',
    'Total de pruebas' => 'Total de pruebas',
    'Descripcion' => 'Descripción',
    'Etapa' => 'Etapa',
    'Minimo' => 'Mínimo',
    'Maximo' => 'Máximo',
    'Media' => 'Media',
    'Valor 1' => 'Valor 1',
    'Valor 2' => 'Valor 2',
    'Valor 3' => 'Valor 3',
    'Valor 4' => 'Valor 4',
    'DMC Estator' => 'DMC Estator',
    'Resumen' => 'Resumen',
    'Pasos' => 'Pasos',
    'Pruebas' => 'Pruebas',
    'igual' => 'igual',
    'entre' => 'entre',
    'contiene' => 'contiene',
    'resultados_dmc' => 'Resultados de DMC',
    'bueno' => 'ESTADO BUENO',
    'malo' => 'ESTADO MALO',
    'todos' => 'AMBOS ESTADOS',
    'mayor' => 'Mayor que',
    'menor' => 'Menor que',
    'select_fecha' => 'Seleccione fecha',
    'nprueba' => 'Numero de prueba',
    'vv1' => 'Valor 1',
    'vv2' => 'Valor 2',
    'vv3' => 'Valor 3',
    'vv4' => 'Valor 4',
    'li' => 'Limite Inferior',
    'ls' => 'Limite Superior',
    'resultado' => 'Resultado',
    'prueba' => 'Prueba',
    'estado' => 'Estado'
);
