<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Email</title>
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <p><b>Se ha creado una nueva No Conformidad</b></p>
            <br>
            <p>Proceso: {{$data->nombreproceso}}</p>
            <p>Fase: {{$data->fase}}</p>
            <p>Resumen: {{$data->resumen}}</p>
            <p>Descripción: {{$data->descripcion}}</p>
            <p>Valor: {{$data->valor}}</p>
            <p>Comentario: {{$data->comentario}}</p>
            <p>Fecha Inicio: {{$data->fechaInicio}}</p>
        </div>
    </body>
</html>
